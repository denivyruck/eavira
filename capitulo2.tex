\chapter{Revisão de Literatura}
\label{chap:revisao}

Os conceitos e tecnologias necessários para o desenvolvimento do mecanismo de realocação são revisados neste capítulo. A Seção \ref{sec:nuvem} apresenta as definições relacionadas com nuvens computacionais, com foco em um serviço específico: o provisionamento de IVs. A Seção ~\ref{sec:elasticidade} revisa o provisionamento elástico de recursos, enquanto os mecanismos de elasticidade encontrados na literatura são discutidos na Seção ~\ref{sec:mecanismos}, com ênfase na economia do consumo energético. As Seções 4 e 5 discutem algoritmos de alocação de IVs e o provisionamento verde, respectivamente. A Seção \ref{chap2:parciais} apresenta as considerações parciais do capítulo.

\section{Nuvens Computacionais}\label{sec:nuvem}

Nuvem computacional é um modelo de computação que vem ganhando destaque pela utilidade dos serviços que oferece, e cuja inspiração proveio dos cenários de grades computacionais, definidas como infraestruturas distribuídas e federadas de aglomerados de servidores interconectados, comumente utilizadas para processamento de alto desempenho~\cite{foster:2008}. 
Assim como \textit{datacenters} tradicionais, sempre que é necessário incrementar o poder computacional desta infraestrutura, é necessário a melhoria ou compra de novos servidores.
Já a nuvem computacional, segundo o \emph{National Institute of Standards and Technology} (NIST) \cite{Mell-2011}, é definida como um ''modelo que permite o acesso conveniente e sob demanda a um conjunto compartilhado de recursos computacionais configuráveis (tais como rede, servidores, armazenamento, aplicações e serviços) que podem ser rapidamente provisionados e liberados sem um grande esforço de gerenciamento pelo provedor de serviço''. 

Com o surgimento deste paradigma, novas aplicações com diferentes padrões de computação e comunicação puderam ser facilmente desenvolvidas~\cite{fox2009above}:

\begin{itemize}
    \item Aplicações \textit{mobile} interativas: devido às limitações de \textit{hardware} presentes atualmente em dispositivos \textit{mobile} e a sensibilidade ao atraso que os humanos apresentam quanto a resposta para suas solicitações, aplicações em \textit{mobile} necessitam retornar uma rápida resposta para os usuários mesmo quando essa resposta requer o processamento de uma alta quantidade de dados \cite{bonomi2012fog}. Isso só passa a ser possível com a utilização de serviços em nuvem, em que as requisições são feitas e processadas diretamente no \textit{datacenter} que hospeda a aplicação, e não no dispositivo final.
    \item Computação paralela em lotes: aplicações hospedadas na nuvem podem explorar o elevado número de recursos disponíveis para acelerar o processamento de dados. Caso a aplicação apresente regiões paralelizáveis, disponibilidade de recursos virtuais escaláveis torna-se uma alternativa. Abstrações como MapReduce\footnote{https://hadoop.apache.org/docs/r1.2.1/mapred\_tutorial.html} e Hadoop\footnote{http://hadoop.apache.org/} são utilizadas neste cenário~\cite{fox2012cloud}.
    \item Análise de dados: fazendo uso das técnicas paralelas do tópico anterior, os recursos disponíveis são agora utilizados para compreender as expectativas do consumidor. Informações como itens comprados, \textit{sites} acessados, horas de compra e tantas outras são processadas de modo a construir uma rede de conhecimento da aplicação. Esses conhecimentos são utilizados para criar estatísticas e sugestões ao consumidor que são revertidas em estratégias de negócio.
    \item Extensões para aplicações \textit{desktop} computacionalmente intensivas: \textit{softwares} como \textit{Matlab}\footnote{http://www.mathworks.com/products/matlab/}, \textit{Maple}\footnote{https://www.maplesoft.com/products/maple/} e \textit{Mathematica}\footnote{https://www.wolfram.com/mathematica/} ou até aplicações de renderização 3D podem fazer uso da nuvem para realizar cálculos computacionalmente custosos que são esporadicamente realizados.
\end{itemize}

Um dos principais pilares de sustento da computação em nuvem, que facilitou o desenvolvimento das aplicações listadas, é a utilização de técnicas de virtualização.
%
A virtualização de recursos físicos permite a abstração de recursos de processamento, armazenamento e memória, possibilitando a existência de diversas \ac{MVs} em uma mesma máquina física. Esta técnica permitiu a diminuição de custos com energia, gerenciamento e manutenção, além da utilização mais eficiente dos recursos disponíveis. Sem a técnica de virtualização, apenas 10\% dos recursos das máquinas eram explorados em \textit{datacenters}. Com a virtualização, por outro lado, quase a totalidade dos recursos das máquinas são explorados \cite{Koslovski-2012}.

Por conta dos recursos virtuais serem provisionados e desligados com facilidade e rapidez, provedores passaram a oferecer o aluguel de recursos computacionais que antes estavam ociosos. Com isso, um novo tipo de serviço passou a ser oferecido, chamado de \ac{IaaS}. Com este serviço, recursos virtuais são alugados para um usuário sendo o seu custo tarifado a cada hora. A partir do momento em que o usuário não mais necessitar do recurso alocado, a sua instância pode ser facilmente desligada pelo provedor. A Figura \ref{fig:services} retrata os principais modelos de serviços oferecidos. Na camada mais alta dos serviços está o \textit{Software as a Service -- SaaS}, que fornece aplicações como \textit{Facebook}, \textit{Google Apps} (\textit{Presentation, Docs, etc.}) para o usuário, geralmente por intermédio de uma interface web. Os serviços baseados em \textit{Platform as a Service - PaaS}, por outro lado, fornecem uma plataforma para desenvolvimento de aplicações na forma de \textit{frameworks}. 
Na camada mais baixa de serviços, a IaaS entrega infraestruturas virtuais sob demanda. Esse serviço é disponibilizado por provedores como \textit{Amazon Elastic Compute Cloud (EC2)}, \textit{Google Computing Engine}, \textit{GoGrid} e \textit{RackSpace}.

\FloatBarrier
\begin{figure}[ht]
\centering
\includegraphics[width=0.7\textwidth]{Imagens/services.png}
\caption{Serviços oferecidos por provedores de nuvem. Fonte:~\cite{zhang:2010}.}
\label{fig:services}
\end{figure}
\FloatBarrier

Embora possível, dificilmente um serviço PaaS ou SaaS será executado diretamente sobre uma máquina física não-virtualizada, justamente pelas vantagens que a virtualização traz relacionada à consolidação dos recursos disponíveis.
Para que todos estes serviços pudessem ser oferecidos com custos menores e com maior praticidade pelos provedores, a virtualização teve uma grande importância. 
A Figura~\ref{fig:virtualization} resume o conceito de virtualização de recursos físicos.
Neste cenário, a abstração e o mapeamento dos recursos físicos em virtuais é de responsabilidade do \ac{MMV}. 
Assim, múltiplas MVs (com configurações de recursos variáveis) podem ser instanciadas em um mesmo hospedeiro físico (com configuração de recursos fixa), permitindo que os recursos sejam explorados em sua totalidade. 
Além disso, a utilização do MMV induz o isolamento dos recursos virtuais, garantindo a consistência dos dados de cada \ac{MV}.

\FloatBarrier
\begin{figure}[ht]
\centering
\includegraphics[width=.5\textwidth]{Imagens/hypervisor.eps}
\caption{Virtualização de recursos computacionais. Fonte: próprio autor.}
\label{fig:virtualization}
\end{figure}
\FloatBarrier

Além da virtualização de recursos físicos, inseriu-se o conceito de virtualização de canais e equipamentos de comunicação (enlaces, roteadores, e \textit{switches}), permitindo o compartilhamento de recursos e equipamentos de transmissão ~\cite{Koslovski-2012}.
%
Assim, criou-se as chamadas Infraestruturas Virtuais, compostas por \ac{MVs} interligadas por canais de comunicação virtuais. A Figura~\ref{fig:iv_alocacao} retrata este cenário: duas requisições constituídas de máquinas, roteadores e enlaces virtuais, são alocadas sob a mesma infraestrutura. 
Por conta da virtualização do \textit{datacenter}, os nós A e D poderiam ter sido alocados no nó N4, caso houvessem recursos disponíveis. 
A escolha de qual nó hospedará os recursos virtuais depende da política de alocação adotada pelo provedor responsável pelo \textit{datacenter}. 
A política posteriormente proposta por este trabalho, por exemplo, é uma política direcionada à economia de energia.

\FloatBarrier
\begin{figure}[ht]
\centering
\includegraphics[width=.8\textwidth]{Imagens/iv_alocacao.png}
\caption{Exemplo de alocação de IVs sobre um \textit{datacenter}. Fonte:~\cite{Ruck-2014}}
\label{fig:iv_alocacao}
\end{figure}
\FloatBarrier

\section{Elasticidade de Recursos Virtuais}\label{sec:elasticidade}

Como aplicações em execução na nuvem podem apresentar variações de carga, os provedores de nuvem precisam desenvolver técnicas para escalar estes recursos adaptando a infraestrutura virtual hospedada aos novos requisitos (computacional, comunicação ou armazenamento). Segundo Righi~\cite{Righi-2013}, a elasticidade é a técnica responsável por gerenciar estas variações de carga de trabalho. Diferentemente da alocação estática de recursos, que busca o provisionamento para o pior caso, a alocação elástica é realizada dependendo da utilização do serviço, das métricas de monitoramento e do nível de serviço (\ac{SLA}) especificado.

Para exemplificar a elasticidade de recursos em nuvens computacionais, a Figura~\ref{fig:escalabilidade} apresenta o comportamento de uma aplicação em dois cenários. 
O cenário (a) retrata a alocação de recursos considerando sempre o pior caso esperado. Entretanto, a ocorrência de variações de requisições é comum, já que a quantidade de usuários que utilizam a aplicação é usualmente dinâmico. Ou seja, a alocação estática resultará em perda de desempenho quando a quantidade de requisições for maior do que a capacidade virtual estipulada pode suportar. Já o cenário (b) demonstra o comportamento da aplicação na presença da elasticidade. Os recursos são alocados sempre para a carga momentânea esperada para a aplicação, sendo que na presença de picos, a capacidade atual da \ac{MV} alocada é aumentada para suprir estes picos. A partir do momento em que a carga da aplicação diminuir, os recursos são liberados. 

\FloatBarrier
\begin{figure}[ht]
\centering
\includegraphics[width=1.0\textwidth]{Imagens/elasticidade.png}
\caption{Comportamento de aplicações na presença (a) ou não (b) de elasticidade. Fonte:~\cite{Righi-2013}}
\label{fig:escalabilidade}
\end{figure}
\FloatBarrier

A elasticidade é um dos principais pilares da computação em nuvem e permitiu a popularização do modelo \textit{pay-as-you-go}, em que os usuários são tarifados de acordo com a sua utilização \cite{Righi-2015}. Os principais benefícios deste modelo contemplam~\cite{Vieira-2016}:

\begin{itemize}
    \item Alta disponibilidade: já que a nuvem propõe a ideia de recursos ilimitados e sob demanda;
    \item Balanceamento de carga e tolerância a faltas: diversas réplicas para uma aplicação podem ser instanciadas, permitindo que requisições sejam encaminhadas à réplica com menor carga de trabalho no momento. Ainda, caso ocorra uma falta em uma das réplicas, a aplicação continuará executando de maneira transparente ao usuário final.
    \item Redução de custos: o usuário paga apenas pelo que utiliza, já que as infraestruturas alocadas podem ser facilmente escaladas para atender a novas demandas de requisições, sendo o preço pago atualizado de acordo com o aumento (ou diminuição) das novas máquinas.
\end{itemize}

\subsection{\textit{Service Level Agreement (SLA)}}

De acordo com Sauvé (2005)~\cite{sauve2005sla}, Acordos de Nível de Serviço (\ac{SLA}) são comumente utilizados em \textit{datacenters} com a finalidade de capturar requisitos de desempenho na alocação de IVs. 
%
Através dos Indicadores de Nível de Serviço (\textit{Service Level Indicators - SLI}), o SLA estabelece certas características que devem ser respeitadas, tais como confiabilidade, custos, desempenho, segurança, localização geográfica, responsabilidades administrativas, etc. 
%
Ou seja, estabelece o tipo de serviço e suas características.
%
De forma complementar, o chamado SLO (\textit{Service Level Objective}) é responsável por representar estas restrições, permitindo uma forma de quantificá-las em termos do SLI.
%
O SLO mede o desempenho do provedor de serviço de acordo com o SLA, trabalhando com métricas como vazão, disponibilidade, tempo de resposta e qualidade~\cite{Righi-2013}~\cite{Pfitscher-2014}~\cite{sauve2005sla}.
%
Ou seja, caso ocorram certos picos de execução, algumas características do SLO podem ser violadas, tais como a disponibilidade do serviço ou o tempo de resposta. 
%
Para que a demanda de serviço continue sendo garantida pelo provedor, é necessário a aplicação de mecanismo de elasticidade, respeitando outras restrições que possam estar presentes no SLA.

É importante ressaltar que durante o desenvolvimento de suas tarefas administrativas, provedores de nuvem devem respeitar as especificações dos usuários.
%
Em \textit{datacenters} virtualizados, provedores têm a liberdade para executar migrações e reconfigurações de recursos virtuais, que usualmente são imperceptíveis ao cliente do serviço hospedado.
%
O mecanismo proposto no presente trabalho considera as especificações dos usuários e o acordo previamente estabelecido (SLA e SLO) durante a execução de uma tarefa gerencial. 
%
É importante notar que a elasticidade deve sempre respeitar a capacidade máxima de recursos do servidor hospedeiro, ou limites de multiplexação definidos pelo SLA contratado.

\subsection{Requisição de Provisionamento Elástico}

Para definir o momento necessário para reconfiguração de uma IV, faz-se necessário a definição de pelo menos dois mecanismos: (i) quais são as métricas analisadas e; (ii) de que forma o comportamento da aplicação será analisada. 
Para a primeira situação, Simões e Kamienski~\cite{simoes-2014} definem certas métricas que podem ser monitoradas para identificar a carga da aplicação hospedada na IV:

\begin{itemize}
    \item Tempo de resposta: tempo entre o envio da requisição e o recebimento da resposta;
    \item Utilização dos recursos computacionais: capacidade utilizada de processamento, memória, armazenamento e rede (largura de banda, roteadores, etc.);
    \item Carga de trabalho: número de requisições simultâneas que devem ser atendidas pela aplicação.
\end{itemize}

O segundo mecanismo que deve ser definido é a forma com que tais métricas são analisadas. Para isso, duas são as técnicas utilizadas: reativas e proativas. A primeira atua através do estabelecimento de certos \textit{thresholds}, ou seja, a elasticidade é disparada caso certo limiar seja ultrapassado. Por exemplo, em um cenário de balanceamento de carga, novas \ac{MVs} seriam dinamicamente alocadas sempre que uma das métricas monitoradas fosse violada (por exemplo, o número de requisições atendidas). 
Esta é a técnica comumente utilizada por provedores de nuvem, tais com \textit{Amazon EC2}, \textit{Microsoft Azure} e \textit{Google Computing Engine}.
Já as técnicas proativas baseiam-se principalmente na identificação de padrões de comportamento. Estes padrões são identificados dentro de um intervalo de tempo e a elasticidade é disparada caso a predição viole alguma das métricas definidas. 
A dificuldade de implementação deste mecanismo está justamente na especificação do tamanho de janela observável e do tempo de observação. 

%Além disso, e similarmente ao método reativo, modelos proativos precisam ser eficientes no tratamento de falsos-positivos.
%O acionamento da elasticidade é realizado quando um certo número de violações das métricas acontece de maneira consecutiva. Entretanto, a identificação deste comportamento não é fácil, pois, por exemplo, se o tamanho da janela de observação for 3 e os dados da aplicação forem {93, 79, 89, 78, 96} (com teto 100), caso o limiar fosse de 80\%, a elasticidade não seria acionada, mesmo a aplicação operando acima da sua capacidade na maior parte do tempo. De modo a evitar estes erros de predição, a principal técnica utilizada é a do envelhecimento (\textit{aging}), em que se define um intervalo de observação para o comportamento da aplicação, atribuindo pesos maiores aos valores analisados mais recentemente. Utilizando esta técnica e para o mesmo exemplo acima, o \textit{threshold} calculado seria de $T=\frac{1}{2}*78+\frac{1}{4}*96+\frac{1}{8}*89+\frac{1}{16}*79+\frac{1}{32}*93 = 82.568$, o que acionaria a elasticidade.

%\FloatBarrier
%\begin{figure}[ht]
%\centering
%\includegraphics[width=1.0\textwidth]{Imagens/proactive.png}
%\caption{Detecção de padrões em técnicas proativas. Fonte:~\cite{Righi-2013}}
%\label{fig:proactive}
%\end{figure}
%\FloatBarrier

O mecanismo proposto neste trabalho é agnóstico às métricas, não atuando diretamente no seu monitoramento, mas sim na execução de ações para provisionamento elástico (conforme discutido na Seção~\ref{sec:mec}) oriundas de um sistema de monitoração.
Um exemplo de modelo proposto que pode identificar o comportamento da IV é discutido em~\cite{Pfitscher-2014}.

\section{Mecanismos para Provisionamento de Elasticidade}\label{sec:mecanismos}
\label{sec:mec}

Tanto usuários quanto provedores usufruem dos benefícios da elasticidade.  Para o usuário, técnicas de elasticidade são garantias de desempenho do sistema mesmo com grandes variações de carga de trabalho. 
Já o provedor consegue consolidar seus recursos físicos, balanceando a utilização da rede, melhorando a taxa de aceitação infraestruturas com sucesso, diminuindo a fragmentação do \textit{datacenter}, entre outros.
Esta seção apresenta as técnicas utilizadas para se alcançar a elasticidade em cenários \textit{IaaS}: (i) Replicação; (ii) Migração, (iii) Redimensionamento e (iv) Consolidação. Provedores de nuvem podem explorar estas técnicas separadamente ou em conjunto.

\subsection{Replicação de Máquinas Virtuais}

A replicação é um método comum quando a finalidade é o balanceamento de carga da aplicações  compostas por um despachante de requisições que contém a visão de todas as réplicas disponíveis, e que decide qual réplica atenderá uma nova requisição do usuário~\cite{Righi-2013}~\cite{Righi-2015}~\cite{Darolt-2016}. Nem sempre este método é o mais indicado, principalmente quando tem-se a preocupação com o tempo para escalar ou com o custo total da infraestrutura alocada~\cite{Sharma-2011}.
Em suma, é um processo que consiste na cópia da imagem de uma MV para a máquina de destino. Após a cópia, o sistema operacional hospedado e as aplicações são iniciadas, ingressando no conjunto de recursos disponíveis.
O tempo total para replicação de uma \ac{MV} $i$ pode ser calculado~\cite{Sharma-2011} conforme a Equação \ref{eq:replicacao}.
$D(i)$ representa o tamanho da imagem do sistema, $r$ representa a largura de banda disponível para mover a \ac{MV} até o hospedeiro destino e $\beta$ é uma constante representando o tempo necessário para inicialização do sistema operacional no equipamento de destino.

\begin{equation}
	\label{eq:replicacao}
	t(i)=\frac{D(i)}{r}+\beta
\end{equation}

\subsection{Migração de Máquinas Virtuais}
\label{sub:migration}

A migração é o processo pelo qual o estado de uma máquina virtual é copiado para outro hospedeiro de destino, geralmente sendo a única opção quando a máquina física atual não possui recursos suficientes para atender à nova demanda de elasticidade.
%
Em ambientes com armazenamento distribuído e compartilhado de imagens de MVs, é um processo menos custoso quando comparado à replicação, visto que apenas o conteúdo da RAM é copiado, não necessitando a cópia de toda a imagem do sistema, e também não apresenta sobrecargas de inicialização~\cite{Sharma-2011}.
%
É uma técnica comumente aplicada quando se necessita realizar manutenções nos componentes de um \textit{datacenter}.
%
Ainda, pode ser aplicada para reduzir o custo de energia da infraestrutura, migrando a MV para consolidar servidores.

Entretanto, visto que no momento da migração toda a memória pertencente à MV deve ser copiada,  caso a execução da máquina virtual fosse interrompida para que a cópia fosse feita, a aplicação apresentaria um alto tempo de indisponibilidade. 
%
De modo a amenizar este problema, Clark \textit{et al.} propuseram um mecanismo de \textit{live-migration}, constituído de 5 etapas~\cite{clark-2005}~\cite{kikuchi2012impact}:

\begin{itemize}
    \item Reserva: verificar se a máquina destino possui recursos suficientes para hospedar a MV a ser migrada.
    \item \textit{Iteractive pre-copy}: as páginas de memória são copiadas da origem para o destino, sendo o processo repetido enquanto houver um número significativo de páginas de memória sujas.
    \item \textit{Stop-and-copy}: a MV de origem é desativada e o restante das páginas de memória são copiadas para a máquina destino. 
    Ou seja, inevitavelmente haverá um certo tempo de indisponibilidade da aplicação, mas  inferior ao tempo necessário para a cópia direta entre as máquinas.
    \item \textit{Commit}: uma simples notificação para a máquina de destino informando o término do procedimento de cópia.
    \item Ativação: a MV é finalmente ativada no novo hospedeiro e a aplicação retoma sua execução.
\end{itemize}

Um dos problemas para a implementação de um sistema eficiente de elasticidade é a correta predição do tempo total de migração.
%
Existem vários parâmetros e métricas aplicáveis para essa predição, tais como a vazão do canal disponível, número de páginas modificadas e sobrecarga pré e pós migração~\cite{Akoush-2010}.
%
Neste cenário, Strunk (2012)~\cite{Strunk-2012} estabelece um algoritmo para se obter uma estimativa do tempo de migração levando em conta alguns destes parâmetros.

\begin{algorithm}[H]
	\SetKwInOut{Input}{input}
	\SetKwInOut{Output}{output}
\Indm  
  \Input{$m_{th}, v_{th}, p_{th}, v_{mem}, b, d, l$}
  \Output{Tempo de migração}
\Indp
let $v_0 = v_{mem}, v_{mig} = 0, t_{mig} = 0, t_{down} = 0$\;
\For{$i=0; i < m_{th}$}{
	$t_i = v_i/b$\;
	$v_{i+1} = t_i*d*l$\;
	$t_{mig} = t_{mig} + t_i$\;
	$v_{mig} = v_{mig} + v_i$\;
	\If{$v_{mig}>v_{th} \lor \frac{v_{i+1}}{l}<p_{th}$}{
		$break$\;
	}
}
$t_{down} = v_{i+1}/b$\;
$t_{mig} = t_{mig}+t_{down}$\;
return $t_{mig}$\;
\caption{Estimativa do tempo de migração \cite{Strunk-2012}}
\label{code:realoc}
\end{algorithm}

Neste algoritmo, estão presentes as fases de \textit{pre-copy} e \textit{stop-and-copy}, sendo que a reserva é feita pelo algoritmo que implementa a política de alocação e o \textit{commit} e a ativação pelo \textit{hypervisor}. 
%
No Algoritmo 1, $m_{th}$ representa o número máximo de iterações, $v_{th}$ a quantidade máxima de memória para ser migrada durante as fases de \textit{pre-copy}, $p_{th}$ a quantidade máxima aceitável de páginas sujas para uma iteração, $v_{mem}$ a memória provisionada para a VM, $b$ a quantidade de banda disponível para migração, $d$ o número de páginas sujas no momento da execução do algoritmo e $l$ o tamanho de cada página.

O algoritmo funciona calculando iterativamente a quantidade de RAM que necessitará ser copiada em iterações futuras (linhas 3 e 4). Caso os limiares de quantidade de páginas sujas ou quantidade máxima de RAM forem ultrapassados (linha 7), o algoritmo passa para a fase de \textit{stop-and-copy} (linha 9), em que a máquina encerra sua execução e o restante da RAM é copiada para a máquina destino.

\subsection{Redimensionamento de Recursos Virtuais}

O redimensionamento é a abordagem mais indicada por ser a menos custosa em relação ao tempo de adaptação~\cite{Sharma-2011}, podendo ser rapidamente realizado através de uma chamada ao MMV, requisitando o aumento ou diminuição da configuração (CPU, RAM, etc.) de uma MV. 
%
Entretanto, caso os recursos disponíveis na máquina não sejam suficientes, outras técnicas como replicação e migração precisam ser previamente aplicadas.

\subsection{Consolidação}

Embora a consolidação não seja uma abordagem estritamente elástica, caracteriza o processo de migração de diversas MVs para uma mesma máquina física.
%
Usualmente é aplicada para desligar servidores com capacidade ociosa, resultando em economia de energia.

\section{Alocação e Realocação de Infraestruturas Virtuais}

Tanto a alocação quanto a realocação de IVs são procedimentos que fazem parte da gama de tarefas gerenciais executadas por um provedor de nuvens.
%
Especificamente, alocação e realocação são guiadas por uma função objetivo bem definida. 
%
Para algoritmos de alocação, as estratégias de virtualização permitiram a exploração mais eficiente dos recursos disponíveis, melhorando as taxas de provisionamento de IVs ao permitir que diversos recursos virtuais sejam hospedados em um mesmo hospedeiro físico. 
%
A Figura~\ref{fig:2iv_alocacao} retrata este cenário: duas MVs (quadrados e triângulos) pertencentes a duas requisições de IVs (RIV) são alocadas em um mesmo hospedeiro físico (círculos).
%
Além disso, a virtualização de enlaces físicos permitiu a alocação de enlaces virtuais em caminhos físicos (caminhos na topologia de um \textit{datacenter}).

\begin{figure}[ht]
\centering
\includegraphics[width=.8\textwidth]{Imagens/2iv_alocacao.png}
\caption{Exemplo de alocação de IVs sobre um \textit{datacenter} de nuvem. Fonte:~\cite{Ruck-2014}}
\label{fig:2iv_alocacao}
\end{figure}

Além da virtualização, a eficiência da exploração dos recursos físicos está diretamente ligada à política de alocação implementada pelo provedor, que tem como finalidade identificar em quais máquinas físicas cada um dos recursos virtuais requisitados será alocado.
%
Ainda, para cada requisição de IV recebida, o provedor precisa optar entre duas alternativas para que essa escolha seja feita: (i) realocar todas as infraestruturas virtuais de modo a melhor comportar a nova requisição; ou (ii) manter as infraestruturas em seus respectivos nós físicos, evitando o descumprimento do SLA contratado~\cite{Ruck-2014}.
%
Em geral, podemos definir as diferenças entre alocação e realocação da seguinte forma:

\begin{enumerate}
    \item Alocação: um procedimento que deve ser mais rápido e, portanto, a opção apresentada em (i) pode não ser viável. Na alocação estão presentes as informações sobre recursos disponíveis no \textit{datacenter} e a requisição da IV que deve ser alocada. Assim, a IV apenas será alocada caso o \textit{datacenter} contenha recursos disponíveis para tal;
    \item Realocação: é um procedimento que requer maior processamento e, por consequência, seu tempo de execução é maior. Este procedimento considera todas as IVs já alocadas, buscando redistribuir os recursos virtuais de forma a otimizar uma função objetivo.
\end{enumerate}

É sabido que a escolha de quais recursos físicos devem hospedar as requisições de IVs não é uma tarefa trivial. Além de ser um problema NP-Difícil ~\cite{chowdhury:2010}, existem questões de interesse do provedor, tais como o aumento do rendimento (lucro) do provedor; minimização do consumo energético dos equipamentos físicos e a diminuição da fragmentação do \textit{datacenter}, que é calculada dividindo o número de MVs ativas pelo número de máquinas ligadas.
%
Por exemplo, a má gestão da alocação de recursos virtuais sobre recursos físicos pode acarretar na fragmentação do \textit{datacenter} e, consequentemente, na diminuição da taxa de aceitação de futuras requisições de alocação~\cite{Ruck-2014}.
%
A escolha dos algoritmos e políticas de alocação, da topologia da rede, do sistema de gerenciamento, estão entre os pontos chaves que garantam o sucesso de um provedor IaaS.

\subsection{Objetivos de Provedores e Usuários}

Para provedores existem diversas abordagens que são tradicionalmente buscadas, dentre elas pode-se citar: diminuição do custo, aumento do lucro e balanceamento de carga do \textit{datacenter}~\cite{survey-vne}.

A Figura \ref{fig:politica} retrata um cenário de diminuição de custo, contendo 2 máquinas físicas com 4 CPUs disponíveis cada e 2 requisições de alocação de IVs utilizando a política de alocação \textit{FCFS - First-Come First-Served}~\cite{Ruck-2014}.
%
Neste cenário, a segunda requisição (de 4 CPUs) não poderá ser atendida, já que a primeira requisição foi alocada no primeiro hospedeiro, restando apenas 3 CPUs disponíveis. 
%
Entretanto, caso o algoritmo \textit{Best-Fit} fosse adotado, as requisições seriam alocadas no nó que resultasse na menor quantidade de sobras para a máquina, ou seja, a requisição 1 seria alocada na máquina física 2, e assim haveria recursos disponíveis para a segunda requisição.

Caso a política de alocação não seja eficiente, para garantir a alocação e provisionamento da segunda requisição, seria necessário: (i) a ativação de novos servidores ou; (ii) a realocação das MVs já alocadas por meio de sucessivas migrações. 
%
A última alternativa, entretanto, não é uma tarefa trivial, pois é reduzido ao problema da mochila~\cite{cormen2009introduction}, que é um problema NP-Completo.
%
Embora simples, o cenário representado pela Figura~\ref{fig:politica} retrata a importância da implementação eficiente de políticas de alocação de IVs para diminuição do custo e aumento da taxa de aceitação de requisições, que reflete no lucro obtido por um provedor.
%
Além disso, algoritmos de realocação possuem importância na reorganização dos recursos alocados de modo a maximizar a taxa de aceitação de futuras requisições de alocação.

\FloatBarrier
\begin{figure}[ht]
\centering
\includegraphics[width=.9\textwidth]{Imagens/politica.png}
\caption{Impacto da política de alocação ao custo do \textit{datacenter}. Fonte: próprio autor.}
\label{fig:politica}
\end{figure}
\FloatBarrier

Para provedores de nuvem, a alocação com foco no balanceamento de carga minimiza a quantidade de gargalos presentes na rede, visto que evita a concentração dos recursos alocados, implicando diretamente no aumento do desempenho geral do \textit{datacenter}.
%
Ou seja, os recursos virtuais são distribuídos sobre o \textit{datacenter}, mesmo que seja necessário ativar servidores que estejam em modo econômico de energia.

Na visão dos usuários, pode-se citar como objetivo buscado o balanceamento de carga da aplicação e a minimização da distância entre os recursos virtuais. 
%
Em relação ao balanceamento de carga, a prática mais comum para escalar uma aplicação é a utilização de replicação \cite{Righi-2013}. 
%
Desse modo, haveria um servidor central chamado despachante que receberia as requisições e as encaminharia à réplica com menos carga de trabalho. 
%
Assim, quando outros limites fossem atingidos, mais réplicas seriam instanciadas e ligadas ao despachante \cite{Darolt-2016}. 
%
A diminuição da distância entre MVs, por outro lado, busca reduzir os caminhos físicos necessários para hospedar os canais virtuais, reduzindo assim a latência necessária para a comunicação entre as MVs~\cite{Ruck-2014}. 
%
Entretanto, a principal consequência, para o provedor, se dá pela criação de gargalos em certos pontos do \textit{datacenter}. 
%
Seguindo este modelo, as alocação de IVs criam nichos de recursos alocados, podendo implicar em taxas de aceitação menores, visto que nem sempre o melhor hospedeiro é o escolhido para a alocação.

Além dos objetivos citados, o presente trabalho aborda a diminuição do consumo energético de provedores IaaS, discutido no Capítulo~\ref{chap:realocacao} e motivado pelo provisionamento verde (Seção~\ref{sec:verde}).

\section{Provisionamento Verde de \ac{IVs}}\label{sec:green}
\label{sec:verde}

%Por exemplo, em \cite{Ruck-2014}, quatro algoritmos de alocação são implementados no simulador CloudSim, de maneira a comparar o seu desempenho em diversos cenários. Dentre os algoritmos, um deles busca a minimização do número de saltos entre os recursos virtuais alocados.

O provisionamento verde tem se tornado uma grande área de pesquisa em \textit{datacenters}, visto que a sua implementação está diretamente ligada a uma maior economia de energia. O provisionamento verde consiste na adoção de funções objetivo que reduzam o dano ambiental causado pelos mais diversos componentes de um \textit{datacenter}, como a refrigeração e a energia.
%
Em relação a este último, a principal abordagem para minimizar o seu consumo é o desligamento de máquinas redundantes \cite{duan-2016}.
%
Ou seja, quanto maior a possibilidade de consolidar MVs, respeitando o acordo de nível de serviço contratado, maior será a redução do consumo de energia. 

O conceito de provisionamento verde é benéfico para provedores e usuários por dois motivos principais: 
(i) o provedor não precisará custear o consumo mínimo de energia (necessário para manter o equipamento ativo) que não esteja sendo dividido pelo número de clientes e; 
(ii) o provedor conseguirá cobrar, de maneira justa, o consumo real de cada usuário. Hinz \textit{et al.} demonstram os seguintes pontos relacionados ao consumo energético de máquinas físicas e virtuais~\cite{hinz:2016}:

\begin{enumerate}
	\item O consumo energético mínimo é representativo, ou seja, existe um custo coletivo que deve ser considerado na tarifação e balanceamento de carga dos recursos;
    \item É possível estimar a quantidade de energia consumida observando-se a taxa de utilização do processador virtual;
    \item O consumo de energia proveniente do tráfego de rede é diferente se o par de comunicação está ou não hospedado na mesma máquina física;
	\item O consumo de energia é impactado pelo número de vCPUs ativas, independentedo número de vCPUs associadas às MVs.
\end{enumerate}

Para tarifar de maneira justa o consumo de energia, faz-se necessário estabelecer quais consumos, dentro de um hospedeiro, são consumos individuais e quais são coletivos. 
%
A Figura \ref{fig:energy} retrata os componentes do consumo energético de uma máquina virtualizada. Dentre os consumos apresentados, o consumo mínimo e o de gerenciamento fazem parte dos consumos coletivos.
%
O consumo mínimo é o consumo do MMV quando não existem MVs ativas. 
%
O consumo de gerenciamento, por outro lado, refere-se ao consumo das operações do MMV, como alocação, escalonamento de CPUs, etc.
%
Os outros consumos (entrada e saída e consumo de MVs) são individuais, ou seja, dependem unicamente da MV em execução.

\FloatBarrier
\begin{figure}[ht]
\centering
\includegraphics[width=.8\textwidth]{Imagens/energy.png}
\caption{Divisão do consumo de energia de um servidor. Fonte:~\cite{hinz:2016}}
\label{fig:energy}
\end{figure}
\FloatBarrier

A partir deste modelo, uma estratégia utilizada para minimizar o consumo energético do provedor e, assim, buscar o provisionamento verde é a de maximizar a quantidade de usuários que compartilham os mesmos custos coletivos, resultando em um cenário onde todos os recursos virtuais estão alocados e os consumos coletivos mínimo e de gerenciamento estão compartilhados em sua totalidade.
%
No caso de um cenário onde isso não aconteça, o restante dos recursos não compartilhados terão sua maior fatia custeada pelo provedor, gerando desperdício.
%
Assim, propor um modelo de realocação verde significa propor um modelo que considere o consumo realizado por usuários de maneira justa, além de buscar a minimização do consumo energético do provedor.

\section{Considerações Parciais}
\label{chap2:parciais}

O presente capítulo retratou a evolução do conceito de nuvens computacionais e a popularização das IVs que, por meio do modelo \textit{pay-as-you-go}, são usadas diariamente por empresas e usuários comuns.  
%
As IVs provisionadas por estes usuários hospedam os mais variados serviços, desde aplicações simples até mesmo as que exigem processamento intensivo de dados ou que possuem topologias complexas.  
%
Para suprir cargas de trabalho escaláveis destas aplicações, faz-se necessário o estudo de mecanismos de elasticidade como os abordados neste capítulo.

Além disso, para acompanhar o crescimento de \textit{datacenters}, é preciso transformar a preocupação pura com o desempenho para preocupações com o provisionamento verde e a precificação justa do consumo energético para usuários.  
%
Devido a grande preocupação com a eficiência energética de \textit{datacenters}, bem como o impacto ambiental causado pelo seu consumo, faz-se necessário o desenvolvimento de novos algoritmos que busquem minimizar o desperdício gerado por servidores e equipamentos que atuam sobre o calor gerado durante sua utilização.

A partir do modelo de tarifação proposto por Hinz et al.~\cite{hinz:2016}, conclui-se que o desperdício energético das máquinas físicas (consumos coletivos não-compartilhados), que fica às custas do provedor, é minimizado quanto maior for o número MVs provisionadas na máquina física. 

Os próximos capítulos abordam o contexto de realocação de IVs juntamente com o problema do consumo energético em \textit{datacenters}, propondo um mecanismo de realocação de IVs tendo este consumo como função objetivo.
%
Este mecanismo utiliza a estratégia de otimizar o compartilhamento de recursos coletivos, ou seja, buscando a realocação das MVs de modo a minimizar a sobra de recursos em um hospedeiro.
