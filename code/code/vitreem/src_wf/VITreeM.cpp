#include "VITreeM.h"

VITreeM::VITreeM(Tree _host, Tree _guest, Selection _selection_method, double _alpha):
	host(_host),
	guest(_guest),
    nodes_map(guest.graph.nodes.size(), -1),
    selection_method(_selection_method),
    alpha(_alpha)
{
    
    mapping(host.root, host.root, guest.root, guest.root, 0);
    if(is_valid()){
    	host.graph.cap_mtx = host.cap_mtx;
    	for(int u=0; u<(int)guest.adj_list.size(); u++){
    		for(int v : guest.adj_list[u]){
    			edges_map[u][v] = host.path_btw(nodes_map[u], nodes_map[v]);
    		}
    	}
    }
}

VITreeM::VITreeM()
{}

bool VITreeM::mapping(
    int p_host_node, int host_node, 
    int p_guest_node, int guest_node, 
    int carry_cost
){


    VI nodes_map_copy = nodes_map;
    Tree host_copy = host;

    if(step1(p_host_node, host_node, p_guest_node, guest_node, carry_cost))
        return true;

    nodes_map = nodes_map_copy;
    host = host_copy;
    
    if(not is_matchable(host_node, guest_node)) {
        return false;
    }

    // subtract node capacity from host_node
    host.graph.nodes[host_node].vcpu -= guest.graph.nodes[guest_node].vcpu;
    host.graph.nodes[host_node].ram -= guest.graph.nodes[guest_node].ram;
    host.graph.nodes[host_node].storage -= guest.graph.nodes[guest_node].storage;
    // map guest_node over host_node
    nodes_map[guest_node] = host_node;

    if(step2(p_host_node, host_node, p_guest_node, guest_node, carry_cost))
        return true;

    nodes_map = nodes_map_copy;
    host = host_copy;
    return false;
}

bool VITreeM::step1(
    int p_host_node, int host_node, 
    int p_guest_node, int guest_node, 
    int carry_cost
){
	VPII selection;

    //SuperNode guest_snode = guest.super_nodes[p_guest_node][guest_node];
    SuperNode guest_snode = guest.get_box(p_guest_node, guest_node);

	for(int h : host.adj_list[host_node]){
        //SuperNode h_snode = host.super_nodes[host_node][h];
        SuperNode h_snode = host.get_box(host_node, h);
        int h_edge_cap = host.cap_mtx[host_node][h];

        //cout << h_snode.alpha(guest_snode) << endl; 
		if(h_edge_cap >= carry_cost and h != p_host_node){
            /*if(h_snode.alpha(guest_snode) >= alpha )
				selection.push_back(PII(h, guest_node));*/
            if(alpha == 0.0){
                if(h_snode >= guest_snode){
                    selection.push_back(PII(h, guest_node));
                }
            }else{
                if(h_snode.raw_cost()/guest_snode.raw_cost() >= alpha){
                    selection.push_back(PII(h, guest_node));
                }
            }
        }
	}
	
	selection = selection_order(host_node, p_guest_node, selection);

	for(PII phg : selection){
        int h = phg.first;

        // decrement edge cap from host
        host.add_to_edge(host_node, h, -carry_cost);

        // decrement super node from host
        //host.super_nodes[host_node][h] -= guest_snode;
        
        // try map guest_node in other host node
		if(mapping(host_node, h, p_guest_node, guest_node, carry_cost))
			return true;

        // increment edge cap from host
        host.add_to_edge(host_node, h, carry_cost);
        // increment super node from host
        //host.super_nodes[host_node][h] += guest_snode;
	}
	return false;
}

bool VITreeM::step2(
    int p_host_node, int host_node, 
    int p_guest_node, int guest_node, 
    int carry_cost
){
    // select nodes that possible will be matched
    VPII selection; // (host, guest)
    // for each host_node neighbor
	for(int h : host.adj_list[host_node]){
        //SuperNode h_snode = host.super_nodes[host_node][h];
        SuperNode h_snode = host.get_box(host_node, h);
        int h_edge_cap = host.cap_mtx[host_node][h];

        // for each guest_node neighbor
        for(int g : guest.adj_list[guest_node]){
            //SuperNode g_snode = guest.super_nodes[guest_node][g];
            SuperNode g_snode = guest.get_box(guest_node, g);
            int g_edge_cap = guest.cap_mtx[guest_node][g];
            
            // check if host_neighbor and guest_neighbor can be mapped
            if(h_edge_cap >= g_edge_cap){
            	/*if(h_snode.alpha(g_snode) >= alpha )
					selection.push_back(PII(h, g));*/
            	if(alpha == 0.0){ // normal algorithm
            		if(h_snode >= g_snode){
                		selection.push_back(PII(h, g));
            		}
            	}else{ // with alpha
            		if(h_snode.raw_cost()/g_snode.raw_cost() >= alpha){
            			selection.push_back(PII(h, g));	
            		}
            	}
            }
        }
	}

    selection = selection_order(host_node, guest_node, selection);
   
    for(PII phg : selection){
        int h = phg.first;
        int h_edge_cap = host.cap_mtx[host_node][h];
        //SuperNode h_snode = host.super_nodes[host_node][h];
        SuperNode h_snode = host.get_box(host_node, h);

        int g = phg.second;
        int g_edge_cap = guest.cap_mtx[guest_node][g];
        //SuperNode g_snode = guest.super_nodes[guest_node][g];
        SuperNode g_snode = guest.get_box(guest_node, g);

        // if g was already map then do nothing
        double beta = h_snode.raw_cost()/g_snode.raw_cost();
        if( nodes_map[g] != -1 
        	or h_edge_cap < g_edge_cap
        	//or h_snode.alpha(g_snode) < alpha
            or (alpha == 0.0 and g_snode < g_snode)
            or (alpha != 0.0 and beta < alpha)
        ){
            continue;
        }

        // subtract guest_edge_cap from host tree
        host.add_to_edge(host_node, h, -g_edge_cap);
        // subtract g_snode from host super_nodes
        //host.super_nodes[host_node][h] -= g_snode;

        // try map guest node g
        if(not mapping(host_node, h, guest_node, g, g_edge_cap)){
            // if was not possible to map then recover the edge capacity
            host.add_to_edge(host_node, h, g_edge_cap);
            // if was not possible to map then recover the super node
            //host.super_nodes[host_node][h] += g_snode;
        }
    }

    // All neighbor of guest_node must be mapped
    for(int guest_neighbor : guest.adj_list[guest_node]){
        if(nodes_map[guest_neighbor] == -1) {
            return false;
	}
    }

    return true;
}

VPII VITreeM::selection_order(
    int p_host_node, int p_guest_node, VPII selection
){ 
    VPII vsort; // vector of pair<diff between raw_costs, vector_id> 
    VPII out; // final sorted vector of pair<host_node, guest_node>

    for(int i=0; i<(int)selection.size(); i++){
        int host_node = selection[i].first;
        int guest_node = selection[i].second;
        //SuperNode host_snode = host.super_nodes[p_host_node][host_node];
        SuperNode host_snode = host.get_box(p_host_node, host_node);
        //SuperNode guest_snode = guest.super_nodes[p_guest_node][guest_node];
        SuperNode guest_snode = guest.get_box(p_guest_node, guest_node);

        int diff_raw_cost = host_snode.raw_cost() - guest_snode.raw_cost();

        int c = (selection_method == Selection::BEST_FIT ? 1 : -1);
        vsort.push_back(PII(c * diff_raw_cost, i));
    }

	sort(all(vsort));
	for(PII pii : vsort){
		out.push_back(selection[pii.second]);
	}
	
	return out;
}

bool VITreeM::is_matchable(int host_node, int guest_node){
    int host_node_vcpu = host.graph.nodes[host_node].vcpu;
    int host_node_ram = host.graph.nodes[host_node].ram;
    int host_node_storage = host.graph.nodes[host_node].storage;
    int host_node_type = host.graph.nodes[host_node].type;

    int guest_node_vcpu = guest.graph.nodes[guest_node].vcpu;
    int guest_node_ram = guest.graph.nodes[guest_node].ram;
    int guest_node_storage = guest.graph.nodes[guest_node].storage;
    int guest_node_type = guest.graph.nodes[guest_node].type;
    if( host_node_vcpu < guest_node_vcpu or host_node_ram < guest_node_ram or host_node_storage < guest_node_storage or host_node_type != guest_node_type){
        return false;
    }

    return true;
}

bool VITreeM::is_valid(){
    for(int i=0; i<(int)nodes_map.size(); i++)
        if(nodes_map[i] == -1)
            return false;
    return nodes_map.size() > 0; 
}

ostream &operator<<(ostream &os, VITreeM &vt){
    os << "nodes_map\n";
    for(int i=0; i<(int)vt.nodes_map.size(); i++){
        os << "[" << i << "]: " << vt.nodes_map[i] << "\n";
    }
    os << "\nedges_map\n";
    for(const auto &pim : vt.edges_map){
        for(const auto &pivi : pim.second){
            os << "[" << pim.first << "][" << pivi.first << "]: ";
            os << pivi.second << "\n";
        }
    }
    os << "\ncost\n";
    for(VI row : vt.host.cap_mtx)
        os << row << "\n";
    return os;
}
