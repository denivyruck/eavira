#include "SuperNode.h"
// ptc = (pair type capacity)

SuperNode::SuperNode(Node _node): node_id(_node.id), edge_cap(0){
    node_types_vcpu[_node.type] = _node.vcpu;
    node_types_ram[_node.type] = _node.ram;
    node_types_storage[_node.type] = _node.storage;
}


SuperNode::SuperNode(): node_id(-1), edge_cap(0)
{}

void SuperNode::operator+=(const SuperNode &other){
	edge_cap += other.edge_cap;
	for(const pair<int,int> &ptc : other.node_types_vcpu){
		node_types_vcpu[ptc.first] += ptc.second;
	}

	for(const pair<int,int> &ptc : other.node_types_ram){
                node_types_ram[ptc.first] += ptc.second;
        }

	for(const pair<int,int> &ptc : other.node_types_storage){
                node_types_storage[ptc.first] += ptc.second;
        }

}

void SuperNode::operator-=(const SuperNode &other){
	edge_cap -= other.edge_cap;
	for(const pair<int,int> &ptc : other.node_types_vcpu){
		node_types_vcpu[ptc.first] -= ptc.second;
        	if(node_types_vcpu[ptc.first] == 0) {
            		node_types_vcpu.erase(ptc.first);
		}
	}

	for(const pair<int,int> &ptc : other.node_types_ram){
                node_types_ram[ptc.first] -= ptc.second;
                if(node_types_ram[ptc.first] == 0) {
                        node_types_ram.erase(ptc.first);
                }
        }

	for(const pair<int,int> &ptc : other.node_types_storage){
                node_types_storage[ptc.first] -= ptc.second;
                if(node_types_storage[ptc.first] == 0) {
                        node_types_storage.erase(ptc.first);
                }
        }
}

bool SuperNode::operator>=(const SuperNode &other) const {
	for(const PII &ptc : other.node_types_vcpu){
        	if(node_types_vcpu.count(ptc.first) == 0) {
            		return false;
		}
        	if(ptc.second > node_types_vcpu.at(ptc.first)) {
            		return false;
		}
	}
	for(const PII &ptc : other.node_types_ram){
                if(node_types_ram.count(ptc.first) == 0) {
                        return false;
                }
                if(ptc.second > node_types_ram.at(ptc.first)) {
                        return false;
                }
        }
	for(const PII &ptc : other.node_types_storage){
                if(node_types_storage.count(ptc.first) == 0) {
                        return false;
                }
                if(ptc.second > node_types_storage.at(ptc.first)) {
                        return false;
                }
        }

	return edge_cap >= other.edge_cap;
}

bool SuperNode::operator<(const SuperNode &other) const {
	return not (*this >= other);
}


bool SuperNode::operator==(const SuperNode &other) const {
    if(other.node_types_vcpu.size() != node_types_vcpu.size())
        return false;
    if(other.node_types_ram.size() != node_types_ram.size())
        return false;
    if(other.node_types_storage.size() != node_types_storage.size())
        return false;   
 
    for(PII sn : node_types_vcpu){
        if(other.node_types_vcpu.count(sn.first) == 0)
            return false;
        if(other.node_types_vcpu.at(sn.first) != sn.second)
            return false;
    }

    for(PII sn : node_types_ram){
        if(other.node_types_ram.count(sn.first) == 0)
            return false;
        if(other.node_types_ram.at(sn.first) != sn.second)
            return false;
    }

    for(PII sn : node_types_storage){
        if(other.node_types_storage.count(sn.first) == 0)
            return false;
        if(other.node_types_storage.at(sn.first) != sn.second)
            return false;
    }
    return edge_cap == other.edge_cap;
}

double SuperNode::alpha(const SuperNode &other){
	double ret = (double)edge_cap/other.edge_cap;

	for(const PII &ptc : other.node_types_vcpu){
	        if(node_types_vcpu.count(ptc.first) == 0)
        		ret = min(ret, 0.0);
       	 	else
        		ret = min(ret, (double)node_types_vcpu[ptc.first]/ptc.second);
	}
	
	for(const PII &ptc : other.node_types_ram){
        	if(node_types_ram.count(ptc.first) == 0)
                	ret = min(ret, 0.0);
        	else
                	ret = min(ret, (double)node_types_ram[ptc.first]/ptc.second);
        }

	for(const PII &ptc : other.node_types_storage){
        	if(node_types_storage.count(ptc.first) == 0)
                	ret = min(ret, 0.0);
        	else
                	ret = min(ret, (double)node_types_storage[ptc.first]/ptc.second);
        }


	return ret;
}

double SuperNode::raw_cost(){
	int value = edge_cap;

	for(const pair<int,int> &ptc : node_types_vcpu)
		value += ptc.second;

        for(const pair<int,int> &ptc : node_types_ram)
                value += ptc.second;

        for(const pair<int,int> &ptc : node_types_storage)
                value += ptc.second;

	return value;
}

ostream& operator<<(ostream &os, const SuperNode sn){
	os << "(";
	bool f = true;
	for(const pair<int,int> &pii : sn.node_types_vcpu){
		if(!f) 
			os << ", ";
		f = false;
		os << "[" << pii.first << "]: " << pii.second;
	}

	for(const pair<int,int> &pii : sn.node_types_ram){
                if(!f)
                        os << ", ";
                f = false;
                os << "[" << pii.first << "]: " << pii.second;
        }

	for(const pair<int,int> &pii : sn.node_types_storage){
                if(!f)
                        os << ", ";
                f = false;
                os << "[" << pii.first << "]: " << pii.second;
        }

	return os << ") link: " << sn.edge_cap;
}
