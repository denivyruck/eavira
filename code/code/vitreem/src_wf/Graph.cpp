#include "Graph.h"

Graph::Graph(vector<Node> _nodes, vector<Edge> _edges):
	nodes(_nodes)
{

    // sorting points by their index's
	sort(nodes.begin(), nodes.end());

	// creating adjacence list 
	adj_list.assign(nodes.size(), VI());

    // creating capacities matrix
    cap_mtx.assign(nodes.size(),VI(nodes.size(),0));
	
	for(Edge &edge : _edges){
		// nodes in edges
		int u = edge.u;
		int v = edge.v;
        
	        assert(u>=0 and u<(int)nodes.size());
        	assert(v>=0 and v<(int)nodes.size());
        
		// create adjacence list;
		adj_list[u].push_back(v);
		adj_list[v].push_back(u);

       		cap_mtx[u][v] = edge.cap;
        	cap_mtx[v][u] = edge.cap;
	}
}

Graph::Graph()
{}

void Graph::add_edge(Edge edge){
    if(edge.u != edge.v and cap_mtx[edge.u][edge.v] == 0){
        adj_list[edge.u].push_back(edge.v);
        cap_mtx[edge.u][edge.v] = edge.cap;

        adj_list[edge.v].push_back(edge.u);
        cap_mtx[edge.v][edge.u] = edge.cap;
    }
}

void Graph::remove_edge(Edge edge){
    auto it = find(all(adj_list[edge.u]), edge.v);
    adj_list[edge.u].erase(it);
    cap_mtx[edge.u][edge.v] = 0;

    it = find(all(adj_list[edge.v]), edge.u);
    adj_list[edge.v].erase(it);
    cap_mtx[edge.v][edge.u] = 0;
}

void Graph::connected_dfs(int u, VI &vis){
    vis[u] = 1;
    for(int v : adj_list[u]){
        if(!vis[v])
            connected_dfs(v, vis);
    }
}

bool Graph::connected(){
	unsigned int i = 0;
	unsigned int ct = 0;

	for (i = 0; i < nodes.size(); i++) {
		ct = 0;
    		VI vis(nodes.size(), i);
    		connected_dfs(0, vis);
    		for(int visited : vis) {
        		if(visited) {
            			ct++;
			}
		}
		if (ct == nodes.size())
			return true;
	}
    return false;
}

void Graph::export_elasticity_dat(const char *out_file, int id, int interval) {
	ofstream ofs;

	//update filename
	stringstream ss;
	ss << out_file << "-" << id << "-" << interval;	

	ofs.open(ss.str());

	//nodes 
	ofs << nodes.size() << endl;
	for (int u=0; u<(int)nodes.size();u++) {
		ofs << u << " "<< nodes[u].vcpu << " " << nodes[u].ram << " " << nodes[u].storage << " " << nodes[u].type << endl;
	}

	//links
	//discover the real number
	int cont = 0;
	for(int u=0; u<(int)nodes.size(); u++){
                for(int v : adj_list[u]){
                        if(u > v)
                                continue;
                        cont++;
                }
        }      
	
	ofs << cont << endl;

	for(int u=0; u<(int)nodes.size(); u++){
                for(int v : adj_list[u]){
                        if(u > v)
                                continue;
                        //print it
                        int cap = cap_mtx[u][v];
                        ofs << u << " " << v << " " << cap << endl;
                }
        }

	ofs.close();
}

bool Graph::operator==(const Graph &other) const {
    return adj_list == other.adj_list and cap_mtx == other.cap_mtx;
}

