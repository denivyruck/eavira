#include "Utils.h"

int rand_btw(int a, int b){
    static random_device rd;
    static mt19937 gen(rd());
    uniform_int_distribution<int> dist(a, b);
	return dist(gen);
}

int rand_btw(PII n){
    return rand_btw(n.first, n.second);
}

double stime(){
    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv, &tz);
    double mlsec = 1000.0 * ((double)tv.tv_sec + (double)tv.tv_usec/1000000.0);
    return mlsec/1000.0;
}

ostream &operator<<(ostream &os, const VI &v){
    os << "[";
    for(int i=0; i<(int)v.size(); i++){
        if(i)
            os << ", ";
        os << v[i];
    }
    os << "]";
    return os;
}

