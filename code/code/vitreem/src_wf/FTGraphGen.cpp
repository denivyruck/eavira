#include "FTGraphGen.h"

FTGraphGen::FTGraphGen(Config config): config(config){
    assert(config.n_layers.first >= 2);
    assert(config.n_layers.second <= int(config.layers.size()));
}

Graph FTGraphGen::generate(){
    vector<Node> nodes;
    vector<Edge> edges;

    VPII lnodes_id(config.layers.size()); // nodes_id by layers
    PII nodes_id;

    int n_layers = rand_btw(this->config.n_layers);
    int lini = int(config.layers.size()) - n_layers;
    int lend = int(config.layers.size()) - 1;

    for(int l=lini; l<=lend; l++){
        Layer &layer = config.layers[l];
        int ln_nodes = rand_btw(layer.n_nodes);

        if(l == lini)
            nodes_id.first = 0;
        else
            nodes_id.first = lnodes_id[l-1].second + 1;
        nodes_id.second = nodes_id.first + ln_nodes - 1;
        lnodes_id[l] = nodes_id;

        for(int id=nodes_id.first; id<=nodes_id.second; id++){
            int type = rand_btw(layer.node_type);
            int vcpu = rand_btw(layer.node_vcpu);
            int ram = rand_btw(layer.node_ram);
            int storage = rand_btw(layer.node_storage);
            nodes.push_back(Node(id, type, vcpu, ram, storage));
        }
    }

    Graph graph(nodes, edges);

    // nodes on first layer are connected between then
    for(int u=lnodes_id[lini].first; u <= lnodes_id[lini].second - 1; u++){
        int cap = rand_btw(config.layers[lini].edge_cap);
        graph.add_edge(Edge(u, u+1, cap)); 
    }

    
    for(int l=lini; l<=lend - 2; l++){
        Layer &layer = config.layers[l];

        for(int u=lnodes_id[l].first; u<=lnodes_id[l].second; u++){
            for(int v=lnodes_id[l+1].first; v<=lnodes_id[l+1].second; v++){
                int cap = rand_btw(layer.edge_cap);
                if(rand_btw(0,1) == 1)
                    graph.add_edge(Edge(u, v, cap));
            }
        }
    }

    // TORs Layer
    Layer &ltor = config.layers[lend - 1]; 
    PII tors_id = lnodes_id[lend - 1];
    int tor_nn = tors_id.second - tors_id.first + 1;

    // Servers Layer
    PII servers_id = lnodes_id[lend];
    int server_nn = servers_id.second - servers_id.first + 1;

    int servers_per_tor = server_nn/tor_nn;

    for(int u=tors_id.first, i=0; u<=tors_id.second; u++, i++){
        int ini = servers_id.first + i*servers_per_tor;
        int end = ini + servers_per_tor - 1;
        for(int v=ini; v<=end; v++){
            int cap = rand_btw(ltor.edge_cap);
            graph.add_edge(Edge(u, v, cap));
        }
    }

    int remainder = server_nn%tor_nn;
    for(int u=tors_id.first, i=0; i<remainder; u++, i++){
        int v = servers_id.second - remainder + i + 1;
        int cap = rand_btw(ltor.edge_cap);
        graph.add_edge(Edge(u, v, cap));
    }

     while(not graph.connected()){
        for(int l=lini; l<=lend - 2; l++){
            Layer &layer = config.layers[l];

            for(int u=lnodes_id[l].first; u<=lnodes_id[l].second; u++){
                for(int v=lnodes_id[l+1].first; v<=lnodes_id[l+1].second; v++){
                    int cap = rand_btw(layer.edge_cap);
                    if(rand_btw(0,1) == 1)
                        graph.add_edge(Edge(u, v, cap));
                }
            }
        }
    }

    /*while(not graph.connected()){
        int l1 = rand_btw(lini, lend);
        int l2 = rand_btw(lini, lend);
        int u = rand_btw(lnodes_id[l1]);
        int v = rand_btw(lnodes_id[l2]);
        graph.add_edge(Edge(u, v, rand_btw(config.layers[l1].edge_cap)));
    }*/

    return graph;
}

