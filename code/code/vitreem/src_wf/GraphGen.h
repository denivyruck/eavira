#ifndef __GRAPH_GEN__
#define __GRAPH_GEN__

#include "StdLib.h"
#include "Graph.h"
#include "tinyxml2.h"
#include <string>
#include <iostream>
#include <sstream>

using namespace tinyxml2;

#ifndef XMLCheckResult
#define XMLCheckResult(a_eResult) if (a_eResult != XML_SUCCESS) { printf("Error: %i\n", a_eResult); ; }
#endif

class GraphGen{
public:
    virtual Graph generate() = 0;

    tuple<Graph, int, int> load_from_file_dat(const char *filename) {
	vector<Node> nodes;
        vector<Edge> edges;

	ifstream ifs;
        ifs.open(filename);	

	string line;
	
	// first line is the number of nodes
	getline(ifs, line);
	int nnodes = stoi(line);
	
	//each line is a node til nnodes
	for (int i = 0; i < nnodes; i++) {
		getline(ifs, line);
		istringstream iss(line);

		string sub;
	
		iss >> sub;
		// node id
		int nid = stoi(sub);
		iss >> sub;
		// cpu		
		int vcpu = stoi(sub);
		//ram 
		iss >> sub;
		int ram = stoi(sub);
		//storage
		iss >> sub;
		int storage = stoi(sub);
		iss >> sub;
		//type (node or switch)
		int type = stoi(sub);

		//create a node
		nodes.push_back(Node(nid, type, vcpu, ram, storage));	

	}

	//next line says the number of links to load
	getline(ifs, line);
        int nlinks = stoi(line);	
	
	//each line is a link
	for (int i = 0; i < nlinks; i++) {
		getline(ifs, line);
                istringstream iss(line);

                string sub;
                iss >> sub;
		//source
		int source = stoi(sub);
                iss >> sub;
		//destination
		int destination = stoi(sub);
		//bw
                iss >> sub;
		int bw = stoi(sub);
		//if (bw > 0) {
		edges.push_back(Edge(source, destination, bw));
		//}
	}

	ifs.close();	

        return make_tuple(Graph(nodes, edges), 1, 2);

    }

    tuple<Graph, int, int> load_from_file_xml(const char *filename){
        vector<Node> nodes;
        vector<Edge> edges;

        XMLDocument doc;
        XMLError eResult = doc.LoadFile(filename);
        XMLCheckResult(eResult);

        XMLNode *pRoot = doc.FirstChildElement("description");
        if (pRoot == nullptr) {cout << "NULL pRoot" << endl; }

/*      XMLPrinter printer;
        doc.Accept( &printer );
        const char* xmlcstr = printer.CStr();
        cout << xmlcstr << endl;
*/

        // Lets load all nodes
        XMLElement *pVI = pRoot->FirstChildElement("virtualInfrastructure");
        if (pVI == nullptr) {cout << "NULL virtualInfrastructure" << endl; }
        XMLElement *pListNode = pVI->FirstChildElement("vNode");
        if (pListNode == nullptr) {cout << "NULL vNode" << endl; }
        map<std::string, int> mapOfIds;
        int count = 0;
        while (pListNode != nullptr) {
                XMLElement *pValue = pListNode->FirstChildElement("vntype");
                int type;
                if (strcmp(pValue->GetText(), "node") == 0) {
                        type = 0;
                } else {
                        type = 1;
                }
                XMLElement *pCPU = pListNode->FirstChildElement("cpu");
                XMLElement *pCores = pCPU->FirstChildElement("cores");
                XMLElement *pSimple = pCores->FirstChildElement("simple");
                int vcpu;
                pSimple->QueryIntText(&vcpu);
		
 		XMLElement *pRAM = pListNode->FirstChildElement("memory");
                pSimple = pRAM->FirstChildElement("simple");
                int ram;
                pSimple->QueryIntText(&ram);

 		XMLElement *pStorage = pListNode->FirstChildElement("storage");
                pSimple = pStorage->FirstChildElement("simple");
                int storage;
                pSimple->QueryIntText(&storage);

		XMLElement *pID = pListNode->FirstChildElement("id");
                string id = pID->GetText();

                mapOfIds.insert(make_pair(id, count));

                nodes.push_back(Node(count, type, vcpu, ram, storage));

                pListNode = pListNode->NextSiblingElement("vNode");
                count++;
        }

        // Now lets load all links
        XMLElement *pListLink = pVI->FirstChildElement("vLink");
        if (pListLink == nullptr) {cout << "NULL vLink" << endl; }
        while (pListLink != nullptr) {
                XMLElement *pSource = pListLink->FirstChildElement("source");
                XMLElement *pDestination = pListLink->FirstChildElement("destination");
                string ssource = pSource->GetText();
                string sdestination = pDestination->GetText();

                int source = mapOfIds.at(ssource);
                int destination = mapOfIds.at(sdestination);
                XMLElement *pBw = pListLink->FirstChildElement("bandwidth");
                XMLElement *pForward = pBw->FirstChildElement("forward");
                XMLElement *pSimple = pForward->FirstChildElement("simple");
                int cap;
                pSimple->QueryIntText(&cap);

                edges.push_back(Edge(source, destination, cap));
                pListLink = pListLink->NextSiblingElement("vLink");
                count++;
        }
        int start;
        int end;
        XMLElement *pStart = pVI->FirstChildElement("startInterval");
        pStart->QueryIntText(&start);
        XMLElement *pEnd = pVI->FirstChildElement("endInterval");
        pEnd->QueryIntText(&end);

        return make_tuple(Graph(nodes, edges), start, end);
    }	
};

#endif
