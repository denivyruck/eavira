#ifndef __PIMANAGER__
#define __PIMANAGER__
#include "StdLib.h"
#include "Graph.h"
#include "Tree.h"
#include "VITreeM.h"
#include "Utils.h"

// Physical Infrastructure Manager
class PIManager{
public:
	PIManager(
        Graph _phy, 
        Tree::Root _root_type, 
        VITreeM::Selection _selection_method,
        double _alpha
    );
    PIManager();

	bool allocate(int vid, Graph vi);
	
	bool deallocate(int vid, Graph vi);

	Graph phy;
    Tree::Root root_type;
    VITreeM::Selection selection_method;

    Graph original_phy;
	map<int, VITreeM> mappings;

    double fragmentation();
    double time_mapping();
    double time_mapping_individual();
    double acceptance();
    double node_load_vcpu();
    double node_load_ram();
    double node_load_storage();
    double link_load();
    double status();
    double revenue_individual();
    double revenue();

    void export_elasticity_mapping_dat(int vid, Graph vi, const char *elasticityMapping, int interval);
private:
    int vi_count;
    int vi_accepted;
    double total_time_mapping;
    double alpha;
    double allocated;
    double ind_frag;
    double ind_time_mapping;
    double revenue_ind;
    double revenue_total;
};

#endif 
