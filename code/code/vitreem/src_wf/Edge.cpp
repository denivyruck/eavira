#include "Edge.h"

Edge::Edge(int _u, int _v, int _cap):
    u(_u),
    v(_v),
	cap(_cap)
{
    if(u > v)
        swap(u, v);
}

bool Edge::operator<(const Edge &other) const{
    if(u != other.u)
        return u < other.u;
    return v < other.v;
}

bool Edge::operator==(const Edge &other) const{
    return u == other.u and v == other.v;
}
