#include "AmazonGraphGen.h"

AmazonGraphGen::AmazonGraphGen(Config config):
    config(config)
{}

Graph AmazonGraphGen::generate(){
    vector<Node> nodes;
    vector<Edge> edges;

    int n_nodes = rand_btw(config.n_nodes);

    nodes.push_back(Node(0, 1, rand_btw(config.node_vcpu), rand_btw(config.node_ram), rand_btw(config.node_storage)));
    for(int u=1; u<1+n_nodes; u++)
        nodes.push_back(Node(u, 0, rand_btw(config.node_vcpu), rand_btw(config.node_ram), rand_btw(config.node_storage)));

    Graph graph(nodes, edges);

    for(int u=1; u<1+n_nodes; u++)
        graph.add_edge(Edge(0, u, rand_btw(config.edge_cap)));

    return graph;
}

