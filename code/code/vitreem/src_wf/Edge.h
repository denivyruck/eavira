#ifndef __EDGE__
#define __EDGE__

#include "Node.h"

class Edge{
public:
    Edge(int _u, int _v, int _cap);

    int u, v; // nodes u, v
	int cap; //edge capacity

    bool operator<(const Edge &other) const;
    bool operator==(const Edge &other) const;
private:	
};

#endif
