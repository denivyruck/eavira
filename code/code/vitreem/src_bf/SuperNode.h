#ifndef __SUPER_NODE__
#define __SUPER_NODE__

#include "StdLib.h"
#include "Node.h"

class SuperNode{
public:
	SuperNode(Node _node);
	SuperNode();
	void operator+=(const SuperNode &other);
    	void operator-=(const SuperNode &other);
	bool operator>=(const SuperNode &other) const;
    	bool operator<(const SuperNode &other) const;
    	bool operator==(const SuperNode &other) const;
	double raw_cost();
	double alpha(const SuperNode &other);

    	int node_id;
	map<int,int> node_types_vcpu;
	map<int,int> node_types_ram;
	map<int,int> node_types_storage;
	int edge_cap;
private:
};

ostream &operator<<(ostream &os, const SuperNode sn);

#endif
