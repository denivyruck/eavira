#include "Simulator.h"

wstring to_wstring(string s){
    return wstring(all(s));
}

void Simulator::run(
        GraphGen *pigen, 
        GraphGen *vigen,
        int n_vis,
        PII interval,
        string out_filename,
	int load_from_file,
	const char *physicalFile,
	const char *virtualPath,
	double alpha,
	const char *elasticityVirtual,
        const char *elasticityPhysical,
	int generate_dat,
	const char *elasticityMapping
){
    vector<Graph> vis;
    vector<Event> events;
    Graph phy;
  
    cout << "load from file " << load_from_file << endl;
 
    if (load_from_file == 1) {
	tuple<Graph, int, int> ltp = pigen->load_from_file_xml(physicalFile);
	phy = get<0>(ltp);
	for(int vid=0; vid<n_vis; vid++){
	    char virtualFile[200];
	    strcpy(virtualFile, virtualPath);
	    sprintf(virtualFile, "%s%d", virtualFile, vid);
	    strcat(virtualFile, ".xml");

	    tuple<Graph, int, int> ltv = vigen->load_from_file_xml(virtualFile);
            vis.push_back(get<0>(ltv));
            int ini = get<1>(ltv);
            int end = get<2>(ltv);
            events.push_back(Event(vid, ini, 1)); // in event
            events.push_back(Event(vid, end, 0)); // out event
        }
    } else if (load_from_file == 2) {
        tuple<Graph, int, int> ltp = pigen->load_from_file_dat(physicalFile);
        phy = get<0>(ltp);
        for(int vid=0; vid<n_vis; vid++){
            char virtualFile[200];
            strcpy(virtualFile, virtualPath);
            sprintf(virtualFile, "%s%d", virtualFile, vid);
            strcat(virtualFile, ".dat");

            tuple<Graph, int, int> ltv = vigen->load_from_file_dat(virtualFile);

            vis.push_back(get<0>(ltv));
            int ini = get<1>(ltv);
            int end = get<2>(ltv);
            events.push_back(Event(vid, ini, 1)); // in event
            events.push_back(Event(vid, end, 0)); // out event
        }

    } else {
 	phy = pigen->generate();
        // gen nvi virtual infrastructure
        for(int vid=0; vid<n_vis; vid++){
            vis.push_back(vigen->generate());
            int delta = rand_btw(1, 30);
            int ini = rand_btw(1, interval.second - delta);
            int end = rand_btw(ini + 1, ini + delta);
            events.push_back(Event(vid, ini, 1)); // in event
            events.push_back(Event(vid, end, 0)); // out event
        }
    }
    sort(all(events));
    wstring description = L"";
    
    JSONObject root;
    root[L"description"] = new JSONValue(description);

    JSONObject jtime_mapping;
    jtime_mapping = create_subgraphic(L"Tempo de Mapeamento", L"periodo",
            L"Tempo de Mapeamento (s)");

    JSONObject jfragmentation;
    jfragmentation = create_subgraphic(L"Fragmentação", L"periodo",
            L"Fragmentação (%)");
//, interval, PII(0, 110));

    JSONObject jacceptance;
    jacceptance = create_subgraphic(L"Aceitação", L"periodo",
            L"Aceitação (%)");
//, interval, PII(0, 110));

    JSONObject jnodevcpuload;
    jnodevcpuload = create_subgraphic(L"Carga dos nós (vCPU)", L"periodo",
            L"Carga dos nós vCPU (%)");
//, interval, PII(0, 110));

    JSONObject jnoderamload;
    jnoderamload = create_subgraphic(L"Carga dos nós (RAM)", L"periodo",
            L"Carga dos nós RAM (%)");

    JSONObject jnodestorageload;
    jnodestorageload = create_subgraphic(L"Carga dos nós (Storage)", L"periodo",
            L"Carga dos nós Storage (%)");

    JSONObject jlinkload;
    jlinkload = create_subgraphic(L"Carga dos canais", L"periodo",
            L"Carga dos canais (%)");
//, interval, PII(0, 110));

    JSONObject jrevenue;
    jrevenue = create_subgraphic(L"Revenue", L"periodo", L"Revenue");
//, interval, PII(0, 110));

    JSONObject jrevenueind;
    jrevenueind = create_subgraphic(L"Revenue Ind", L"periodo", L"Revenue Ind");
//, interval, PII(0, 110));

    JSONObject jstatus;
    jstatus = create_subgraphic(L"Status", L"periodo", L"Status");
//, interval, PII(0, 110));

    JSONObject jtimeind;
    jtimeind = create_subgraphic(L"Tempo de Mapeamento Ind", L"periodo", L"Tempo de Mapeamento Ind (s)");
//, interval, PII(0, 110));

    vector< tuple<Tree::Root, VITreeM::Selection, double> > params = {
        make_tuple(Tree::Root::LRC, VITreeM::Selection::BEST_FIT, alpha),
//        make_tuple(Tree::Root::LRC, VITreeM::Selection::WORST_FIT, alpha),
//        make_tuple(Tree::Root::CENTER, VITreeM::Selection::BEST_FIT, alpha),
//        make_tuple(Tree::Root::CENTER, VITreeM::Selection::WORST_FIT, alpha),
    };

    vector<string> param_root = { "LRC | ", "CENTER | " };
    vector<string> param_selection = { "BEST_FIT | ", "WORST_FIT | " };
    vector<string> param_alpha = { "NORMAL", "ALPHA 25%", "ALPHA 50%"  };

    JSONObject curve;
    JSONArray points;

    JSONArray tcurves, ocurves, fcurves, acurves, nlvcpucurves, nlramcurves, nlstoragecurves, llcurves, revenuecurves, statuscurves, timeindcurves, revenueindcurves;

    total = params.size() * events.size();
    current = 0;

    map<string, vector<PDD> > stats;
    for(auto param : params){
        string name = param_root[(int)get<0>(param)];
        name += param_selection[(int)get<1>(param)];
        name += param_alpha[(get<2>(param) == 0.0 ? 0 :(get<2>(param) == 0.25 ? 1 : 2))];

        curve[L"name"] = new JSONValue(to_wstring(name));

        cout << name << endl;
        auto stats = simulate(phy, vis, events, get<0>(param), get<1>(param), get<2>(param), elasticityVirtual, elasticityPhysical, generate_dat, elasticityMapping);

        // TIME MAPPING
        points.clear();
        for(PDD pdd : stats["time_mapping"]){
            JSONValue *x = new JSONValue((double)pdd.first);
            JSONValue *y = new JSONValue((double)pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        tcurves.push_back( new JSONValue(curve) );
        
	// ACCEPTANCE
        points.clear();
        for(PDD pdd : stats["acceptance"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        acurves.push_back( new JSONValue(curve) );

        // FRAGMENTATION
        points.clear();
        for(PDD pdd : stats["fragmentation"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        fcurves.push_back( new JSONValue(curve) );

        // NODE LOAD
        points.clear();
        for(PDD pdd : stats["node_load_vcpu"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        nlvcpucurves.push_back( new JSONValue(curve) );

	points.clear();
        for(PDD pdd : stats["node_load_ram"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        nlramcurves.push_back( new JSONValue(curve) );

	points.clear();
        for(PDD pdd : stats["node_load_storage"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        nlstoragecurves.push_back( new JSONValue(curve) );

        // LINK LOAD
        points.clear();
        for(PDD pdd : stats["link_load"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        llcurves.push_back( new JSONValue(curve) );

        // REVENUE
        points.clear();
        for(PDD pdd : stats["revenue"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        revenuecurves.push_back( new JSONValue(curve) );

	// REVENUE IND
        points.clear();
        for(PDD pdd : stats["revenue_ind"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        revenueindcurves.push_back( new JSONValue(curve) );


 	// STATUS
        points.clear();
        for(PDD pdd : stats["status"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        statuscurves.push_back( new JSONValue(curve) );

	 // TIME_IND
        points.clear();
        for(PDD pdd : stats["time_ind"]){
            JSONValue *x = new JSONValue(pdd.first);
            JSONValue *y = new JSONValue(pdd.second);
            JSONArray point = {x, y};
            points.push_back(new JSONValue(point));
        }
        curve[L"points"] = new JSONValue(points);
        timeindcurves.push_back( new JSONValue(curve) );


    }

    jtime_mapping[L"curves"] = new JSONValue(tcurves);
    jacceptance[L"curves"] = new JSONValue(acurves);
    jfragmentation[L"curves"] = new JSONValue(fcurves);
    jnodevcpuload[L"curves"] = new JSONValue(nlvcpucurves);
    jnodestorageload[L"curves"] = new JSONValue(nlstoragecurves);
    jnoderamload[L"curves"] = new JSONValue(nlramcurves);
    jlinkload[L"curves"] = new JSONValue(llcurves);
    jrevenue[L"curves"] = new JSONValue(revenuecurves);
    jrevenueind[L"curves"] = new JSONValue(revenueindcurves);
    jstatus[L"curves"] = new JSONValue(statuscurves);
    jtimeind[L"curves"] = new JSONValue(timeindcurves);

    JSONArray subgs = {
        new JSONValue(jtime_mapping),
        new JSONValue(jacceptance),
        new JSONValue(jfragmentation),
        new JSONValue(jnodevcpuload),
        new JSONValue(jnoderamload),
        new JSONValue(jnodestorageload),
        new JSONValue(jlinkload),
        new JSONValue(jrevenue),
        new JSONValue(jrevenueind),
        new JSONValue(jstatus),
        new JSONValue(jtimeind)
    };

    root[L"subgraphics"] = new JSONValue(subgs);

    wofstream file(out_filename);

    JSONValue *fstats = new JSONValue(root);

    file << fstats->Stringify();

    file.close();
}

JSONObject create_subgraphic(
    wstring title,
    wstring xlabel,
    wstring ylabel,
    PII xaxis,
    PII yaxis
){
    JSONObject out;
    out[L"title"] = new JSONValue(title.c_str());
    out[L"xlabel"] = new JSONValue(xlabel.c_str());
    out[L"ylabel"] = new JSONValue(ylabel.c_str());
    out[L"curves"] = new JSONValue(JSONArray());

    if(xaxis.first == -1 or yaxis.first == -1)
        return out;

    JSONArray axis = {
        new JSONValue((double)xaxis.first),
        new JSONValue((double)xaxis.second),
        new JSONValue((double)yaxis.first),
        new JSONValue((double)yaxis.second)
    };
    out[L"axis"] = new JSONValue(axis);
    return out;
}

map<string, vector<PDD> > Simulator::simulate(
    Graph phy,
    vector<Graph> vis,
    vector<Event> events,
    Tree::Root root_type,
    VITreeM::Selection selection_type,
    double alpha, 
    const char *elasticityVirtual,
    const char *elasticityPhysical,
    int generate_dat,
    const char *elasticityMapping
){
    double x,y;
    PIManager pimanager(phy, root_type, selection_type, alpha);
    map<string, vector<PDD> > stats;

    int it = 0, perc = 0;

    stats.clear();
    for(Event event : events){
        perc = 100.0*(double(it) + 1)/double(events.size());
        cout << "\r" << perc << "%" << flush;

        current++;
        it++;

        if(event.type == 1){
            if (pimanager.allocate(event.vid, vis[event.vid])) {
		if (generate_dat) {
			phy.export_elasticity_dat(elasticityPhysical, 0, it);
			vis[event.vid].export_elasticity_dat(elasticityVirtual, event.vid, it);
			pimanager.export_elasticity_mapping_dat(event.vid, vis[event.vid], elasticityMapping, it);
		}
	    }
        }else if(event.type == 0){
            pimanager.deallocate(event.vid, vis[event.vid]);
        }

        x = event.interval;

        y = pimanager.time_mapping();
        stats["time_mapping"].push_back(PDD(x, y));

        y = pimanager.fragmentation();
        stats["fragmentation"].push_back(PDD(x, y));

        y = pimanager.acceptance();
        stats["acceptance"].push_back(PDD(x, y));

	//cout << " event " << event.interval << endl;

        y=pimanager.node_load_vcpu();
        stats["node_load_vcpu"].push_back(PDD(x,y));

	y=pimanager.node_load_ram();
        stats["node_load_ram"].push_back(PDD(x,y));

	y=pimanager.node_load_storage();
        stats["node_load_storage"].push_back(PDD(x,y));

        y=pimanager.link_load();
        stats["link_load"].push_back(PDD(x,y));

	//revenue
	y=pimanager.revenue();
        stats["revenue"].push_back(PDD(x,y));

        //revenue individual
        y=pimanager.revenue_individual();
        stats["revenue_ind"].push_back(PDD(x,y));

	//status
	y=pimanager.status();
        stats["status"].push_back(PDD(x,y));

 	//time ind
        y=pimanager.time_mapping_individual();
        stats["time_ind"].push_back(PDD(x,y));

    }
    cout << endl;
    return stats;
}
