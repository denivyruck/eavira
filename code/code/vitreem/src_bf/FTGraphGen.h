#ifndef __FT_GRAPH_GEN__
#define __FT_GRAPH_GEN__

#include "StdLib.h"
#include "Utils.h"
#include "Graph.h"
#include "GraphGen.h"

/* Fat Tree Graph Generator */
class FTGraphGen : public GraphGen {
public:
	struct Layer{
		PII n_nodes; // number of nodes in the layer
		PII node_type; // type of the nodes in the layer
		PII node_vcpu; // capacity of the nodes in the layer
		PII node_ram; // capacity of the nodes in the layer
		PII node_storage; // capacity of the nodes in the layer
		PII edge_cap; // edge capacities from this Layer to layer below
	};

    struct Config{
        PII n_layers;
        vector<Layer> layers;
        Config(PII n_layers, vector<Layer> layers): n_layers(n_layers), layers(layers){}
    };

	FTGraphGen(Config config);

    virtual Graph generate();

    Config config;
};

#endif
