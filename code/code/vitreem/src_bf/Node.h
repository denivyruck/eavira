#ifndef __NODE__
#define __NODE__

#include "StdLib.h"

class Node{
public:
	Node(int id, int type, int vcpu, int ram, int storage);

	void operator-=(const Node &other);
	void operator+=(const Node &other);

	bool operator<(const Node &other) const;

	int id; //node identifier (must be unique in same graph)
	int type; //type of node (vm, router, ...)
	int vcpu; 
	int ram; 
	int storage;
private:	
};

#endif
