#include "StdLib.h"
#include "Simulator.h"
#include "JSON.h"
#include "Utils.h"
#include "PIGraphGen.h"
#include "FTGraphGen.h"
#include "FMGraphGen.h"
#include "RingGraphGen.h"
#include "WebServerGraphGen.h"
#include "AmazonGraphGen.h"

wstring readfile(string filename){
    wfstream wif(filename);
    wstringstream wss;
    wss << wif.rdbuf();
    return wss.str();
}

PII read_pii(JSONArray jarray){
    assert(jarray.size() == 2);
    int a = jarray[0]->AsNumber();
    int b = jarray[1]->AsNumber();
    return PII(a, b);
}

FTGraphGen::Layer read_ftgraphgen_layer(JSONObject jlayer){
    PII n_nodes = read_pii(jlayer[L"n_nodes"]->AsArray());
    PII node_type = read_pii(jlayer[L"node_type"]->AsArray());
    PII node_vcpu = read_pii(jlayer[L"node_vcpu"]->AsArray());
    PII node_ram = read_pii(jlayer[L"node_ram"]->AsArray());
    PII node_storage = read_pii(jlayer[L"node_storage"]->AsArray());
    PII edge_cap = read_pii(jlayer[L"edge_cap"]->AsArray());
    return FTGraphGen::Layer{n_nodes, node_type, node_vcpu, node_ram, node_storage, edge_cap};
}

FTGraphGen* read_ftgraphgen(JSONObject jgg){
    PII n_layers = read_pii(jgg[L"n_layers"]->AsArray());
    JSONArray jlayers = jgg[L"layers"]->AsArray();
    vector<FTGraphGen::Layer> layers;
    for(int i=0; i<(int)jlayers.size(); i++){
        layers.push_back(read_ftgraphgen_layer(jlayers[i]->AsObject()));
    }
    return new FTGraphGen(FTGraphGen::Config(n_layers, layers));
}


PIGraphGen* read_pigraphgen(JSONObject jgg){
    int n_dc = jgg[L"n_dc"]->AsNumber();
    JSONObject jdc = jgg[L"datacenter"]->AsObject();
    PIGraphGen::Datacenter dc;
    dc.core_node_vcpu = jdc[L"core_node_vcpu"]->AsNumber();
    dc.core_node_ram = jdc[L"core_node_ram"]->AsNumber();
    dc.core_node_storage = jdc[L"core_node_storage"]->AsNumber();
    dc.core_edge_cap = jdc[L"core_edge_cap"]->AsNumber();
    dc.agregation_node_vcpu = jdc[L"agregation_node_vcpu"]->AsNumber();
    dc.agregation_node_ram = jdc[L"agregation_node_ram"]->AsNumber();
    dc.agregation_node_storage = jdc[L"agregation_node_storage"]->AsNumber();
    dc.agregation_edge_cap = jdc[L"agregation_edge_cap"]->AsNumber();
    dc.tor_node_vcpu = jdc[L"tor_node_vcpu"]->AsNumber();
    dc.tor_node_ram = jdc[L"tor_node_ram"]->AsNumber();
    dc.tor_node_storage = jdc[L"tor_node_storage"]->AsNumber();
    dc.tor_edge_cap = jdc[L"tor_edge_cap"]->AsNumber();
    dc.server_node_vcpu = jdc[L"server_node_vcpu"]->AsNumber();
    dc.server_node_ram = jdc[L"server_node_ram"]->AsNumber();
    dc.server_node_storage = jdc[L"server_node_storage"]->AsNumber();
    return new PIGraphGen(PIGraphGen::Config(n_dc, dc));
}

FMGraphGen* read_fmgraphgen(JSONObject jgg){
    PII n_nodes = read_pii(jgg[L"n_nodes"]->AsArray());
    PII node_vcpu = read_pii(jgg[L"node_vcpu"]->AsArray());
    PII node_ram = read_pii(jgg[L"node_ram"]->AsArray());
    PII node_storage = read_pii(jgg[L"node_storage"]->AsArray());
    PII edge_cap = read_pii(jgg[L"edge_cap"]->AsArray());
    return new FMGraphGen(FMGraphGen::Config(n_nodes, node_vcpu, node_ram, node_storage, edge_cap));
}

RingGraphGen* read_ringgraphgen(JSONObject jgg){
    PII n_nodes = read_pii(jgg[L"n_nodes"]->AsArray());
    PII node_vcpu = read_pii(jgg[L"node_vcpu"]->AsArray());
    PII node_ram = read_pii(jgg[L"node_ram"]->AsArray());
    PII node_storage = read_pii(jgg[L"node_storage"]->AsArray());
    PII edge_cap = read_pii(jgg[L"edge_cap"]->AsArray());
    return new RingGraphGen(RingGraphGen::Config(n_nodes, node_vcpu, node_ram, node_storage, edge_cap));
}

WebServerGraphGen* read_webservergraphgen(JSONObject jgg){
    PII n_nodes = read_pii(jgg[L"n_nodes"]->AsArray());
    PII node_vcpu = read_pii(jgg[L"node_vcpu"]->AsArray());
    PII node_ram = read_pii(jgg[L"node_ram"]->AsArray());
    PII node_storage = read_pii(jgg[L"node_storage"]->AsArray());
    PII edge_cap = read_pii(jgg[L"edge_cap"]->AsArray());
    return new WebServerGraphGen(WebServerGraphGen::Config(n_nodes,
                                                           node_vcpu, node_ram, node_storage, 
                                                           edge_cap));
}

AmazonGraphGen* read_amazongraphgen(JSONObject jgg){
    PII n_nodes = read_pii(jgg[L"n_nodes"]->AsArray());
    PII node_vcpu = read_pii(jgg[L"node_vcpu"]->AsArray());
    PII node_ram = read_pii(jgg[L"node_ram"]->AsArray());
    PII node_storage = read_pii(jgg[L"node_storage"]->AsArray());
    PII edge_cap = read_pii(jgg[L"edge_cap"]->AsArray());
    return new AmazonGraphGen(AmazonGraphGen::Config(n_nodes, node_vcpu, node_ram, node_storage, edge_cap));
}

GraphGen* read_graphgen(JSONObject jgg){
    if(jgg[L"type"]->AsString() == L"PI"){
        return (GraphGen*)read_pigraphgen(jgg);
    }else if(jgg[L"type"]->AsString() == L"FM"){
        return (GraphGen*)read_fmgraphgen(jgg);
    }else if(jgg[L"type"]->AsString() == L"FT"){
        return (GraphGen*)read_ftgraphgen(jgg);
    }else if(jgg[L"type"]->AsString() == L"RING"){
        return (GraphGen*)read_ringgraphgen(jgg);
    }else if(jgg[L"type"]->AsString() == L"WS"){
        return (GraphGen*)read_webservergraphgen(jgg);
    }else if(jgg[L"type"]->AsString() == L"AZ"){
        return (GraphGen*)read_amazongraphgen(jgg);
    }else{
        cout << "Undefined type" << endl;
        exit(EXIT_FAILURE);
    }
    return NULL;
}


int main(int argc, char **argv){
    //test();
    //return 0;

    srand(time(NULL));
    if(argc < 3){
        cout << "usage: simulator <parameters file> <output filename>\n";
        return -1;
    }

    JSONValue *vroot = JSON::Parse(readfile(argv[1]).c_str());
    if(vroot == NULL){
        cout << "Error to parse " << argv[1] << endl;
        return -1;
    }

    JSONObject root = vroot->AsObject();

    //load_from_file ===>>> 0: vitreem simulator
    //load_from_file ===>>> 1: load from VXDL (journal)
    //load_from_file ===>>> 2: load from elasticity

    Simulator sim;
    int n_vis = root[L"n_vis"]->AsNumber();
    int load_from_file = root[L"load_from_file"]->AsNumber();
    int generate_dat = root[L"generate_dat"]->AsNumber();
    wstring wPhysicalFile = root[L"physical_file"]->AsString();
    string physicalFile(wPhysicalFile.begin(), wPhysicalFile.end());

    wstring wVirtualPath = root[L"virtual_path"]->AsString();
    string virtualPath(wVirtualPath.begin(), wVirtualPath.end());

    wstring wVirtualElasticity = root[L"elasticity_virtual"]->AsString();
    string virtualElasticity(wVirtualElasticity.begin(), wVirtualElasticity.end());

    wstring wPhysicalElasticity = root[L"elasticity_physical"]->AsString();
    string physicalElasticity(wPhysicalElasticity.begin(), wPhysicalElasticity.end());

    wstring wMappingElasticity = root[L"elasticity_mapping"]->AsString();
    string mappingElasticity(wMappingElasticity.begin(), wMappingElasticity.end());

    PII interval = read_pii(root[L"interval"]->AsArray());

	GraphGen *pigen = NULL, *vigen = NULL;

	if (load_from_file == 0) {
    		pigen = read_graphgen(root[L"pigraphgen"]->AsObject());
    		vigen = read_graphgen(root[L"vigraphgen"]->AsObject());
	}
    double alpha = root[L"alpha"]->AsNumber();

    cout << "\nAlpha: " << alpha << "\n";

   double tini = stime();
    sim.run(pigen, vigen, n_vis, interval, argv[2], load_from_file, physicalFile.c_str(), virtualPath.c_str(), alpha, virtualElasticity.c_str(), physicalElasticity.c_str(), generate_dat, mappingElasticity.c_str());
    double tend = stime();

    cout << "\nSimulation time: " << tend - tini << "s\n";

    return 0;
}
