#include "WebServerGraphGen.h"

WebServerGraphGen::WebServerGraphGen(Config config):
    config(config)
{}

Graph WebServerGraphGen::generate(){
    vector<Node> nodes;
    vector<Edge> edges;

    int n_nodes = rand_btw(config.n_nodes);

    nodes.push_back(Node(0, 1, rand_btw(config.node_vcpu), rand_btw(config.node_ram), rand_btw(config.node_storage)));
    nodes.push_back(Node(1, 1, rand_btw(config.node_vcpu), rand_btw(config.node_ram), rand_btw(config.node_storage)));
    nodes.push_back(Node(2, 1, rand_btw(config.node_vcpu), rand_btw(config.node_ram), rand_btw(config.node_storage)));


    for(int u=3; u<3+2*n_nodes; u++)
        nodes.push_back(Node(u, 0, rand_btw(config.node_vcpu), rand_btw(config.node_ram), rand_btw(config.node_storage)));

    Graph graph(nodes, edges);

    graph.add_edge(Edge(0, 1, rand_btw(config.edge_cap)));
    graph.add_edge(Edge(0, 2, rand_btw(config.edge_cap)));

    for(int u=3; u<3+n_nodes; u++){
        graph.add_edge(Edge(1, u, rand_btw(config.edge_cap)));
    }

    for(int u=3+n_nodes; u<3+2*n_nodes; u++){
        graph.add_edge(Edge(2, u, rand_btw(config.edge_cap)));
    }

    return graph;
}

