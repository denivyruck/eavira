#include "Node.h"

Node::Node(int _id, int _type, int _vcpu, int _ram, int _storage):
	id(_id)
	,type(_type)
	,vcpu(_vcpu)
	,ram(_ram)
	,storage(_storage)
{
}

void Node::operator-=(const Node &other){
	vcpu -= other.vcpu;
	assert(vcpu >= 0);
	ram -= other.ram;
        assert(ram >= 0);
	storage -= other.storage;
        assert(storage >= 0);	
}

void Node::operator+=(const Node &other){
	vcpu += other.vcpu;
        assert(vcpu >= 0);
        ram += other.ram;
        assert(ram >= 0);
        storage += other.storage;
        assert(storage >= 0);
}

bool Node::operator<(const Node &other) const{
	return id < other.id;
}

ostream &operator<<(ostream &os, const Node &n){
    os << "[" << n.id << "] = " << n.vcpu << ", " << n.ram << ", " << n.storage << " (" << n.type << ")";
    return os;
}
