#ifndef __PI_GRAPH_GEN__
#define __PI_GRAPH_GEN__

#include "Graph.h"
#include "GraphGen.h"

/*   Physical Infrastructure Generator 
 *
 *   Based on [Cisco Data Center Infrastructure: 2.5 Design Guide 2007]
 *
 *   The Physical Infrastructure has n_dc datacenters that are 
 *   interconected.
 *
 *   Each Datacenter has 4 layers:
 *      Layer 1: 2 core switchs
 *      Layer 2: 4 agreagation switchs
 *      Layer 3: 4 tor switchs
 *      Layer 4: 12 server by tor (48 server total)
 *
 *  More information on: http://www.cisco.com/application/pdf/en/us/
 *  guest/netsol/ns107/c649/ccmigration_09186a008073377d.pdf 
 *
 */

class PIGraphGen : public GraphGen {
public:
    struct Datacenter{
        int core_node_vcpu;
        int core_node_ram;
        int core_node_storage;
        int core_edge_cap;

        int agregation_node_vcpu;
        int agregation_node_ram;
        int agregation_node_storage;

        int agregation_edge_cap;

        int tor_node_vcpu;
        int tor_node_ram;
        int tor_node_storage;
        int tor_edge_cap;
        
        int server_node_vcpu;
        int server_node_ram;
        int server_node_storage;
    };

    struct Config{
        int n_dc;
        Datacenter dc;
        Config(int n_dc, Datacenter dc):
            n_dc(n_dc),
            dc(dc)
        {}
    };

    PIGraphGen(PIGraphGen::Config config);

    void add_datacenter(vector<Node> &nodes, vector<Edge> &edges, VI &core_ids);

    virtual Graph generate();

    Config config;
};

#endif
