#include "PIManager.h"

PIManager::PIManager(
    Graph _phy, 
    Tree::Root _root_type, 
    VITreeM::Selection _selection_method,
    double _alpha
):
    phy(_phy),
    root_type(_root_type),
    selection_method(_selection_method),
    original_phy(phy),
    vi_count(0),
    vi_accepted(0),
    total_time_mapping(0.0),
    alpha(_alpha)
{}

bool PIManager::allocate(int vid, Graph vi){
    assert(mappings.count(vid) == 0);

    allocated = 0;
    revenue_ind = 0;    

    vi_count++;

    double tini = stime();

    VITreeM mapping(
            Tree(phy, Tree::PHYSICAL, root_type), 
            Tree(vi, Tree::VIRTUAL, root_type), 
            selection_method, alpha
    ); 

    double tend = stime();
    ind_time_mapping = 0;
    total_time_mapping += (tend - tini);
    ind_time_mapping = tend - tini;
    
    if(mapping.is_valid()){
	allocated = 1;
        mappings[vid] = mapping;
        phy = mapping.host.graph;
        vi_accepted++;
	
	for (unsigned int j = 0; j < vi.nodes.size(); j++) {
                revenue_ind += (vi.nodes[j].vcpu + vi.nodes[j].ram + vi.nodes[j].storage);
                for (unsigned int w = 0; w < vi.adj_list[j].size(); w++) {
                        revenue_ind += vi.cap_mtx[j][w];
                }

        }	

	revenue_total = revenue_total + revenue_ind;
    }

    return mapping.is_valid();
}

bool PIManager::deallocate(int vid, Graph vi){
    if(mappings.count(vid) == 0)
        return false;
    VITreeM mapping = mappings[vid];

    for(int v=0; v<(int)vi.nodes.size(); v++){
        int u = mapping.nodes_map[v];
        if(not (u>=0 and u<(int)phy.nodes.size()))
            cout << "u: " << u << endl;
        assert(u>=0 and u<(int)phy.nodes.size());
        assert(v>=0 and v<(int)vi.nodes.size());
        phy.nodes[u].vcpu += vi.nodes[v].vcpu;
        phy.nodes[u].ram += vi.nodes[v].ram;
        phy.nodes[u].storage += vi.nodes[v].storage;
    }

    for(int u=0; u<(int)vi.nodes.size(); u++){
        for(int v : vi.adj_list[u]){
            if(u > v)
                continue;
            int cap = vi.cap_mtx[u][v];
            VI path = mapping.edges_map[u][v];
            for(int i=0; i<(int)path.size() - 1; i++){
                int a = path[i];
                int b = path[i+1];
                phy.cap_mtx[a][b] += cap;
                phy.cap_mtx[b][a] += cap;
            }
        }
    }

    return true;
}

void PIManager::export_elasticity_mapping_dat(int vid, Graph vi, const char *elasticityMapping, int interval) {

	if(mappings.count(vid) == 0) {
        	cout << "Mapping not found to generate a file" << endl;
		return;
	}
	VITreeM mapping = mappings[vid];

	//update filename
	ofstream ofs;
        stringstream ss;
        ss << elasticityMapping << "-" << interval;

        ofs.open(ss.str());

	//just one VI
	ofs << "1" << endl;
	
	//number of VI nodes
	ofs << vi.nodes.size() << endl;

	//VI nodes and maps
	for (int u=0; u<(int)vi.nodes.size();u++) {
		int um = mapping.nodes_map[u];
                ofs << u << " "<< vi.nodes[u].vcpu << " " << vi.nodes[u].ram << " " << vi.nodes[u].storage << " " << vi.nodes[u].type << " " << um << endl;
        }

        //discover the number of links
	int nlinks = 0;
	
	for(int u=0; u<(int)vi.nodes.size(); u++){
        	for(int v : vi.adj_list[u]){
            		if(u > v)
                		continue;
			VI path = mapping.edges_map[u][v];
                        if (path.size() > 0) {		
				nlinks++;
			}
		}
	}	
	//inform it 
	ofs << nlinks << endl;

	//for each virtual link
	for(int u=0; u<(int)vi.nodes.size(); u++){
                for(int v : vi.adj_list[u]){
                        if(u > v)
                                continue;
			//if we have a path, print it
			VI path = mapping.edges_map[u][v];
                        if (path.size() > 0) {
				//print it
				int cap = vi.cap_mtx[u][v];
				ofs << u << " " << v << " " << cap << endl;
				//print path
            			for(int i=0; i<(int)path.size(); i++){
		        	        ofs << path[i] << " ";
				}
				ofs << endl;
			}
		}
	}
	ofs.close();
}


/* Metrics */
double PIManager::fragmentation(){
    int active = 0;
    int total = 0;
    
    //nodes
    for(int i=0; i<(int)phy.nodes.size(); i++){
        active += (original_phy.nodes[i].vcpu > phy.nodes[i].vcpu);
    }
    total += phy.nodes.size();
    
    //edges
    for(int u=0; u<(int)phy.adj_list.size(); u++){
        for(int v : phy.adj_list[u]){
            if(u > v)
                continue;
            
            total++;
            active += (original_phy.cap_mtx[u][v] > phy.cap_mtx[u][v]);
        }
    }
    
    return 100.0*double(active)/double(total);
}

double PIManager::revenue_individual(){
	return revenue_ind;
}

double PIManager::revenue(){
	if (vi_accepted > 0) {
		return revenue_total/vi_accepted;
	} else {
		return 0;
	}
}

double PIManager::status(){
    return allocated;
}

double PIManager::time_mapping_individual(){
    return ind_time_mapping;
}

double PIManager::time_mapping(){
    return total_time_mapping/double(vi_count);
}

double PIManager::acceptance(){
    return 100.0*double(vi_accepted)/double(vi_count);
}

double PIManager::node_load_vcpu(){
    double load = 0;
    double total = 0;
    
    for(int u=0; u<(int)phy.nodes.size(); u++) 
	{
            load += (original_phy.nodes[u].vcpu - phy.nodes[u].vcpu);
 	    total += original_phy.nodes[u].vcpu;
    	}
	return 100.0*double(load)/double(total);
}

double PIManager::node_load_ram(){
    double load = 0;
    double total = 0;
    
    for(int u=0; u<(int)phy.nodes.size(); u++)
        {
            load += (original_phy.nodes[u].ram - phy.nodes[u].ram);
            total += original_phy.nodes[u].ram;
        }
        return 100.0*double(load)/double(total);
}

double PIManager::node_load_storage(){
    double load = 0;
    double total = 0;
    
    for(int u=0; u<(int)phy.nodes.size(); u++)
        {
            load += (original_phy.nodes[u].storage - phy.nodes[u].storage);
            total += original_phy.nodes[u].storage;
        }
        return 100.0*double(load)/double(total);
}


double PIManager::link_load(){
    double total=0, load=0;
    
    for(int u=0; u<(int)phy.adj_list.size(); u++){
        for(int v : phy.adj_list[u]){
            if(u > v)
                continue;
            total += original_phy.cap_mtx[u][v];
            load += (original_phy.cap_mtx[u][v] - phy.cap_mtx[u][v]);
        }
    }
	return 100.0*load/total;
}

