#!/usr/bin/env python

import sys
import string
from random import randint

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print >> sys.stderr, ("[virtual | physical] file.dat")
        sys.exit(-1)

    out = ""
    f = open(sys.argv[2])
    for line in f:
	s = line
	l = line.split()
	if len(l) == 5: #nodes
	    value = 24
	    if sys.argv[1] == "virtual":
	        value = randint(1, 9)
	    s = "%s %s %s %s %s\n" %(l[0], value, value, value, l[4])
	elif len(l) == 3: #links
	    value = 1000
            if sys.argv[1] == "virtual":
		value = randint(1, 4) * 100
	    s = "%s %s %s\n" %(l[0], l[1], value)
	out = "%s%s" %(out, s)
    f.close()
    f = open(sys.argv[2], 'w')
    f.write(out)
    f.close()
