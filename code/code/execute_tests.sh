#!/bin/bash 

echo "****************************"
NIT=1000
NFS_RAIZ='/home/mpillon/eavira/code'
NFS_DIR=$NFS_RAIZ'/code'
PYPY=$NFS_RAIZ/pypy_portable/bin/pypy
#PYPY=/usr/bin/python

mkdir $NFS_DIR/resultados/ 2> /dev/null

echo "Pronto para compilar vitreem"

cd $NFS_DIR/vitreem/
echo "Compilando vpc-m3"
make clean
make dir
make bf > /dev/null


echo "Pronto p/ lançar ..../execute_tests.sh"
# BI_LIST=( 'SLA' 'VI' )
# SLA_LIST=( 'FREE' 'NORMAL' )
# PR_LIST=( 0.1 0.4 0.8 )
# POLICY_LIST=( BF WF )
# VIRTUAL_LIST=( '../input/virtual/ft/' '../input/virtual/vpc/' '../input/virtual/nlayers/' '../input/virtual/mixed/' )

SLA_LIST=( 'NORMAL' )
BI_LIST=( 'SLA' 'VI' )
PR_LIST=( 0.1 0.4 )
Online_LIST=( 'MBDF' 'VITREEM' )
Offline_LIST=( 'MM' 'EAVIRA' )
POLICY_LIST=( BF )
VIRTUAL_LIST=( '../input/virtual/vpc-m3/' )

echo "Criando Eventos do Relogio Virtual"
cd $NFS_DIR/ivrealoc/src/
for VIRTUAL in "${VIRTUAL_LIST[@]}"
do
	#Cria-se um log para utilização na pasta compartilhada NFS
	rm -f *.log *.pyc *.pkl
	#echo $PYPY GVT.py -nit $NIT -pr 0.1 -virtual $VIRTUAL -output $NFS_DIR/ivrealoc/src/
	#$PYPY GVT.py -nit $NIT -pr 0.1 -virtual $VIRTUAL -output $NFS_DIR/ivrealoc/src/ 
	echo $PYPY myGVT.py -nit $NIT -pr 0.1 -virtual $VIRTUAL -output $NFS_DIR/ivrealoc/src/ -dist uniform 
	$PYPY myGVT.py -nit $NIT -pr 0.1 0.4 -virtual $VIRTUAL -output $NFS_DIR/ivrealoc/src/  -dist uniform  > ../../myGVT.log
done

cd $NFS_DIR
for POLICY in "${POLICY_LIST[@]}"
do
	for BI in "${BI_LIST[@]}"
	do
		for SLA in "${SLA_LIST[@]}"
		do
			for PR in "${PR_LIST[@]}"
			do
				for Online in "${Online_LIST[@]}"
				do
					for Offline in "${Offline_LIST[@]}"
					do
						#PROFILE=$VIRTUAL
						#PROFILE=${PROFILE%/}; PROFILE=${PROFILE##*/}
						#sh $NFS_DIR/denivy.sh $NIT $BI $SLA $PR $POLICY $Online $offline $NFS_RAIZ
						oarsub -l nodes=1,walltime=12 "$NFS_DIR/denivy.sh $NIT $BI $SLA $PR $POLICY $Online $Offline"
						echo 'oarsub -l nodes=1,walltime=3' $NFS_DIR/denivy.sh $NIT $BI $SLA $PR $POLICY $Online $Offline
						# echo "Copiando diretorios..."
						# cd $NFS_DIR
						# cd ..
						# cp -r eavira/ /tmp/eavira/
						# echo "Lançando!"
						# cd /tmp/eavira/ivrealoc/src/
						# echo 'oarsub -l nodes=1,walltime=13' $PYPY main.py -nit $NIT -bi $BI -pr $PR -sla $SLA -policy $POLICY -alg VIT RLC-E -physical ../input/physical/g5k.dat -virtual ../input/virtual/vpc-m3/ -profile ./vpc-m3 -output $NFS_DIR/resultados/ --overwrite -online $Online -offline $offline
						#oarsub -l nodes=1,walltime=13 $PYPY main.py -nit $NIT -bi $BI -pr $PR -sla $SLA -policy $POLICY -alg VIT RLC-E -physical ../input/physical/g5k.dat -virtual ../input/virtual/vpc-m3/ -profile ./vpc-m3 -output $NFS_DIR/resultados/ --overwrite -online $Online -offline $offline						
					done
				done 
			done
		done
	done
done
