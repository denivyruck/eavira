import argparse
import cPickle as pickle
from copy import deepcopy
from random import randint, uniform
from math import sqrt
#from numpy import std, mean, sqrt
from timeit import default_timer

from BaseInfrastructure import SLA_BASED, VI_BASED
from Virtual import VirtualInfrastructure, VirtualSwitch, VirtualMachine
from Physical import MACHINE, SWITCH
from Datacenter import Datacenter
from Algorithms import dijkstra
from Controller import *

ALLOC = 0
REALLOC = 1
DEALLOC = 2
DO_NOTHING = 3

vitreem_dir = '../../vitreem/'
ivrealoc_dir = '../../ivrealoc/'

def my_std(data):
	u = mean(data)
	std = sqrt(1.0/(len(data)-1)*sum([(e-u)**2 for e in data]))
	return 1.96*std/sqrt(len(data))

def mean(data):
	return sum(data)/float(len(data))

def get_profile(nit, pr, profile_file):
	profile = {}
	vi_list = []

	#Restore pickle objects
	with open(profile_file+'_'+str(pr)+'.pkl', 'rb') as in_vi:
		while True:
			try:
				vi = pickle.load(in_vi)
				vi_list.append(vi)
			except EOFError:
				break

	with open(profile_file+'.log', 'r') as log:
		for i in range(nit):
			profile[i] = []

			op = int(log.readline())
			if op == DO_NOTHING:
				line = log.readline()
				profile[i].append({'op':DO_NOTHING})
			elif op == ALLOC:
				line = log.readline().split()
				vi_id = int(line[1])
				pkl_vi = -1
				for vi in vi_list:
					if vi.get_id() == vi_id:
						pkl_vi = vi
						break

				profile[i].append({'op':ALLOC,
								   'cfg':line[0],
								   'vi':pkl_vi})
			elif op == DEALLOC:
				vi = int(log.readline())
				profile[i].insert(0, {'op':DEALLOC,
									  'vi':vi})
			elif op == REALLOC:
				ndel = int(log.readline())
				delete_requests = []
				for j in range(ndel):
					line = list(map(lambda e:int(e), log.readline().split()))
					delete_requests.append({'vi':line[0],
											'vnode':line[1]})

				nrecfg = int(log.readline())
				recfg_requests = []
				for j in range(nrecfg):
					line = list(map(lambda e:int(e), log.readline().split()))
					recfg_requests.append({'vi':line[0],
										   'vnode':line[1],
										   'vcpu':line[2],
										   'vram':line[3],
										   'vstorage':line[4]})

				nrepl = int(log.readline())
				repl_requests = []
				for j in range(nrepl):
					line = list(map(lambda e:int(e), log.readline().split()))
					repl_requests.append({'vi':line[0],
										  'vnode':line[1]})
				profile[i].append({'op':REALLOC,
								   'delete_requests':delete_requests,
								   'recfg_requests':recfg_requests,
								   'repl_requests':repl_requests})
	return profile

def main():
	for pr in pr_list:
		profile = get_profile(nit, pr, profile_file)

		#print ("#MAP MSG00")
		for physical_file in physical_list: 
			for bi_type in bi_list:
				for sla_type in sla_list:
					for policy in policy_list:
						datacenter = Datacenter(bi_type, sla_type)
						controller = Controller(datacenter)
							
						datacenter.allocate_physical_infrastructure(physical_file)

						total_alloc = 0.0
						accepted_alloc = 0.0
						acc_list, acc_means = [], []
						energy_list, energy_means = [], []
						nop_list, nop_means = [], []

						sla_break = [0 for i in range(nit)]
						sla_break_steal = [0 for i in range(nit)]
						alloc = [0 for i in range(nit)]
						total_alloc_list = [0 for i in range(nit)]
						dealloc = [0 for i in range(nit)]
						realloc = [0 for i in range(nit)]
						energy_rlc = [0 for i in range(nit)]
						energy_ttl = [0 for i in range(nit)]

						dc_vm_load = [0 for i in range(nit)]
						dc_load = [0 for i in range(nit)]
						dc_network_load = [0 for i in range(nit)]

						for it in range(nit):
							s_break, s_break_steal = datacenter.get_sla_breaks()
						
							dc_vm_load[it] = datacenter.get_vms_load()	
							dc_load[it] = datacenter.get_dc_node_load()	
							dc_network_load[it] = datacenter.get_dc_network_load()	
                                                        sla_break[it]  = s_break
                                                        sla_break_steal[it] = s_break_steal
							energy_rlc[it] = datacenter.get_wasted_energy()
                                                        datacenter.update_vcpu_usage()

							for task in profile[it]:
								if task['op'] == DEALLOC:
									#print('Trying to deallocate vi')
									vi = datacenter.get_vi(task['vi'])
									if vi != -1:
										datacenter.deallocate_virtual_infrastructure(vi)
										dealloc[it] += 1
								elif task['op'] == ALLOC:
									print('Trying to allocate with %s algorithm' %(online))
									total_alloc += 1.0
									total_alloc_list[it] = total_alloc
									if online == 'VITREEM':
										t = controller.execute_vitreem(task['cfg'], policy)
										if t != -1:
                                            						if controller.allocate_virtual_infrastructures_vitreem(task['vi'], pr) == True:
                                            							accepted_alloc += 1.0
                                                						alloc[it] += 1
									else:
										t = controller.execute_mbfd(task['vi'])
										if t != -1:
											accepted_alloc += 1.0
											alloc[it] += 1
								elif task['op'] == REALLOC:
									#Means we'll also do elasticity
									print('Trying to reallocate (E) vis with %s' %(offline))
									delete_requests = task['delete_requests']
									recfg_requests = task['recfg_requests']
									repl_requests = task['repl_requests']

									controller.execute_elasticity(delete_requests, recfg_requests, repl_requests, offline)
									if offline == "EAVIRA":
										controller.reallocate_infrastructure_eavira()
									else:
										controller.reallocate_infrastructure_mm()
									realloc[it] += 1
									energy_rlc[it] = datacenter.get_wasted_energy()
								
							energy_ttl[it] = datacenter.get_energy_consumption()

							nop_list.append(datacenter.get_nop())
							if total_alloc == 0: acc_list.append(0.0)
							else: acc_list.append(accepted_alloc/total_alloc*100.0)
							energy_list.append(datacenter.get_wasted_energy())

							nop_means.append(mean(nop_list))
							acc_means.append(mean(acc_list))
							energy_means.append(mean(energy_list))

						# print("\n################# Datacenter Final Allocation #################")
						# for phy in datacenter.get_physical_resources():
						# 	for vm in phy.get_virtual_resources():
						# 		print("TYPE(%d): VM_id %s phy_host id %s cpu %s/%s ram %s/%s sto %s/%s" %( vm.get_type(),
						# 		vm.get_id(), phy.get_id(), phy.get_cpu(), phy.get_used_cpu(), 
						# 		phy.get_ram(), phy.get_used_ram(), phy.get_storage(), phy.get_used_storage()))
						# 		#print ("links %s", % (vm.get_links()))	
						# 		print "links"
						# 	 	for link in vm.get_links():
						# 	 		print ("source %s - destination %s - bw %s - path " %(vm.get_id(), link.get_destination().get_id(), 
						# 		 	link.get_allocated_bandwidth()))
						# 			for p in link.get_physical_path():
						# 	 			print p.get_id()
						# print("######################### END LIST #########################")


						op = mean(nop_means)
						u_op = my_std(nop_means)
						energy = mean(energy_means)
						u_energy = my_std(energy_means)
						ta = mean(acc_means)
						u_ta = my_std(acc_means)
						sla_break_mean = mean(sla_break)
						sla_break_steal_mean = mean(sla_break_steal)
						u_sla_break = my_std(sla_break)
						u_sla_break_steal = my_std(sla_break_steal)


						f = open(output_dir+'/'+virtual_dir.split('/')[-2]+'_'+physical_file.split('/')[-1].split('.')[0]+'_'+str(pr)+'_'+bi_type+'_'+sla_type+'_'+policy+'_'+online+'_'+offline+'.dat', 'w')
						f.write("i nop_list[i] acc_list[i] energy_list[i] alloc[i] dealloc[i] realloc[i] energy_rlc[i] energy_ttl[i] sla_break[i] sla_break_steal[i] total_alloc[i] migrations\n")
						for i in range(nit):
							f.write('%d %d %.2lf %.2lf %d %d %d %.2lf %.2lf %d %d %d %d\n' % (i, nop_list[i], acc_list[i], energy_list[i], alloc[i], dealloc[i], realloc[i], energy_rlc[i], energy_ttl[i], sla_break[i], sla_break_steal[i], total_alloc_list[i], datacenter.nmig))
						f.write('# NA: %d\t TA: %d\t ND: %d\t NMIG: %d\t REPL: %d/%d\t RECFG: %d/%d\t DEL: %d/\n' % (datacenter.nalloc, total_alloc, datacenter.ndealloc, datacenter.nmig, datacenter.nrepl, datacenter.trepl, datacenter.nrecfg, datacenter.trecfg, datacenter.ndelete))

						f.write('# TA: %.10lf +- %.2lf \t OP: %d +- %.2lf\t Energy: %.2lf +- %.2lf\tSLA break: %.2lf +- %.2lf\t SLA break steal: %.2lf +- %.2lf' % (ta, u_ta, op, u_op, energy, u_energy, sla_break_mean, u_sla_break, sla_break_steal_mean, u_sla_break_steal))
						f.close()


						f = open(output_dir+'/'+virtual_dir.split('/')[-2]+'_'+physical_file.split('/')[-1].split('.')[0]+'_'+str(pr)+'_'+bi_type+'_'+sla_type+'_'+policy+'_'+online+'_'+offline+'_datacenter_nodes_status.dat', 'w')
						for i in range(nit):
							for j in dc_load[i]:
								dcl = "%s" %(i)
								for jj in j:
									dcl = "%s %s" %(dcl, jj)
								dcl = "%s\n" %(dcl) 
								f.write(dcl)	
						f.close()		

						f = open(output_dir+'/'+virtual_dir.split('/')[-2]+'_'+physical_file.split('/')[-1].split('.')[0]+'_'+str(pr)+'_'+bi_type+'_'+sla_type+'_'+policy+'_'+online+'_'+offline+'_datacenter_network_status.dat', 'w')
                                                for i in range(nit):
                                                        for j in dc_network_load[i]:
                                                                dcl = "%s" %(i)
                                                                for jj in j:
                                                                        dcl = "%s %s" %(dcl, jj)
                                                                dcl = "%s\n" %(dcl)
        	                                                f.write(dcl)
                                                f.close()

						f = open(output_dir+'/'+virtual_dir.split('/')[-2]+'_'+physical_file.split('/')[-1].split('.')[0]+'_'+str(pr)+'_'+bi_type+'_'+sla_type+'_'+policy+'_'+online+'_'+offline+'_datacenter_vm_status.dat', 'w')

                                                for i in range(nit):
                                                        for j in dc_vm_load[i]:
                                                                dcl = "%s" %(i)
                                                                for jj in j:
                                                                        dcl = "%s %s" %(dcl, jj)
                                                                dcl = "%s\n" %(dcl)
                                                                f.write(dcl)
                                                f.close()

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Elastic Energy-Aware Virtual Infrastructure Realocation Algoritm')
	parser.add_argument('-nit', dest='nit', action='store', nargs=1, type=int, help='Number of iterations: N', required=True)
	parser.add_argument('-bi', dest='bi', action='store', nargs='+', help='BI type: VI|SLA', required=True)
	parser.add_argument('-pr', dest='pr', action='store', nargs='+', type=float, help='%% of SLA-restricted machines: 0.2', required=True)
	parser.add_argument('-sla', dest='sla', action='store', nargs='+', help='SLA policy: NORMAL|FREE', required=True)
	parser.add_argument('-online', dest='online', action='store', nargs='+', help='Allocation on-line algorithm: VITREEM|MBFD', required=True)
	parser.add_argument('-offline', dest='offline', action='store', nargs='+', help='Reallocation algorithm (offline): EAVIRA|MM', required=True)
	parser.add_argument('-policy', dest='policy', action='store', nargs='+', help='Allocation policy: BF|WF', required=True)
	parser.add_argument('-physical', dest='physical', action='store', nargs='+', help='Physical infrastructure location', required=True)
	parser.add_argument('-virtual', dest='virtual', action='store', nargs=1, help='Virtual infrastructure input folder', required=True)
	parser.add_argument('-profile', dest='profile', action='store', nargs=1, help='Profile file location (build with GVT)', required=True)
	parser.add_argument('-output', dest='output', action='store', nargs=1, help='Results output directory', required=True)
	args = parser.parse_args() 
	
	physical_list = args.physical
	bi_list = args.bi
	pr_list = args.pr
	sla_list = args.sla
	policy_list = args.policy

	nit = args.nit[0]
	virtual_dir = args.virtual[0]
	output_dir = args.output[0]
	profile_file = args.profile[0]
	online = args.online[0]
	offline = args.offline[0]
	main()
