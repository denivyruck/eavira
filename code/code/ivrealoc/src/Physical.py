#!/usr/bin/python
#-*- coding: utf-8 -*-

MACHINE = 0
SWITCH = 1

class PhysicalResource(object):
	def __init__(self, id, cpu, ram, storage, type):
		self.id = id
		self.cpu = cpu
		self.ram = ram
		self.storage = storage
		self.type = type
	
		self.virtual_resources = []

		self.linked_to = []

		self.acc = 0
		self.father = 0

		#With vms
		self.max_energy = 202.43
		self.min_energy = 118.11

		#Witout vms
		self.max_dom0 = 202.43
		#self.max_dom0 = 200.27
		self.min_dom0 = 118.11
		#self.min_dom0 = 116.68

	def allocate(self, vnode):
		if self.can_allocate(vnode):
			self.cpu -= vnode.get_vcpu()
			self.ram -= vnode.get_vram()
			self.storage -= vnode.get_vstorage()

			vnode.set_physical_host(self)
			self.virtual_resources.append(vnode)
			return True
		return False

	def deallocate(self, vnode):
		self.cpu += vnode.get_vcpu()
		self.ram += vnode.get_vram()
		self.storage += vnode.get_vstorage()
	
		vnode.set_physical_host(None)
		self.virtual_resources.remove(vnode)
		
	def remove_resources(self, vcpu, vram, vstorage):
		self.cpu -= vcpu
		self.ram -= vram
		self.storage -= vstorage

	def connect(self, pm, bw):
		physical_link = PhysicalLink(pm, bw)
		self.linked_to.append(physical_link)

	def allocate_bandwidth(self, dest, bw):
		for link in self.linked_to:
			if link.get_destination() == dest:
				link.allocate_bandwidth(bw)
	
	def deallocate_bandwidth(self, dest, bw):
		for link in self.linked_to:
			if link.get_destination() == dest:
				link.deallocate_bandwidth(bw)

	def can_allocate(self, vm):
		if self.cpu >= vm.get_vcpu() + vm.get_vcpu_network() and self.ram >= vm.get_vram() and self.storage >= vm.get_vstorage():
			return True
		return False

	def get_config(self):
		return '%d %d %d %d %d' % (self.get_id(), self.get_cpu(), self.get_ram(), self.get_storage(), self.get_type())

	def get_usage(self):
		used_cpu = sum([v.get_vcpu_usage() for v in self.virtual_resources])
		used_ram = sum([v.get_vram() for v in self.virtual_resources])
		used_storage = sum([v.get_vstorage() for v in self.virtual_resources])

		return r'PM %d (%d VMs) - CPU: %.2lf%% | RAM: %.2lf%% | Storage: %.2lf%%' % (self.id, len(self.virtual_resources), used_cpu/float(used_cpu+self.cpu)*100, \
																												used_ram/float(used_ram+self.ram)*100, \
																												used_storage/float(used_storage+self.storage)*100)

	def get_used_cpu(self):
		return sum([v.get_vcpu() for v in self.virtual_resources])

	def get_used_ram(self):
		return sum([v.get_vram() for v in self.virtual_resources])
	
	def get_used_storage(self):
		return sum([v.get_vstorage() for v in self.virtual_resources])

	def get_id(self):
		return self.id

	def get_type(self):
		return self.type

	def get_virtual_resources(self):
		return list(self.virtual_resources)
	
	def has_restricted_resources(self):
		for vnode in self.get_virtual_resources():
			if vnode.get_sla_time() == -1:
				return True
		return False

	def get_links(self):
		return self.linked_to

	def get_bandwidth_to(self, node):
		for link in self.linked_to:
			if link.get_destination() == node:
				return link.get_residual()

	def get_ram(self):
		return self.ram

	def get_cpu_usage(self):
		return sum([vnode.get_vcpu_usage() for vnode in self.get_virtual_resources()]) 
	
	def get_cpu(self):
		return self.cpu

	def get_storage(self):
		return self.storage

	def get_total_ram(self):
		return self.get_ram() + self.get_used_ram()
	
	def get_total_cpu(self):
		return self.get_cpu() + self.get_used_cpu()

	def get_total_storage(self):
		return self.get_storage() + self.get_used_storage()

	def has_virtual_resources(self):
		if len(self.get_virtual_resources()) > 0:
			return True
		return False

	def get_min_energy(self):
		if self.has_virtual_resources():
			return self.min_energy
		return self.min_dom0

	def get_max_energy(self):
		if self.has_virtual_resources():
			return self.max_energy
		return self.max_dom0

	def get_energy_consumption(self):
		if len(self.get_virtual_resources()) == 0:
			return 0 #esta desligado

		m_vcpu = self.get_total_cpu() - sum([vnode.get_vcpu_usage() for vnode in self.get_virtual_resources()])
		p = float(m_vcpu) / float(self.get_total_cpu())


		ret = sum([vnode.get_energy_consumption() for vnode in self.get_virtual_resources()]) + (self.get_min_energy() * p) + (self.get_management_consumption() * p)

		if ret > self.get_max_energy():
			print "Due to overbooking, energy is breaking our account! Returning just max!"
			return self.get_max_energy()
		return ret

	"""
	Method: get the management consumption on a given moment,
			which we get using the formula:
				C_g(u) = C_t(u) - C_min(u) - sum(Cr(vm) for vm in the machine)
	"""
	def get_management_consumption(self):
		C_t = sum([vnode.get_cpu_energy_usage() for vnode in self.get_virtual_resources()]) + \
				sum([vnode.get_network_energy_usage() for vnode in self.get_virtual_resources()])
		C_min = self.get_min_energy()
		C_net = sum([vnode.get_network_energy_usage() for vnode in self.get_virtual_resources()])
		
		return C_t - C_min - C_net

	"""
	Method: returns the fraction of energy that's being paid by the
			provider, i.e, what's left of C_g and C_min, which, in this
			model, are shared costs. The other costs are individual.
	"""
	def get_wasted_energy(self):
		if len(self.get_virtual_resources()) == 0:
			return 0 #servidor esta desligado

		m_vcpu = self.get_total_cpu() - sum([vnode.get_vcpu_usage() for vnode in self.get_virtual_resources()])
                p = float(m_vcpu) / float(self.get_total_cpu())
		myC_g = 0
		#management is only calaculated when there is at least one VM running, otherwise 0
		if len(self.get_virtual_resources()) > 0:
			myC_g = self.get_management_consumption()*p
		myC_min = self.get_min_energy()*p

		ret = myC_g+myC_min

		if ret > self.get_max_energy():
                        print "Due to overbooking, wasted energy is breaking our account! Returning just max!"
                        return self.get_max_energy()
                return ret		

########################################
#Used inheritance otherwiser i'd need to
#	write the same methods again. Don't
#	know if it's right, my OOP sucks
########################################
class PhysicalMachine(PhysicalResource):
	def __init__(self, id, cpu, ram, storage, type):
		PhysicalResource.__init__(self, id, cpu, ram, storage, type)

class PhysicalSwitch(PhysicalResource):
	def __init__(self, id, cpu, ram, storage, type):
		PhysicalResource.__init__(self, id, cpu, ram, storage, type)

class PhysicalLink(object):
	def __init__(self, destination, bw):
		self.destination = destination
		self.bw = bw
		
		#Increased everyime a VirtualLink is allocated
		self.weight = 0.0

	def allocate_bandwidth(self, bw):
		if self.weight + bw <= self.bw:
			self.weight += bw
			return True
		print('ib')
		raise 'Insufficient bandwidth'
		return False

	def deallocate_bandwidth(self, bw):
		self.weight -= bw

	def get_destination(self):
		return self.destination

	def get_bandwidth(self):
		return self.bw

	def get_allocated_bandwidth(self):
		return self.weight

	#How much bw is available
	def get_residual(self):
		return self.bw-self.weight

	def get_link_usage(self):
		return '%.2lf' % (self.weight/self.bw*100 + 1.0)

