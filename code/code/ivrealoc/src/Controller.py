#!/usr/bin/python
#-*- coding: utf-8 -*-

import os
import timeit
import sys
import subprocess
import re
import time
import random

from Datacenter import Datacenter
from Algorithms import ksp_yen, dijkstra

vitreem_dir = '../../vitreem/'
ivrealoc_dir = '../../ivrealoc/'

"""
Class: Controller
Description: Controller is the class that manages
			what operations are performed
"""
class Controller(object):
	def __init__(self, datacenter):
		self.datacenter = datacenter
		
	def reallocate_infrastructure_mm(self):
		self.datacenter.reallocate_infrastructure_mm()

	def reallocate_infrastructure_eavira(self):
		self.datacenter.reallocate_infrastructure_eavira()

	def execute_elasticity(self, delete_requests, recfg_requests, repl_requests, offline):
		repl_requests_count = 0
		recfg_requests_count = 0
		delete_requests_count = 0

		new_delete_requests = []
		for delete in delete_requests:
			delete_requests_count += 1
			vi = self.datacenter.get_vi(delete['vi'])
			if vi != -1:
				new_vnode = vi.get_virtual_resource(delete['vnode'])
				if new_vnode != -1:
					new_delete_requests.append(new_vnode)
		self.datacenter.answer_delete_requests(new_delete_requests)

		new_recfg_requests = []
                for recfg in recfg_requests:
                	recfg_requests_count+= 1
                	vi = self.datacenter.get_vi(recfg['vi'])
                        if vi != -1:
                        	new_vnode = vi.get_virtual_resource(recfg['vnode'])
                                if new_vnode != -1:
                                	recfg['vnode'] = new_vnode
                                        new_recfg_requests.append(recfg)

                new_repl_requests = []
                for repl in repl_requests:
                	repl_requests_count += 1
                	vi = self.datacenter.get_vi(repl['vi'])
                        if vi != -1:
                        	new_vnode = vi.get_virtual_resource(repl['vnode'])
                                if new_vnode != -1:
                                	new_repl_requests.append(new_vnode)

		if offline == "EAVIRA":
			self.datacenter.build_base_infrastructure()
			self.datacenter.answer_reconfiguration_requests_eavira(new_recfg_requests)
			self.datacenter.answer_replication_requests_eavira(new_repl_requests)
		else:
			#call MM, MBFD and Buyya solutions
			self.datacenter.answer_reconfiguration_requests_mbfd(new_recfg_requests)
			self.datacenter.answer_replication_requests_mbfd(new_repl_requests)

	def allocate_virtual_infrastructures_vitreem(self, vi, pr):
		ret = self.datacenter.allocate_virtual_infrastructures_vitreem(vi, vitreem_dir+'elasticity/resources/output/mapping.dat-1', pr)
		os.system('rm %s > /dev/null' % (vitreem_dir+'elasticity/resources/output/mapping.dat-1') )
		return ret

	def choose_config(self, virtual_folder):
		#Choose a random virtual file
		virtual_files = os.listdir(virtual_folder)
		return virtual_folder+virtual_files[random.randint(0, len(virtual_files)-1)]

	def execute_mbfd(self, vi):
		return self.datacenter.mbfd(vi)

	def execute_vitreem(self, chosen_cfg, policy):
		#Snapshot of datacenter to output/physical.dat
		self.datacenter.physical_cfg_to_file()
		
		#Move output/physical.dat to vitreem's input configuration
		self.move_new_physical_configuration()
		
		#Move file as input to Vitreem
		os.system('cp %s %s' % (chosen_cfg, vitreem_dir+'elasticity/resources/input/0.dat'))

		if policy == 'BF':
			code = '%s %s %s > /dev/null' % (vitreem_dir+'simulator_bf', \
								vitreem_dir+'elasticity/config/nlayers_param_sc_1_dat', \
								vitreem_dir+'elasticity/resources/output/vitreem.json')
			os.system(code)
		elif policy == 'WF':
			code = '%s %s %s > /dev/null' % (vitreem_dir+'simulator_wf', \
								vitreem_dir+'elasticity/config/nlayers_param_sc_1_dat', \
								vitreem_dir+'elasticity/resources/output/vitreem.json')
			os.system(code)

		if os.path.exists(vitreem_dir+'elasticity/resources/output/mapping.dat-1'):
			#print('\nAllocating ' + chosen_cfg)
			#time = re.findall('\d*\.\d+', out)
			#return float(time[0])
			return 1
		return -1

	def move_new_physical_configuration(self):
		#Move /output/physical to vitreem's input
		os.system('cp %s %s' % (ivrealoc_dir+'output/physical.dat', vitreem_dir+'elasticity/resources/input/physical.dat'))

	def get_datacenter(self):
		return self.datacenter
