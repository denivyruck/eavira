#!/usr/bin/python
#-*- coding: utf-8 -*-

#Python stuff
from random import randrange, uniform, random, randint
from timeit import default_timer
from copy import deepcopy
from re import findall
import math
import os, sys

#My stuff
from Algorithms import ksp_yen, dijkstra
#from Graph import DiGraph
from Physical import PhysicalMachine, PhysicalSwitch, MACHINE, SWITCH
from Virtual import VirtualMachine, VirtualInfrastructure, VirtualSwitch
from BaseInfrastructure import *

#Show on image
#import graphviz as grv
#import networkx as nx

class Datacenter(object):
	def __init__(self, bi_type, sla_type):
		self.resources = [] #Both pm or sw
		self.pm_list = []
		self.sw_list = []
		self.vi_list = []
		self.base_infrastructure = None
		
		self.bi_type = bi_type
		self.sla_type = sla_type

		self.rollback_list = []

		self.ndelete = 0
		self.ndealloc = 0
		self.nalloc = 0
		self.trepl = 0
		self.nrepl = 0
		self.trecfg = 0
		self.nrecfg = 0
		self.nmig = 0

	"""
	Method: creates the base infrastructure accordingly
			to the input
	"""
	def build_base_infrastructure(self):
		self.base_infrastructure = None
		if self.bi_type == VI_BASED:
			if len(self.vi_list) >= 1:
				self.base_infrastructure = VIBasedBaseInfrastructure(self.vi_list)
		elif self.bi_type == SLA_BASED:
			self.base_infrastructure = SLABasedBaseInfrastructure(self.resources)

	def answer_reconfiguration_requests_mbfd(self, recfg_requests):
		if len(recfg_requests) == 0: return

                for request in recfg_requests:
                        self.trecfg += 1

                        vnode = request['vnode']
                        vram = request['vram']
                        vcpu = request['vcpu']
                        vstorage = request['vstorage']

                        if vnode.get_vcpu() + vcpu < 1 or vnode.get_vram() + vram < 1 or vnode.get_vstorage() + vstorage < 1:
                                continue

                        pnode = vnode.get_physical_host()

                        if vnode.can_reconfigure(vcpu, vram, vstorage):
                                vnode.add_resources(vcpu, vram, vstorage)
                                pnode.remove_resources(vcpu, vram, vstorage)

                                self.nrecfg += 1
                        else:
                                vnode.add_resources(vcpu, vram, vstorage)
				original = vnode.get_physical_host().get_id()
				vml = []
				vml.append(vnode)
				self.mbfd_and_migration(vml)	
				if vnode.get_physical_host().get_id() != original:
					#migration is done
					self.nrecfg += 1


	def answer_reconfiguration_requests_eavira(self, recfg_requests):
		if len(recfg_requests) == 0: return

		for request in recfg_requests:
			self.trecfg += 1
			
			vnode = request['vnode']
			vram = request['vram']
			vcpu = request['vcpu']
			vstorage = request['vstorage']


			if vnode.get_vcpu() + vcpu < 1 or vnode.get_vram() + vram < 1 or vnode.get_vstorage() + vstorage < 1:
				continue

			pnode = vnode.get_physical_host()

			if vnode.can_reconfigure(vcpu, vram, vstorage):
				vnode.add_resources(vcpu, vram, vstorage)
				pnode.remove_resources(vcpu, vram, vstorage)

				self.nrecfg += 1
				#print('\t|----Reconfigured node %d of VI %d' % (vnode.get_id(), vnode.get_vi().get_id()))
			else:
				vnode.add_resources(vcpu, vram, vstorage)

				#First attempt: migrate inside the base_infrastructure to avoid future migrations
				bi_resources = self.base_infrastructure.get_resources(vnode.get_type())
				
				chosen = self.migrate(vnode, bi_resources)
				if chosen == -1:
					#Second attempt: find a candidate in the datacenter's resources 
					dc_resources = list(filter(lambda e:e not in bi_resources, self.get_resources(vnode.get_type())))
					
					chosen = self.migrate(vnode, dc_resources)
					if chosen == -1:
						vnode.remove_resources(vcpu, vram, vstorage)
						#print('\t|----Failed to reconfig %d of VI %d' % (vnode.get_id(), vnode.get_vi().get_id()))
					else:
						#When it deallocated in migrate it added
						#Extra resources to pnode
						pnode.remove_resources(vcpu, vram, vstorage)
						self.nrecfg += 1
						self.nmig += 1
						#print('\t|----Reconfigured node %d of VI %d' % (vnode.get_id(), vnode.get_vi().get_id()))
				else:
					pnode.remove_resources(vcpu, vram, vstorage)
					self.nrecfg += 1
					self.nmig += 1


	def answer_replication_requests_mbfd(self, repl_requests):
		if len(repl_requests) == 0: return

                for vnode in repl_requests:
                        self.trepl += 1
                        vi = vnode.get_vi()
                        new_id = vi.get_new_id()

                        new_replica = VirtualMachine(new_id, \
                                                                                vnode.get_vi(), \
                                                                                vnode.get_vcpu(), \
                                                                                vnode.get_vram(), \
                                                                                vnode.get_vstorage(), \
                                                                                vnode.get_sla_time(), \
                                                                                vnode.get_type(), \
                                                                                None)
                        new_replica.set_datacenter(self)

                        for link in vnode.get_links():
                                vnode2 = link.get_destination()
                                bw = link.get_allocated_bandwidth()

                                new_replica.connect(vnode2, [], bw)
                                vnode2.connect(new_replica, [], bw)

			#Lets find a host on datacenter using MBFD
                        minPower = vnode.get_physical_host().get_max_energy()
                        allocatedHost = None
                        for h in self.get_resources(new_replica.get_type()):
                        	if h.can_allocate(new_replica):
                                	power = new_replica.get_energy_consumption_estimate(h)
                                        if power <= minPower:
                                        	allocatedHost = h
                                                minPower = power
                        if allocatedHost != None:
                        	if allocatedHost.allocate(new_replica):
                        		connect, disconnect = new_replica.reconnect_adjacencies()
                        		if len(connect) == 0 and len(disconnect) == 0:
                        			#Return everything as it was
                                		allocatedHost.deallocate(new_replica)
	
			#Otherwise, couldn't replicate, undo everything
                        if new_replica.get_physical_host() == None:
                                for link in new_replica.get_links():
                                        vnode2 = link.get_destination()
                                        new_replica.disconnect(vnode2)
                                        vnode2.disconnect(new_replica)
                        else:
                                self.nrepl += 1
                                vi.add_virtual_resource(new_replica)
				print("OK, just replicated a new VM with id %d hosted by %d" %(new_replica.get_id(), new_replica.get_physical_host().get_id()))
		

	"""
	Method: tries to replicate on the same machine
			If failed, tries on the IB
			If also failed, on the DC
	"""
	def answer_replication_requests_eavira(self, repl_requests):
		if len(repl_requests) == 0: return

		for vnode in repl_requests:
			self.trepl += 1
			
			vi = vnode.get_vi()
			new_id = vi.get_new_id()

			new_replica = VirtualMachine(new_id, \
										vnode.get_vi(), \
										vnode.get_vcpu(), \
										vnode.get_vram(), \
										vnode.get_vstorage(), \
										vnode.get_sla_time(), \
										vnode.get_type(), \
										None)
			new_replica.set_datacenter(self)

			for link in vnode.get_links():
				vnode2 = link.get_destination()
				bw = link.get_allocated_bandwidth()

				new_replica.connect(vnode2, [], bw)
				vnode2.connect(new_replica, [], bw)

			#1st attempt: replicate on the same host
			pnode = vnode.get_physical_host()
			if pnode.allocate(new_replica):
				connect, disconnect = new_replica.reconnect_adjacencies()
				if len(connect) == 0 and len(disconnect) == 0:
					pnode.deallocate(new_replica)

			#2nd attempt: replicate on the BI
			bi_resources = self.base_infrastructure.get_resources(new_replica.get_type())
			if new_replica.get_physical_host() == None:
				for candidate in bi_resources:
					if candidate.allocate(new_replica):
						connect, disconnect = vnode.reconnect_adjacencies()
						if len(connect) == 0 and len(disconnect) == 0:
							candidate.deallocate(new_replica)
						else:
							break

			#Final attempt: replicate on any host inside the datacenter
			if new_replica.get_physical_host() == None:
				dc_resources = list(filter(lambda e: e not in bi_resources, self.get_resources(new_replica.get_type())))
				for candidate in dc_resources:
					if candidate.allocate(new_replica):
						connect, disconnect = new_replica.reconnect_adjacencies()
						if len(connect) == 0 and len(disconnect) == 0:
							candidate.deallocate(new_replica)
						else:
							break
			
			#Otherwise, couldn't replicate, undo everything
			if new_replica.get_physical_host() == None:
				for link in new_replica.get_links():
					vnode2 = link.get_destination()
					new_replica.disconnect(vnode2)
					vnode2.disconnect(new_replica)
				#print('\t|----Failed to replicate node %d' % vnode.get_id())
			else:
				self.nrepl += 1
				vi.add_virtual_resource(new_replica)
				print('\t|EAVIRA----Replicated node %d as %d' % (vnode.get_id(), new_replica.get_id()))
		
	"""
	Method: the user can request to deallocate an entire
			VI.
	"""
	def answer_delete_requests(self, delete_requests):
		for vnode in delete_requests:
			self.ndelete += 1
			
			vi = vnode.get_vi()
			vnode.get_physical_host().deallocate(vnode)
			vi.remove_virtual_resource(vnode)

			for link in vnode.get_links():
				vnode2 = link.get_destination()
				vnode.disconnect(vnode2)
				vnode2.disconnect(vnode)
			#print('\t|----Deallocated vnode %d' % vnode.get_id())
				
	"""
	Method: tries to find a migration candidate in resources
	Complexity: best case - allocates on the first host (k*(dijkstra complexity)) where k in the number of adjancet nodes
				worst case - tries all of them (E*k*dijkstra)
	"""
	def migrate(self, vnode, resources):
		pnode = vnode.get_physical_host()

		#Try to migrate in all candidates
		for destination in resources:
			if destination != pnode and destination.can_allocate(vnode):
				path, available_bw = self.shortest_path(pnode, destination, 1)
				if len(path) != 0 and available_bw != 0.0:
					time = vnode.get_migration_time(available_bw)
					if self.sla_type == 'FREE' or time < vnode.get_sla_time():
						#We can migrate
						pnode.deallocate(vnode)
						destination.allocate(vnode)

						#Try to reconnect all vnode's adjacencies to destination
						connect, disconnect = vnode.reconnect_adjacencies()
						if len(connect) == 0 and len(disconnect) == 0:
							#Return everything as it was
							destination.deallocate(vnode)
							if not pnode.allocate(vnode): print('vishmig2')
						else:
							#Migration successful
							self.rollback_list.append({'vnode':vnode, 'connect':connect, 'disconnect':disconnect, 'pnode':pnode})
							if pnode == None:
								print "Migrate AAHAHAHAHAHA -- just added a vnode without pnode %s" %(vnode.get_id())
							return destination
		#Failure
		return -1

	def get_sla_breaks(self):
		sla = 0
		steal = 0
		for h in self.get_resources(MACHINE):
			sum_nodes = 0
			for vm in h.get_virtual_resources():
				print "%s %s %s %s" %(vm.get_id(), vm.get_vcpu_usage(), vm.get_vcpu_network(), int(math.ceil(vm.get_vcpu_usage() + vm.get_vcpu_network())))

				sum_nodes = sum_nodes + int(math.ceil(vm.get_vcpu_usage() + vm.get_vcpu_network()))
				if sum_nodes > h.get_total_cpu():
					sla = sla + 1	

			if sum_nodes > h.get_total_cpu():
				steal = steal + (sum_nodes - h.get_total_cpu())

		for h in self.get_resources(SWITCH):
                        sum_nodes = 0
                        for vm in h.get_virtual_resources():
				sum_nodes = sum_nodes + int(math.ceil(vm.get_vcpu_usage() + vm.get_vcpu_network()))
	                        if sum_nodes > h.get_total_cpu():
	                                sla = sla + 1

			if sum_nodes > h.get_total_cpu():
				steal = steal + (sum_nodes - h.get_total_cpu())

		return sla, steal



	def update_vcpu_usage(self):
		#update all VI usages
                for h in self.get_resources(MACHINE):
                        for vm in h.get_virtual_resources():
                                vm.update_usage()

		for h in self.get_resources(SWITCH):
                        for vm in h.get_virtual_resources():
                                vm.update_usage()

	def reallocate_infrastructure_mm(self):
		THRESH_UP = 19 #80%
		THRESH_LOW = 5 #~20%
		t = 0
		bestFitVM = None
		migrationList = []

		#execute MM from Buyya
		for h in self.get_physical_resources():
			vms = sorted(h.get_virtual_resources(), key=lambda v: v.get_vcpu_usage())
			if len(vms) == 0:
				break
			hUtil = h.get_cpu_usage()
			bestFitUtil = hUtil
			while hUtil > THRESH_UP:
				for vm in vms:
					if vm.get_vcpu_usage() > hUtil - THRESH_UP:
						t = vm.get_vcpu_usage() - hUtil + THRESH_UP
						if t < bestFitUtil:
							bestFitUtil = t
							bestFitVM = vm
					else:
						if bestFitUtil == hUtil:
							bestFitVM = vm
						break
				hUtil = hUtil - bestFitVM.get_vcpu_usage()
				present = 0
				for v in migrationList:
					if v.get_id() == bestFitVM.get_id():
						present = 1
						break
				if present == 0:
					migrationList.append(bestFitVM)

			if hUtil < THRESH_LOW:
				for vm in vms:
					present = 0
					for v in migrationList:
						if v.get_id() == vm.get_id():
							present = 1
							break
					if present == 0:
						migrationList.append(vm)
		print ("MM: Now I must migrate:")
				
		for vm in migrationList:
			print("VM id %d" %(vm.get_id()))

		# print ("\n\n*** VI before to run the migration ***")
		# vi_before_migration = vm.get_vi()
		# vi_before_migration.print_allocation()
		# print ("\n *************************** \n")
		
		if len(migrationList) > 0:
			#lets call MBFD to conclude migration
			self.mbfd_and_migration(migrationList)

		# print ("\n\n###### VI after to run the migration ####")
		# vi_after_migration = vm.get_vi()
		# vi_after_migration.print_allocation()
		# print ("\n ################################ \n")

	def mbfd(self, vi):
		max_power = self.get_resources(MACHINE)[0].get_max_energy()
		vmList = sorted(vi.get_virtual_resources(), key=lambda v: v.get_vcpu_usage())
		rollback = []
		#VMs: if something went wrong, just return -1. We shouldn't fix MBFD :)
                for vm in vmList:
			vm.set_datacenter(self)

                        minPower = max_power
                        allocatedHost = None
                        for h in self.get_resources(vm.get_type()):
				if h.can_allocate(vm):
                                        power = vm.get_energy_consumption_estimate(h)
                                        if power <= minPower:
                                                allocatedHost = h
                                                minPower = power

                        if allocatedHost == None:
				for row in rollback:
					pnode = row[0]
					vnode = row[1]
					pnode.deallocate(vnode)
				print "MBFD - None: Without solution for node %s" %(vm.get_id())
				return -1

                        if not allocatedHost.allocate(vm):
				for row in rollback:
                                        pnode = row[0]
                                        vnode = row[1]
                                        pnode.deallocate(vnode)
				
				print "MBFD: Without solution for node %s" %(vm.get_id())
                                return -1

			rollback.append((allocatedHost, vm))
		print "MBFD - moving to network allocation"
		disconnectDict = {}
		for vm in vmList:
			connect, disconnect = vm.reconnect_adjacencies()
			#no need for track connect here
			if len(connect) == 0 and len(disconnect) == 0:
				print "MBFD: Without solution for network!"
				#rollback all links. Connect must be empty, otherwise we have a problem :)
				for i in disconnectDict:
					for j in disconnectDict[i]:
						i.disconnect(j['vnode2'])
						j['vnode2'].disconnect(i)

				#rollback all nodes
				for row in rollback:
                                        pnode = row[0]
                                        vnode = row[1]
                                        pnode.deallocate(vnode)
				return -1
			disconnectDict[vm] = disconnect
	
		vi.print_allocation()
		self.nalloc += 1
		self.vi_list.append(vi)
		
		print "MBFD: OK. Allocation is done with MBFD"
		return 1

	def mbfd_and_migration(self, migrationList):
		vmList = sorted(migrationList, key=lambda v: v.get_vcpu_usage())
		for vm in vmList:
			minPower = vm.get_physical_host().get_max_energy()
			allocatedHost = None
			for h in self.get_resources(vm.get_type()):
				#avoid migration to same host
				if vm.get_physical_host() != h and h.can_allocate(vm):
					path, available_bw = self.shortest_path(vm.get_physical_host(), h, 1)
					if len(path) != 0 and available_bw > 0.0:
						time = vm.get_migration_time(available_bw)
						#ok, migration is allowed
						power = vm.get_energy_consumption_estimate(h)
						if power <= minPower:
							allocatedHost = h
							minPower = power
			if allocatedHost != None:
				#migrate vm
				original = vm.get_physical_host()
                                original.deallocate(vm)
                                allocatedHost.allocate(vm)

                                #Try to reconnect all vnode's adjacencies to destination
                                connect, disconnect = vm.reconnect_adjacencies()
                                if len(connect) == 0 and len(disconnect) == 0:
  	                              #Return everything as it was
                                      allocatedHost.deallocate(vm)
                                      if not original.allocate(vm): print('vishmig2')
				self.nmig += 1
				print("MBFD just conclude the migration of %s from %s to %s" %(vm.get_id(), original.get_id(), allocatedHost.get_id()))



	"""
	Method: main method. BFS trying to consolidade
			physical hosts
	"""
	def reallocate_infrastructure_eavira(self):
		self.build_base_infrastructure()
		if self.base_infrastructure == None:
			return 0
		
		total_migrations = 0
		#print('\nReallocating', len(self.get_vi_list()), 'virtual infrastructures')
		#To visit stores the PMs yet to visit
		nodes = []
		visited = []

		#Initialize PMs to visit
		resources = self.base_infrastructure.get_resources(MACHINE, SWITCH)
		for pnode in resources:
			for link in pnode.get_links():
				dest = link.get_destination()
				if dest not in nodes and dest not in resources:
					nodes.append(dest)
		while len(nodes) != 0:
			#Start analyzing the lightest PM, the one that has more free resources
			pnode = max(nodes, key=lambda e:e.get_cpu())
			nodes.remove(pnode)
			#pnode = nodes.pop(0)
			visited.append(pnode)

			if pnode.has_virtual_resources():
				if pnode.has_restricted_resources():
					self.base_infrastructure.insert(pnode)
				else:
					#Append all nodes that still need to be checked
					resources = self.base_infrastructure.get_resources(MACHINE, SWITCH)
					for link in pnode.get_links():
						dest = link.get_destination()
						if dest not in nodes and \
									dest not in visited and \
									dest not in resources:
							nodes.append(dest)

					failed = False
					migrations = 0
					self.rollback_list = []
					#Trying to migrate all VMs
					for vnode in pnode.get_virtual_resources():
						#Resources are all possible migration candidades in BI
						resources = self.base_infrastructure.get_resources(vnode.get_type())
						#print("VM id %d" %(vnode.get_id()))
						
						#print(vnode, vnode.get_physical_host())
						chosen = self.migrate(vnode, resources)
						if chosen == -1:
							#At least 1 migration failed
							failed = True
							break
						else:
							migrations += 1

					if failed:
						if not self.rollback():
							sys.exit(1)
						self.base_infrastructure.insert(pnode)
					else:
						#print('migrated all from ', pnode.get_id(), pnode.has_virtual_resources())
						#Global values
						self.nmig += migrations
						total_migrations += migrations

		return total_migrations

	"""
	Method: rolls back all the changes made in reallocate_infrastructure()
	"""
	def rollback(self):
		self.rollback_list.reverse()

		if self.rollback_list.reverse() == None:
			return True

		print self.rollback_list.reverse()

		for backup in self.rollback_list:
			vnode1 = backup['vnode']
			pnode1 = backup['pnode']
			connect = backup['connect']
			disconnect = backup['disconnect']

			for link in disconnect:
				vnode2 = link['vnode2']
				vnode1.disconnect(vnode2)
				vnode2.disconnect(vnode1)

			for link in connect:
				vnode2 = link['vnode2']
				to_vnode2 = link['to_vnode2']
				bw = link['bw']

				vnode1.connect(vnode2, to_vnode2, bw)
				to_vnode1 = list(to_vnode2)
				to_vnode1.reverse()
				vnode2.connect(vnode1, to_vnode1, bw)

			vnode1.get_physical_host().deallocate(vnode1)

		
		for backup in self.rollback_list:
			pnode1 = backup['pnode']
			vnode1 = backup['vnode']

			if not pnode1.allocate(vnode1):
                                print('vishrb')
                                return False


		self.rollback_list = []
		return True

	##########################################
	#Returns the best path out of k choices
	##########################################
	def shortest_path(self, start, end, min_bw):
		#graph = self.datacenter_to_graph()
		graph = self.build_dijkstra_graph()
		paths = ksp_yen(graph, start, end, min_bw)

		routes = [(path['path'], self.get_maximum_available_bw(path['path'])) for path in paths]
		return max(routes, key=lambda e: e[1])

	#Maximum available bandwidth for administrative tasks of
	#	the datacenter. min(max(path(u, v)))
	def get_maximum_available_bw(self, path):
		available_bw = []
		for i in range(len(path)-1):
			pnode1 = path[i]
			pnode2 = path[i+1]
			available_bw.append(pnode1.get_bandwidth_to(pnode2))
	
		#Same machine most likely
		if len(available_bw) == 0:
			return 0.0
		return min(available_bw)

	#def datacenter_to_graph(self):
	#	G = DiGraph()
	#	for node in self.resources:
	#		G.add_node(node)
	#		for link in node.get_links():
	#			dest = link.get_destination()
	#			#bw = float(link.get_link_usage())
	#			bw = float(link.get_residual())
	#			G.add_edge(node, dest, bw)
	#			G.add_edge(dest, node, bw)
	#	return G

	##########################################
	#Returns a dictionary representation of
	#	the physical datacenter which can be
	#	understood by the dijkstra algorithm
	##########################################
	def build_dijkstra_graph(self):
		graph = {}
		for node in self.resources:
			if node not in graph:
				graph[node] = {}

			for link in node.get_links():
				node2 = link.get_destination()
				bw = {'res':float(link.get_residual()),
						#'usage':float(link.get_link_usage())}
						'usage':float(link.get_allocated_bandwidth())}
				graph[node][node2] = bw
		return graph

	####################################################
	#Parser for reading the input file
	####################################################
	def allocate_physical_infrastructure(self, file_name):
		f = open(file_name, 'r')

		n_pm = int(f.readline())
		for i in range(n_pm):
			info = f.readline().split(' ')
			#config = findall(r'[-+]?\d*\.\d+|\d+', row)

			id = int(info[0])
			cpu = int(info[1])
			ram = int(info[2])
			storage = float(info[3])
			node_type = int(info[4])
	
			if node_type == MACHINE:
				pm = PhysicalMachine(id, cpu, ram, storage, node_type)
				self.pm_list.append(pm)
				self.resources.append(pm)
			else: #Switch
				sw = PhysicalSwitch(id, cpu, ram, storage, node_type)
				self.sw_list.append(sw)
				self.resources.append(sw)

		#Links
		n_links = int(f.readline())
		for i in range(n_links):
			info = f.readline().split(' ')

			src, dest, bw = int(info[0]), int(info[1]), float(info[2])
			node1, node2 = self.get_resource(src), self.get_resource(dest)
		
			node1.connect(node2, bw)
			node2.connect(node1, bw)
		f.close()

	"""
	Method: creates a copy of vi with the designed mapping
	Input: vi - VI object to be coppied
			mapping_file - vitreem's mapping output
			sla_type - normal (preserve vi), free (infinite sla)
	"""
	def allocate_virtual_infrastructures_vitreem(self, vi, mapping_file, pr):
		with open(mapping_file, 'r') as mapping:
			mapping.readline()

			new_vi = deepcopy(vi)

			#Number of VMs
			n_vms = int(mapping.readline())

			rollback = []
			#Virtual resources
			for i in range(n_vms):
				virtual_info = mapping.readline().split()

				vid = int(virtual_info[0])
				host_id = int(virtual_info[5])

				vnode = new_vi.get_virtual_resource(vid)
				if vnode == -1:
					print "PROBLEM: VM %s not found in VI %s" %(vid, new_vi.get_id())
					print new_vi.get_virtual_resources()
					sys.exit()
				pnode = self.get_resource(host_id)

				vnode.set_datacenter(self)
				vnode.set_vi(new_vi)
				
				#Treating VITreeM's wrong allocations
				if not pnode.allocate(vnode):
					print("Hey! I should never be here! Please, review VITreeM integration with EAVIRA!")
					for row in rollback:
						pnode = row[0]
						vnode = row[1]
						pnode.deallocate(vnode)
					return False
				else:
					rollback.append((pnode, vnode))

			#Now we need to connect everything
			n_links = int(mapping.readline())
			for j in range(n_links):
				row = mapping.readline().split(' ')
				
				src, dest, bw = int(row[0]), int(row[1]), float(row[2])
				node1, node2 = new_vi.get_virtual_resource(src), new_vi.get_virtual_resource(dest)

				node1.disconnect(node2)
				node2.disconnect(node1)

				#The path itself (index of physical machines)
				row = mapping.readline()
				physical_links = findall(r'[-+]?\d*\.\d+|\d+', row)
		
				to_node2_link_list = []
				for link in physical_links:
					to_node2_link_list.append(self.get_resource(int(link)))
				to_node1_link_list = list(to_node2_link_list)
				to_node1_link_list.reverse()

				node1.connect(node2, to_node2_link_list, bw)
				node2.connect(node1, to_node1_link_list, bw)

			self.vi_list.append(new_vi)
			self.nalloc += 1
		print "VITREEM allocation is done!"
		return True

	def deallocate_virtual_infrastructure(self, vi):
		#print('Deallocating VI %d\n' % vi.get_id())
		for vnode in vi.get_virtual_resources():
			for link in vnode.get_links():
				vnode2 = link.get_destination()
				vnode.disconnect(vnode2)
				vnode2.disconnect(vnode)

			vnode.get_physical_host().deallocate(vnode)
		self.ndealloc += 1
		self.vi_list.remove(vi)

	def print_datacenter(self):
		print('Physical Infrastructure')
		for pm in self.pm_list:
			spc = '----|'
			print(pm.get_usage())
			for link in pm.get_links():
				print(spc + str(link.get_destination().id) + ' bw: %.4lf' % (link.weight))
		
		#print('')
		#for vi in self.vi_list:
		#	spc = '----|'
		#	print('Virtual Infrastructure ' + str(vi.id))
		#	
		#	for vm in vi.get_vm_list():
		#		print(spc + vm.get_config()) 
		#		spc = '----|'
		#		for link in vm.get_links():
		#			print(spc + spc + str(link.get_destination().get_id()) + ' through ' + ''.join(' ' + str(e.id) for e in link.get_path()))
		#	if vi != self.vi_list[-1]:
		#		print('')

	def print_to_image(self, prefix):
		out_dir = '../output/images/'
		if not os.path.exists(out_dir):
			os.makedirs(out_dir)
	
		datacenter = nx.Graph()
		
		for node in self.resources:
			if node.get_type() == 0: #Host
				node_shape = 'circle'
			else:
				node_shape = 'rectangle'
			datacenter.add_node(node.get_id(), \
								color='black', \
								shape=node_shape)
			datacenter.node[node.get_id()]['label'] = str(node.get_id())

		for node in self.resources:
			for link in node.get_links():
				datacenter.add_edge(node.get_id(), \
									link.get_destination().get_id(), \
									color='black')
				datacenter[node.get_id()][link.get_destination().get_id()]['label'] = link.get_residual()

		datacenter_tmp = deepcopy(datacenter)
		for node in self.resources:
			if node.has_virtual_resources():
				datacenter.node[node.get_id()]['style'] = 'filled'
				datacenter.node[node.get_id()]['fillcolor'] = 'green'

		#Converts to Graphviz style output
		out = nx.drawing.nx_agraph.to_agraph(datacenter)
		out.layout('dot')
		out.draw(out_dir+prefix+'datacenter.png')

		datacenter = datacenter_tmp
		#Printing base infrastructure
		if self.base_infrastructure != None:
			base_infra = deepcopy(datacenter)
			for pm in self.base_infrastructure.get_resources():
				base_infra.node[pm.get_id()]['style'] = 'filled'
				base_infra.node[pm.get_id()]['fillcolor'] = 'blue'
		
			out = nx.drawing.nx_agraph.to_agraph(base_infra)
			out.layout('dot')
			out.draw(out_dir+prefix+'base_infrastructure.png')

		#Prints all VIs
		for vi in self.vi_list:
			vi_snapshot = deepcopy(datacenter)

			to_color = []
			for vnode in vi.get_virtual_resources():
				node_id = vnode.get_physical_host().get_id()
				
				#First checkup in this pm
				if not '(' in vi_snapshot.node[node_id]['label']:
					vi_snapshot.node[node_id]['label'] += ' ('

				vi_snapshot.node[node_id]['style'] = 'filled'
				vi_snapshot.node[node_id]['fillcolor'] = 'red'
				if vi_snapshot.node[node_id]['label'] != str(node_id)+' (':
					vi_snapshot.node[node_id]['label'] += ','+str(vnode.get_id())
				else:
					vi_snapshot.node[node_id]['label'] += str(vnode.get_id())

				for link in vnode.get_links():
					physical_path = link.get_physical_path()
					for i in range(len(physical_path)-1):
						pair = (physical_path[i].get_id(), physical_path[i+1].get_id())
						if pair not in to_color:
							to_color.append(pair)
			
			pm_id_list = list(map(lambda vm:vm.get_physical_host().get_id(), vi.get_virtual_resources()))
			for pm in pm_id_list:
				if ')' not in vi_snapshot.node[pm]['label']:
					vi_snapshot.node[pm]['label'] += ')'
			
			to_color = [(vnode.get_id(), link.get_destination().get_id()) for link in vnode.get_links()]
			for e in vi_snapshot.edges():
				if (e[0], e[1]) in to_color:
					vi_snapshot[e[0]][e[1]]['color'] = 'red'
					vi_snapshot[e[0]][e[1]]['style'] = 'bold'

			out = nx.drawing.nx_agraph.to_agraph(vi_snapshot)
			out.layout('dot')
			out.draw(out_dir+prefix+'vi%d.png' % vi.get_id())
				#edges = vi_snapshot.edges()
				#id1 = vm.get_id()
				#for link in vm.get_links():
				#id2 = link.get_destination().get_id()
		
				#vi_snapshot[id1][id2]['color'] = 'red'
				
				#idx = vm.get_id()
				#edges_to_color = [(idx, link.get_destination().get_id()) for link in vm.get_links()]
				
				#colors = list(map(lambda pair: 'red' if pair[0] == id1 and pair[1] == id2 else 'black', edges))
				#colors = [vi_snapshot[u][v]['color'] for u, v in edges]

	def physical_cfg_to_file(self):
		with open('../output/physical.dat', 'w') as physical_file:
			physical_file.write(str(len(self.resources)) + '\n')
			for node in self.resources:
				physical_file.write(node.get_config() + '\n')
			
			nr_links = sum([len(node.get_links()) for node in self.resources])/2
			physical_file.write(str(int(nr_links)) + '\n')
			visited = []
			for node in self.resources:
				visited.append(node)
				for link in node.get_links():
					if link.get_destination() not in visited:
						to_write = '%d %d %d\n' % (node.get_id(), link.get_destination().get_id(), link.get_residual())
						physical_file.write(to_write)

	def get_resource(self, id):
		for pnode in self.resources:
			if pnode.get_id() == id:
				return pnode
		return -1

	def get_resources(self, type):
		if type == MACHINE:
			return self.get_physical_resources()
		elif type == SWITCH:
			return self.get_network_resources()

	def get_physical_resources(self):
		self.pm_list.sort(key=lambda e: e.get_cpu())
		return list(self.pm_list)

	def get_network_resources(self):
		self.sw_list.sort(key=lambda e: e.get_cpu())
		return list(self.sw_list)

	"""
	Method: guarantees at least % resources
			for the next reallocation iteration
	"""
	def provide_elasticity(self, pe):
		p = self.get_used_resources()/float(self.get_total_resources())
		if 1.0 - p < pe:
			new_pm = PhysicalMachine(len(self.resources), 24, 256, 1024, MACHINE)
			
			#Get all switches that connect to machines
			candidates = [sw for sw in self.sw_list if self.links_to_host(sw)]
			
			#Medo dessa fórmula
			chosen_sw = min(candidates, key=lambda e: \
							len(list(filter(lambda u:u.get_type() == MACHINE,
										map(lambda v:v.get_destination(), e.get_links())))))
			new_pm.connect(chosen_sw, 1000)
			chosen_sw.connect(new_pm, 1000)

			self.resources.append(new_pm)
			self.pm_list.append(new_pm)
			
			#Keep appending new machines until p is reached
			return self.provide_elasticity(pe)
		return True

	def links_to_host(self, sw):
		for link in sw.get_links():
			if link.get_destination().get_type() == MACHINE:
				return True
		return False
			
	def get_used_resources(self):
		used_resources = 0	
	
		for pnode in self.resources:
			used_resources += pnode.get_used_cpu()
		return used_resources

	def get_total_resources(self):
		total_resources = 0

		for pnode in self.resources:
			total_resources += pnode.get_total_cpu()
		return total_resources

	def get_vi_list(self):
		return list(self.vi_list)

	def get_vi(self, id):
		for vi in self.vi_list:
			if vi.get_id() == id:
				return vi
		return -1

	def get_vms_load(self):
		ret = []
		for vi in self.vi_list:
	                for vn in vi.get_virtual_resources():
	                        rn = []
				rn.append(vi.get_id())
        	                rn.append(vn.get_id())
        	                rn.append(vn.get_energy_consumption())
				rn.append(vn.get_vcpu())
				rn.append(vn.get_vcpu_usage())
	                        ret.append(rn)
                return ret		

	def get_dc_network_load(self):
		ret = []
		for pnode in self.resources:
                        for link in pnode.get_links():
                                dest = link.get_destination()
                                link_id = "%s-%s" %(pnode.get_id(), dest.get_id())
				rn = []
				rn.append(link_id)
				rn.append(link.get_allocated_bandwidth())
				ret.append(rn)
		return ret

	def get_dc_node_load(self):
		ret = []
		for node in self.resources:
			rn = []
			rn.append(node.get_id())
			rn.append(node.get_used_ram())
			rn.append(node.get_used_storage())
			rn.append(node.get_used_cpu())
			rn.append(node.get_cpu_usage())
			rn.append(node.get_energy_consumption())
			rn.append(node.get_wasted_energy())
			ret.append(rn)
		return ret

	def get_energy_consumption(self):
		return sum([node.get_energy_consumption() for node in self.resources if node.has_virtual_resources()])

	def get_wasted_energy(self):
		total_waste = 0.0
		
		#total_resources = self.get_total_resources()
		#needed_for_pe = pe*total_resources
		
		#not_used = 0.0
		#for pnode in self.resources:
		#	if pnode.has_virtual_resources():
		#		not_used += pnode.get_cpu()

		#Simulating turning a machine on
		#while not_used < needed_for_pe:
		#	total_waste += 116
		#	not_used += 24
		
		return total_waste + sum([node.get_wasted_energy() for node in self.resources if node.has_virtual_resources()])

	def get_nop(self):
		return self.ndelete + self.ndealloc + self.nalloc + self.nrepl + self.nrecfg + self.nmig

	def clear(self):
		for vi in self.get_vi_list():
			self.deallocate_virtual_infrastructure(vi)

		self.vi_id = 0
		self.ndealloc = 0
		self.ndelete = 0
		self.nalloc = 0
		self.nrepl = 0
		self.nrecfg = 0
		self.nmig = 0
		self.rollback_list = []
