#!/usr/bin/python
#-*- coding: utf-8 -*-

import random
import re
import math
from Physical import MACHINE, SWITCH

class VirtualResource(object):
	def __init__(self, id, vi, vcpu, vram, vstorage, sla_time, type, physical_host):
		self.id = id
		self.vi = vi
	
		self.vcpu = vcpu
		
		self.usage = 1.0 # starts with 100
		
		self.vram = vram
		self.vstorage = vstorage
		self.dirty_pages = random.randint(100,1000)
	
		self.type = type
		self.physical_host = physical_host

		self.sla_time = float(sla_time)
		
		self.linked_to = []

		self.datacenter = None
		self.energy_table = self.fetch_energy_info()
		self.network_vcpu_table = self.fetch_network_vcpu_info()

	def update_usage(self):
		if self.type == SWITCH:
			return
		self.usage = random.uniform(0.5, 1.0) 

	##################################################
	#Input: max_it -> maximum number of iterations
	#		max_mem -> maximum amount of memory to copy
	#		p -> minimum amount of dirty pages. If it's
	#			low, stop and copy the rest
	#		bw -> available bandwidth
	#Output: time in seconds
	##################################################
	def get_migration_time(self, bw):
		if bw == 0 or self.get_sla_time() == -1:
			return float('inf')

		#d = random.uniform(100.0, 1000.0)
		d = self.dirty_pages
		l = 4096.0 #bytes

		max_it = 30
		max_mem = 2*self.get_vram()*1024*1024*1024
		p = 1000.0

		#vRAM GB -> bytes
		v = self.get_vram()*1024*1024*1024
		#Bandwidth to use => min(max(path)) in bytes/s
		bw = (bw/8.0)*1024*1024

		vmig = 0.0
		tmig = 0.0
		t = 0.0
		for i in range(max_it):
			t = v/bw
			v = t*d*l
			tmig += t
			vmig += v

			#print(v/1024.0/1024.0)

			if vmig >= max_mem or v/l < p:
				break
		tdown = v/bw
		tmig += tdown

		return tmig

	def connect(self, destination, physical_path, bw):
		virtual_link = VirtualLink(destination, physical_path, bw)
		virtual_link.allocate_bandwidth()
		self.linked_to.append(virtual_link)

	###############################################
	#Used to disconnect vm from self
	#	- Deallocated all the bandwidth from the
	#		physical path;
	###############################################
	def disconnect(self, vnode):
		for link in self.get_links():
			if link.get_destination() == vnode:
				link.deallocate_bandwidth()
				self.linked_to.remove(link)

	"""
	Method: tries to update all my adjacencies' links to pnode
			In case of failure, everything is rolled back
	"""
	def reconnect_adjacencies(self):
		connect = [] #Redo these connections in case of failure
		disconnect = [] #Undo these connections in case of failure
		failed = False

		vnode1 = self
		pnode1 = self.get_physical_host()

		#Save all vnode1's connections in case of failure
		for link in vnode1.get_links():
			vnode2 = link.get_destination()
			
			backup = {}
			backup['vnode2'] = vnode2
			backup['to_vnode2'] = link.get_physical_path()
			backup['bw'] = link.get_allocated_bandwidth()
			connect.append(backup)

			vnode1.disconnect(vnode2)
			vnode2.disconnect(vnode1)
		
		#Now our datacenter's network has some free bw available
		for link in connect:
			vnode2 = link['vnode2']
			pnode2 = vnode2.get_physical_host()
			needed_bw = link['bw']

			#print(vnode2, pnode2, needed_bw)
			to_vnode1, available_bw = self.datacenter.shortest_path(pnode2, pnode1, needed_bw)
			
			#We need a path that can hold at least needed_bw
			if len(to_vnode1) == 0 or available_bw < needed_bw:
				#Can't migrate as we don't have a new link
				failed = True
				break
			else:
				#Here we will insert the updated link
				#Which will need to be rolled back in case any other link fails
				to_vnode2 = list(to_vnode1)
				to_vnode2.reverse()

				backup = {}
				backup['vnode2'] = vnode2
				disconnect.append(backup)
	
				vnode2.connect(vnode1, to_vnode1, needed_bw)
				vnode1.connect(vnode2, to_vnode2, needed_bw)
				
		if failed:
			#Disconnect all the paths we had connected
			for link in disconnect:
				vnode2 = link['vnode2']
				
				vnode1.disconnect(vnode2)
				vnode2.disconnect(vnode1)
		
			#Connect all the links we had disconnected
			for link in connect:
				vnode2 = link['vnode2']
				to_vnode2 = link['to_vnode2']
				bw = link['bw']

				vnode1.connect(vnode2, to_vnode2, bw)
				to_vnode1 = list(to_vnode2)
				to_vnode1.reverse()
				vnode2.connect(vnode1, to_vnode1, bw)

			#In theory everything is as it originally was
			return [], []
		else:
			#All links were updated without any problem
			#We need to return the changes for future rollbacks
			return connect, disconnect
		
	def add_resources(self, vcpu, vram, vstorage):
		self.set_vcpu(self.get_vcpu()+vcpu)
		self.set_vram(self.get_vram()+vram)
		self.set_vstorage(self.get_vstorage()+vstorage)
		
	def remove_resources(self, vcpu, vram, vstorage):
		self.set_vcpu(self.get_vcpu()-vcpu)
		self.set_vram(self.get_vram()-vram)
		self.set_vstorage(self.get_vstorage()-vstorage)

	"""
	Method: check if new requested upgrade (+ or -) is supported
			by the host
	"""
	def can_reconfigure(self, vcpu, vram, vstorage):
		pnode = self.get_physical_host()
		if self.get_vcpu() + vcpu <= pnode.get_cpu() + self.get_vcpu() and \
			self.get_vram() + vram <= pnode.get_ram() + self.get_vram() and \
			self.get_vstorage() + vstorage <= pnode.get_storage() + self.get_vstorage():
			return True
		return False

	def get_sla_time(self):
		return self.sla_time

	def get_physical_host(self):
		return self.physical_host
	
	def get_links(self):
		return list(self.linked_to)

	def get_config(self):
		return '%d %d %d %d' % ((self.id, self.vcpu, self.vram, self.vstorage))

	def get_vram(self):
		return self.vram
	
	def get_vcpu(self):
		return self.vcpu

	def get_vcpu_usage(self):
		return int(math.ceil(self.vcpu * self.usage))

	def get_vstorage(self):
		return self.vstorage

	def get_id(self):
		return self.id

	def get_vi(self):
		return self.vi

	def get_type(self):
		return self.type

	def set_datacenter(self, datacenter):
		self.datacenter = datacenter
		
	def set_vi(self, vi):
		self.vi = vi

	def set_vram(self, vram):
		self.vram = vram

	def set_vcpu(self, vcpu):
		self.vcpu = vcpu
		
	def set_vstorage(self, vstorage):
		self.vstorage = vstorage

	def set_physical_host(self, physical_host):
		self.physical_host = physical_host

	def get_energy_consumption_estimate(self, pnode):
                p = float(self.get_vcpu_usage())/float(pnode.get_total_cpu())

                U_min = pnode.get_min_energy()*p
                U_g = pnode.get_management_consumption()*p
                U_r = self.get_network_energy_usage()
                U_p = self.get_cpu_energy_usage() - pnode.get_min_energy()
    		
 		v = U_min + U_g + U_r + U_p

		if v > pnode.get_max_energy():
  			return pnode.get_max_energy()

                return v 

	def get_energy_consumption(self):
		pnode = self.get_physical_host()

		if pnode == None:
			print "There is something wrong!!!! pnode is None for vi %s vm %s" %(self.vi, self.get_id())

		p = float(self.get_vcpu_usage())/float(pnode.get_total_cpu())

		U_min = pnode.get_min_energy()*p
		U_g = pnode.get_management_consumption()*p
		U_r = self.get_network_energy_usage()
		U_p = self.get_cpu_energy_usage() - pnode.get_min_energy()

 		v = U_min + U_g + U_r + U_p

		if v > pnode.get_max_energy():
  			return pnode.get_max_energy()

                return v 

	def get_cpu_energy_usage(self):
		#due to overbooking we can have more vcpus than previouslly discussed. In this case, I'm assuming the power consumption isn't impacted and returning the max value
		u = self.get_vcpu_usage()
		m = 0
		for k in self.energy_table['HOST']:
			if k > m:
				m = k
		if u > m:
			return self.energy_table['HOST'][m]
		else:
			return self.energy_table['HOST'][u]

	def get_vcpu_network(self):
		total_vcpu = 0.0
                for link in self.get_links():
                        vnode = link.get_destination()
                        bw = 2 * link.get_allocated_bandwidth()

                        bw_id = int(bw)
                        idv = 0
                        while bw_id / (100 + idv * 100):
                                idv+=1
                        idv+=1
                        idv*=100

                        total_vcpu += self.network_vcpu_table[idv]
			
		return total_vcpu / 100
#		return int(math.ceil(total_vcpu))

	def get_network_energy_usage(self):
		total_energy = 0.0
		for link in self.get_links():
			vnode = link.get_destination()
			bw = 2 * link.get_allocated_bandwidth()

			bw_id = int(bw)
			idv = 0
			while bw_id / (100 + idv * 100):
				idv+=1
			idv+=1
			idv*=100

			if self.get_physical_host() == vnode.get_physical_host():
				#total_energy += self.energy_table['NET']['SAME'][int(bw)]
#				total_energy += bw/100*self.energy_table['NET']['SAME'][100]
				total_energy += self.energy_table['NET']['SAME'][idv]
			else:
				#total_energy += self.energy_table['NET']['APART'][int(bw)]
#				total_energy += bw/100*self.energy_table['NET']['APART'][100]
				total_energy += self.energy_table['NET']['APART'][idv]

		return total_energy

	def fetch_network_vcpu_info(self):
                network = open('../input/network/network.dad', 'r')

                network_table = {}
                for line in network:
                        info = re.findall(r'[-+]?\d*\.\d+|\d+', line)
                        bw = int(info[0])
                        cpu = float(info[1])

                        network_table[bw] = cpu
                network.close()

                return network_table


	def fetch_energy_info(self):
		host_energy = open('../input/energy/processador.dad', 'r')

		host_table = {}
		for line in host_energy:
			info = re.findall(r'[-+]?\d*\.\d+|\d+', line)
			ncpu = int(info[0])
			venergy = float(info[1])
		
			host_table[ncpu] = venergy
		host_energy.close()
	
		same_energy = open('../input/energy/cenario2.dad', 'r')
		
		same_table = {}
		for line in same_energy:
			info = re.findall(r'[-+]?\d*\.\d+|\d+', line)
			bw = int(info[0])
			venergy = float(info[1])
		
			same_table[bw] = venergy
		same_energy.close()
	
		apart_energy = open('../input/energy/cenario4.dad', 'r')

		apart_table = {}
		for line in apart_energy:
			info = re.findall(r'[-+]?\d*\.\d+|\d+', line)
			bw = int(info[0])
			venergy = float(info[1])
		
			apart_table[bw] = venergy
		apart_energy.close()

		net_table = {'SAME':same_table, 'APART':apart_table}

		return {'HOST':host_table, 'NET':net_table}

########################################
#Used inheritance otherwiser i'd need to
#	write the same methods again. Don't
#	know if it's right, my OOP sucks
########################################
class VirtualMachine(VirtualResource):
	def __init__(self, id, vi_id, v_cpu, v_ram, v_storage, sla_time, type, physical_host):
		VirtualResource.__init__(self, id, vi_id, v_cpu, v_ram, v_storage, sla_time, type, physical_host)

class VirtualSwitch(VirtualResource):
	def __init__(self, id, vi_id, v_cpu, v_ram, v_storage, sla_time, type, physical_host):
		VirtualResource.__init__(self, id, vi_id, v_cpu, v_ram, v_storage, sla_time, type, physical_host)

########################################
#It is necessary a class to represent
#	a virual path with n nodes between
#	X and Y
########################################
class VirtualLink(object):
	def __init__(self, vm, physical_path, bw):
		self.destination = vm
		self.bw = bw
		self.physical_path = physical_path
	
	def allocate_bandwidth(self):
		if len(self.physical_path) == 0: return

		for i in range(len(self.physical_path)-1):
			node1, node2 = self.physical_path[i], self.physical_path[i+1]
			node1.allocate_bandwidth(node2, self.bw)

	def deallocate_bandwidth(self):
		if len(self.physical_path) == 0: return

		for i in range(len(self.physical_path)-1):
			node1, node2 = self.physical_path[i], self.physical_path[i+1]
			node1.deallocate_bandwidth(node2, self.bw)
	
	def get_destination(self):
		return self.destination

	def get_allocated_bandwidth(self):
		return float(self.bw)

	def get_physical_path(self):
		return self.physical_path

class VirtualInfrastructure(object):
	def __init__(self, id, virtual_file):
		self.id = id
		self.virtual_resources = []
		self.virtual_file = virtual_file

		with open(virtual_file, 'r') as virtual:
			#Number of VMs
			n_vms = int(virtual.readline())

			#Virtual resources
			for i in range(n_vms):
				virtual_info = virtual.readline().split()
				
				vid = int(virtual_info[0])
				vcpu = int(virtual_info[1])
				vram = int(virtual_info[2])
				storage = float(virtual_info[3])
				node_type = int(virtual_info[4])

				sla_time = random.uniform(60.0, 600.0) #Seconds

				if node_type == MACHINE: #Virtual machine
					vm = VirtualMachine(vid, self, vcpu, vram, storage, sla_time, node_type, None)
					self.virtual_resources.append(vm)
				elif node_type == SWITCH: #Virtual switch
					vsw = VirtualSwitch(vid, self, vcpu, vram, storage, sla_time, node_type, None)
					self.virtual_resources.append(vsw)
			
			#Now we need to connect everything
			n_links = int(virtual.readline())
			for j in range(n_links):
				row = virtual.readline().split()
				
				src, dest, bw = int(row[0]), int(row[1]), float(row[2])
				node1, node2 = self.virtual_resources[src], self.virtual_resources[dest]

				node1.connect(node2, [], bw)
				node2.connect(node1, [], bw)

		self.vnode_ids = len(self.virtual_resources)
	
	########################################
	#Implements the cost policy, defined as
	#	CIV(Rf, Rv) = Sum(Rf)/sla_time + 
	#				  Sum(Rv*len(Rv));
	########################################
	def get_cost(self):
		cost = 0.0
		
		for vm in self.virtual_resources:
			cost += vm.get_vram()*1024 #MB
		
			#sum(c(u)) + sum(c(l)*len(p)) l => p
			for link in vm.get_links():
				cost += link.get_allocated_bandwidth()/8.0*len(link.get_physical_path())
		return cost

	def get_virtual_resource(self, id):
		for vnode in self.virtual_resources:
			if vnode.get_id() == id:
				return vnode
		return -1

	def add_virtual_resource(self, vnode):
		self.virtual_resources.append(vnode)

	def remove_virtual_resource(self, vnode):
		self.virtual_resources.remove(vnode)

	def get_virtual_resources(self):
		return list(self.virtual_resources)

	def set_virtual_resources(self, virtual_resources):
		self.virtual_resources = virtual_resources

	def get_new_id(self):
		id = self.vnode_ids
		self.vnode_ids += 1
		return id

	def get_id(self):
		return self.id

	def print_allocation(self):
		print "\n####*********************** nodes ***********************####"
		for vm in self.virtual_resources:
                        p = vm.get_physical_host()
                        print("TYPE(%d): VM_id %s phy_host id %s cpu %s/%s ram %s/%s sto %s/%s energy %s" %(vm.get_type(), vm.get_id(), p.get_id(), p.get_cpu(), 
                        	p.get_used_cpu(), p.get_ram(), p.get_used_ram(), p.get_storage(), p.get_used_storage(), p.get_energy_consumption()))

		
		print "####*********************** links ***********************####"
		for vm in self.virtual_resources:
			for link in vm.get_links():
				print ("source %s - destination %s - bw %s - path " %(vm.get_id(), link.get_destination().get_id(), 
					link.get_allocated_bandwidth()))
			 	for p in link.get_physical_path():
					print p.get_id()
		print "####*********************** End list ***********************####\n"
