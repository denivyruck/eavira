import argparse
import cPickle as pickle
from copy import deepcopy
from random import randint, uniform
#from numpy import std, mean, sqrt
from timeit import default_timer

from BaseInfrastructure import SLA_BASED, VI_BASED
from Virtual import VirtualInfrastructure, VirtualSwitch, VirtualMachine
from Physical import MACHINE, SWITCH
from Datacenter import Datacenter
from Algorithms import dijkstra
from Controller import *

ALLOC = 0
REALLOC = 1
DEALLOC = 2

vitreem_dir = '../../vitreem/'
ivrealoc_dir = '../../ivrealoc/'

def choose_config(virtual_folder):
	#Choose a random virtual file
	virtual_files = os.listdir(virtual_folder)
	return virtual_folder+virtual_files[random.randint(0, len(virtual_files)-1)]

def create_elasticity_requests(vi_list):
	delete_requests = []
	recfg_requests = []
	repl_requests = []

	#Delete - 5%
	for vi in vi_list:
		for vnode in vi.get_virtual_resources():
			if random.random() < 0.1:
				delete_requests.append({'vi':vi.get_id(),
										'vnode':vnode.get_id()})

	#Replication - 50%
	for vi in vi_list:
		for vnode in vi.get_virtual_resources():
			if random.random() < 0.5:
				repl_requests.append({'vi':vi.get_id(),
									'vnode':vnode.get_id()})

	#Reconfiguration - 50%
	recfg_requests = []
	for vi in vi_list:
		for vnode in vi.get_virtual_resources():
			if random.random() < 0.5:
				up_vcpu = random.randint(-1, 1)*randint(1, 8)
				up_vram = random.randint(-1, 1)*[1, 2, 4, 8, 16, 32][randint(0, 5)]
				up_vstorage = random.randint(-1, 1)*[8,16,32,64][randint(0,3)]

				new_cfg = {}
				new_cfg['vi'] = vi.get_id()
				new_cfg['vnode'] = vnode.get_id()
				if vnode.get_vcpu() + up_vcpu < 1:
					new_cfg['vcpu'] = 0
				else:
					new_cfg['vcpu'] = up_vcpu
					
				if vnode.get_vram() + up_vram < 1:
					new_cfg['vram'] = 0
				else:
					new_cfg['vram'] = up_vram

				if vnode.get_vstorage() + up_vstorage < 1:
					new_cfg['vstorage'] = 0
				else:
					new_cfg['vstorage'] = up_vstorage

				recfg_requests.append(new_cfg)

	return delete_requests, recfg_requests, repl_requests

def create_profile(nit):
	vi_id = 0
	op_log = {}
	vi_list = []

	for i in range(nit):
		if i % (nit/10) == 0:
			delete_requests, recfg_requests, repl_requests = create_elasticity_requests(vi_list)
			
			if i not in op_log:
				op_log[i] = []
			op_log[i].append({'op':REALLOC,
							  'delete_requests':delete_requests,
							  'recfg_requests':recfg_requests,
							  'repl_requests':repl_requests})

		else:
			cfg = choose_config(virtual_folder)
			vi = VirtualInfrastructure(vi_id, cfg)
			vi_list.append(vi)
			vi_id += 1

			lifespan = int(randint(20, 40)/100.0*nit)

			if i not in op_log:
				op_log[i] = []
			if i+lifespan not in op_log:
				op_log[i+lifespan] = []
			op_log[i].append({'op':ALLOC,
							  'cfg':cfg,
							  'vi':vi.get_id()})
			op_log[i+lifespan].insert(0, {'op':DEALLOC,
										  'vi':vi.get_id()})
	
	return op_log, vi_list

def main():
	profile, vi_list = create_profile(nit)

	out_fname = output_folder+virtual_folder.split('/')[-2]

	#Set restrictive SLA and pickle it
	for pr in pr_list:
		new_vi_list = deepcopy(vi_list)

		with open(out_fname+'_'+str(pr)+'.pkl', 'wb') as out_vi:
			for vi in new_vi_list:
				vresources = vi.get_virtual_resources()
				#Restricting VI's SLA
				chosen = []
				for i in range(int(pr*len(vresources))):
					vnode = vresources[randint(0, len(vresources)-1)]
					while vnode in chosen:
						vnode = vresources[randint(0, len(vresources)-1)]
					chosen.append(vnode)
					vnode.sla_time = -1

				pickle.dump(vi, out_vi, pickle.HIGHEST_PROTOCOL)

	with open(out_fname+'.log', 'w') as log:
		for i in range(nit):
			for task in profile[i]:
				if task['op'] == ALLOC:
					log.write('%d\n%s %d\n' % ((ALLOC, task['cfg'], task['vi'])))
				elif task['op'] == DEALLOC:
					log.write('%d\n%s\n' % ((DEALLOC, task['vi'])))
				elif task['op'] == REALLOC:
					log.write('%d\n' % (REALLOC))
					log.write('%d\n' % (len(task['delete_requests'])))
					for request in task['delete_requests']:
						log.write('%d %d\n' % (request['vi'], request['vnode']))
					log.write('%d\n' % (len(task['recfg_requests'])))
					for request in task['recfg_requests']:
						log.write('%d %d %d %d %d\n' % (request['vnode'],
													  request['vnode'],
													  request['vcpu'],
													  request['vram'],
													  request['vstorage']))
					log.write('%d\n' % (len(task['repl_requests'])))
					for request in task['repl_requests']:
						log.write('%d %d\n' % (request['vi'], request['vnode']))

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Elastic Energy-Aware Virtual Infrastructure Realocation Algoritm')
	parser.add_argument('-nit', dest='nit', action='store', nargs=1, type=int, help='Number of iterations: N')
	parser.add_argument('-pr', dest='pr', action='store', nargs='+', help='%% of restricted VMs in decimal')
	parser.add_argument('-virtual', dest='virtual', action='store', nargs=1, help='Virtual infrastructure input folder')
	parser.add_argument('-output', dest='output', action='store', nargs=1, help='Profile output directory')
	args = parser.parse_args() 
	
	nit = args.nit[0]
	pr_list = [float(e) for e in args.pr]
	virtual_folder = args.virtual[0]
	output_folder = args.output[0]

	main()
