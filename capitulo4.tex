\chapter{\textit{Energy-Aware Virtual Infrastructure Reallocation Algorithm} (EAVIRA)}
\label{chap:eavira}

O mecanismo de realocação proposto baseia-se em uma busca em largura para consolidar nós físicos. Antes dessa busca ser iniciada, é necessário que as requisições de elasticidade (desalocações, redimensionamentos e replicações) realizadas pelos usuários sejam atendidas.  
%
Após o atendimento das requisições, busca-se a reorganização, através de sucessivas migrações, dos recursos virtuais alocados.

A ordem da aplicação dos mecanismos de elasticidade foi definida com base no seu custo de realização.  
%
Dentre os três mecanismos abordados no Capítulo \ref{chap:revisao}, o redimensionamento é o menos custoso, pois consiste apenas em uma chamada para o \textit{hypervisor}, seguido da replicação e da migração.

Entretanto, para que a migração seja possível, é necessário que: (i) a restrição de tempo máximo para a migração do SLA contratado seja respeitada, ou seja, é necessária uma forma de estimar o seu tempo de migração; (ii)  que exista um hospedeiro destino com recursos disponíveis e; (iii) que todos os recursos virtuais conectados à máquina migrada tenham seus enlaces reconectados para o hospedeiro destino. 
%
Caso algum destes requisitos não forem atendidos, a migração será impossibilitada.

A Figura \ref{fig:realocdiagram} representa a sequência de ações tomadas pelo mecanismo, onde as fases 1, 2 e 3 caracterizam o atendimento das requisições de liberação de recursos, redimensionamento e replicação, respectivamente.
%
Por fim, a fase de realocação contém a principal estrutura (e a mais custosa computacionalmente) do algoritmo. 
%
As próximas seções detalham cada uma destas etapas.

\FloatBarrier
\begin{figure}[ht]
  \centering
  \includegraphics[width=0.58\textwidth]{Imagens/realocdiagram.eps}
  \caption{Atendimento das requisições dos usuários por parte do provedor e realocação de IVs. Fonte: próprio autor.}
  \label{fig:realocdiagram}
  \end{figure}
\FloatBarrier

\section{Definição da Infraestrutura Base}
\label{sec:eavira_ib}

O ponto inicial do algoritmo é a obtenção de uma infraestrutura alvo para as migrações. 
%
Para fins de comparação, propõem-se dois tipos de infraestruturas, chamadas Infraestrutura Base (IB): (i) por VI e; (ii) por SLA. 
%
A IB por VI é definida como a infraestrutura física que hospeda a IV de maior peso (custo) no \textit{datacenter}. 
%
A sua escolha é realizada através do cálculo das capacidades de cada IV, dado pela Equação \eqref{eq:ifcoef}, sendo $\sum_{i \in Rv} c(i)$ o somatório das capacidades provisionadas para cada recurso virtual da IV e $\sum_{l \in Ev} c(l).len(p)$ o produto entre a capacidade de cada enlace virtual alocado e o número de saltos de enlaces físicos ($l \Rightarrow p$) ocupados por este enlace virtual, visto que o seu mapeamento pode ser feito sobre múltiplos enlaces físicos.

\begin{equation}\label{eq:ifcoef}
CIV(Rv,Ev) = \sum_{i \in Rv} c(i) + \sum_{l \in Ev} c(l)*len(p); l \Rightarrow p
\end{equation}

Deste modo, obtém-se a infraestrutura de maior impacto no \textit{datacenter} e de maior dificuldade de ser realocada levando em termos de recursos alocados. A IB será, portanto, a infraestrutura física que hospeda a IV com o maior $CIV$.

Como a ideia do EAVIRA é a consolidação de MVs para desligar máquinas redundantes, qualquer máquina física que hospede uma MV com SLA restrito, ou seja, onde $t_{mig} = 0$, nunca poderá ser desligada. 
%
Ou seja, uma estratégia é compor a IB com todas as máquinas físicas que tenham ao menos uma MV com esta restrição, representando a opção (ii).

Esta definição da IB acontecerá a cada execução do algoritmo, visto que recursos virtuais podem ser liberados pelo usuário ou alterados em iterações anteriores de realocação (por exemplo, diminuição dos caminhos que hospedam os encales virtuais).
%
Entretanto, as opções de IB são aplicadas separadamente, ou seja, configuradas pelo provedor de IaaS.

\section{Etapa de Liberação de Recursos}

A partir da definição da IB, inicia-se a o atendimento das requisições elásticas do usuário. Dentre as requisições disponíveis, a liberação de recursos é a mais rápida (Figura \ref{fig:realocdiagram} - 1), além de incrementar os recursos disponíveis do \textit{datacenter}, favorecendo a execução de futuras requisições elásticas com o aumento de sua capacidade.

\section{Etapa de Redimensionamento}

Na etapa de redimensionamento (Figura \ref{fig:realocdiagram} - 2), a única verificação que necessita ser realizada é sobre redimensionamentos que aumentem a configuração provisionada, já que o hospedeiro deve conter os recursos necessários para a nova configuração.
%
Caso a capacidade residual da máquina física que hospeda a \ac{MV} a ser redimensionada ($c(H(rd))$) seja suficiente para atender ao novo requisito de redimensionamento ($c(rd)$), a \ac{MV} é redimensionada.
%
Caso os recursos já estejam esgotados, a requisição de redimensionamento será convertida em uma requisição de migração (com a capacidade requisitada atualizada, ou seja, substituída pela requisitada) direcionada à IB. Caso a IB não possua recursos disponíveis, a migração leva em conta todos os recursos do \textit{datacenter}. A requisição não é satisfeita caso todas as tentativas falhem.

\section{Etapa de Replicação}

Após as requisições de redimensionamento, iniciam-se as tentativas de replicação requisitadas pelo usuário (Figura \ref{fig:realocdiagram} - 3). 
%
A tentativa inicial é de replicar no mesmo hospedeiro, visto que a imagem do sistema já se encontra na mesma máquina.
%
Caso falhe, o mecanismo seleciona os hospedeiros da IB como candidatos a receber o novo recurso replicado.
%
Por fim, caso não seja possível, tenta-se replicar em um dos recursos disponíveis no \textit{datacenter}, sendo o hospedeiro escolhido o que deixar as menores lacunas de recursos ociosos, seguindo a abordagem \textit{best-fit} de~\cite{Ruck-2014}.

\section{Etapa de Migração}\label{sub:eavira_migracao}

Após as fases de liberações, redimensionamentos e replicações requisitadas pelos usuários, inicia-se a fase de sucessíveis migrações para os recursos virtuais do \textit{datacenter} através de uma busca em largura (\ac{BFS}) a partir da IB (Figura \ref{fig:realocdiagram} - Etapa de Realocação). No EAVIRA, o nó central da BFS é simulado por um grupo de nós pertencentes à IB, sendo a busca em nível realizada com cada nó em referencial.

A tomada de decisão sobre a migração, entretanto, leva em conta o seu tempo máximo permitido pelo SLA contratado. Assim, é preciso um modo de estimar o tempo exigido para a sua migração de um hospedeiro ao outro. O algoritmo utilizado para este fim foi proposto na Seção \ref{sub:migration}, e tem como base a RAM provisionada para a \ac{MV}. Caso o tempo estimado para esta migração, que é sempre para o pior caso, não estiver dentro dos limites do SLA, a migração não é realizada.

A Figura \ref{fig:realocacao} retrata o cenário de migração das MVs pertencentes às \ac{MVs} conectadas à IB.
%
Neste cenário, ocorreram um total de 5 migrações com sucesso, implicando no desligamento dos nós físicos 7, 9 e 10. As arestas pontilhadas indicam a ocorrência de migrações. Após o sucesso de uma migração, os enlaces provisionados são realocados para o caminho mais curto entre os recursos conectados à MV migrada. 

\FloatBarrier
\begin{figure}[ht]
  \centering
  \includegraphics[width=1.0\textwidth]{Imagens/realocacao.eps}
  \caption{Cenário de realocação de IVs. Fonte: próprio autor.}
  \label{fig:realocacao}
  \end{figure}
\FloatBarrier

Já para o caso de impossibilidade na migração, representado pela tentativa com a MV pertencente à IV3 no hospedeiro 8, a IB será expandida para comportar este nó (grafo do lado direito da Figura \ref{fig:realocacao}).
%
Ou seja, na próxima iteração o hospedeiro 8 poderá ser alvo de outras migrações. 

Além disso, as migrações das MVs de um nó são realizadas somente caso todas as migrações forem possíveis. Basta que uma migração seja impossibilitada para que a IB seja expandida. 
%
O procedimento será repetido para todas as máquinas ainda não analisadas, na forma de uma busca em largura. 

O Algoritmo \ref{code:realoc} representa o mecanismo implementado na forma de pseudo-código. 
%
As linhas 1, 2 e 3 inicializam as listas iniciais da busca em largura a partir das folhas da IB.
%
Os novos nós são inseridos iterativamente conforme forem visitados (linha 9). 
%
Para cada nó pertencente a esta lista (linha 5), busca-se a migração de todos os seus recursos virtuais (linha 12-19). 
%
Em caso de falha, todo o processo é desfeito para a máquina em análise (linhas 20-22). Ao final do algoritmo (linha 25), é retornado o número de migrações realizadas nesta iteração.
%
É importante ressaltar que a tempo de análise de possibilidades nenhuma migração é efetivada, apenas analisada.

Ao término do algoritmo, a infraestrutura em utilização pelo \textit{datacenter} será pelo menos menor ou igual à infraestrutura anterior, já que ou (i) a infraestrutura física não sofrerá alterações, ou (ii) o número de máquinas físicas utilizadas pelo provedor será reduzido por conta das consolidações, resultando na economia de energia que antes era desperdiçada pela ineficiente utilização destes recursos. 
%
Além disso, o custo  da IV pago pelo usuário continuará sendo o mesmo, visto que o modelo de tarifação é proporcional à capacidade requisitada \cite{hinz:2016}.

A política de alocação \textit{best-fit}~\cite{Ruck-2014} foi adotada por apresentar resultados satisfatórios, alinhado com a consolidação de recursos buscada pelo presente trabalho.
%
Essa escolha baseia-se na ideia de que a IB deve ser otimizada para comportar o maior número de MVs possível, visto que essa infraestrutura sempre será o alvo das migrações. 

Em contrapartida, caso a escolha do nó ocorra através da política de \textit{worst-fit}, os recursos do \textit{datacenter} serão subutilizados, visto que esta política tem impacto direto na diminuição da taxa de aceitação de IVs maiores, implicando diretamente em uma alta fragmentação do \textit{datacenter} pela necessidade de ativar novos recursos para comportar a requisição recebida.
%
Além disso, a política de \textit{worst-fit} possui péssimo desempenho em termos energéticos, já que os recursos físicos estarão subutilizados na maior parte do tempo, implicando na maior fatia de energia sendo paga pelo provedor.

\begin{algorithm}[H]
	\SetKwInOut{Input}{input}
	\SetKwInOut{Output}{output}
\Indm  
  \Input{$BI(V,E)$}
  \Output{Número de migrações realizadas}
\Indp
$resources = get\_resources(BI)$\;
$nodes = \{\forall neighb; neighb \in adj(node) \land node \in resources\}$\;
$visited = \{\}$\;
$total\_migrations = 0$\;
\For{$\forall node; node \in nodes$}{
	$nodes = nodes - \{node\}$\;
	$visited = visited \cup \{node\}$\;
	$resources = get\_resources(BI)$\;
	$nodes = nodes \cup \{\forall neighb; neighb \in adj(node) \land neighb \notin resources \land neighb \notin nodes \land neighb \notin visited\}$\;
	$rollback\_list = \{\}$\;
	$migrations = 0$\;
	\For{$\forall vnode \in virtual\_resources(node)$}{
		$resources = get\_resources(BI, vnode.type)$\;
		\eIf{$\neg migrate(vnode, resources)$}{
			$failed = True$\;
			$break$\;
		}{
			$migrations = migrations + 1$\;
			$rollback\_list = rollback\_list \cup \{vnode\}$\;
		}
	}
	\eIf{$failed$}{
		$rollback(rollback\_list)$\;
		$rollback\_list = \{\}$\;
	}{
		$total\_migrations = total\_migrations + migrations$\;
	}
}
return $total\_migrations$\;
\caption{Etapa de realocação}
\label{code:realoc}
\end{algorithm}

\section{Considerações Parciais}

O presente capítulo retratou o funcionamento do EAVIRA, o mecanismo de realocação elástica de IVs implementado. Este mecanismo possui dois módulos principais: (i) o atendimento de requisições elásticas e; (ii) a realocação, por meio de sucessivas migrações, dos recursos virtuais alocados, tendo como função objetivo a economia de energia pelo provedor.

Um dos principais pontos do algoritmo é a definição de uma infraestrutura base (IB) que servirá de alvo para as migrações, que tem como ideia principal a existência de IVs muito pesadas ou VMs que são totalmente restritivas para migrações. Após a definição da IB e do atendimento das requisições elásticas, o EAVIRA executa uma busca em largura a partir das folhas da IB tendo como finalidade última o desligamento de máquinas redundantes. Entretanto, o SLA contratado pelo usuário é um dos obstáculos que podem impossibilitar esta migração. Portanto, é necessário uma forma de estimar o seu tempo de migração para decidir se a máquina pode ou ser migrada ou não. Um algoritmo para este fim foi definido na Subseção \ref{sub:migration}.

Ao final da execução do EAVIRA, garante-se um melhor compartilhamento de recursos físicos pelos usuários da nuvem e a consequência direta na redução do gasto energético pelo provedor.
