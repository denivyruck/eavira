****************************************************
* Implementation of a elastic energy-aware virtual
* infrastructure realocation algorithm for my final
* course project (TCC2).
* Language: Python
* 
* 
* Author: Denivy Braiam Rück
****************************************************

*Utilização

Compile o mecanismo de alocação:

cd code/vitreem/;

mkdir obj_bf;

mkdir obj_wf;

make clean;

make simulator_bf;

make simulator_wf;

****************************************************

GVT.py - cria os arquivos de log utilizados para as diferentes classes
de infraestruturas virtuais

usage: GVT.py [-h] [-nit NIT] [-pr PR [PR ...]] [-virtual VIRTUAL]
              [-output OUTPUT] 

Elastic Energy-Aware Virtual Infrastructure Realocation Algoritm

optional arguments:
  -h, --help        show this help message and exit
  -nit NIT          Number of iterations: N
  -pr PR [PR ...]   % of restricted VMs in decimal
  -virtual VIRTUAL  Virtual infrastructure input folder
  -output OUTPUT    Profile output directory

Exemplo de uso: 100 iterações, 10% de MVs restritas com IVs do tipo FT

python2 GVT.py -nit 100 -pr 0.1 -virtual ../input/virtual/ft/ -output ./

****************************************************

main.py - EAVIRA

usage: main.py [-h] [-nit NIT] [-bi BI [BI ...]] [-pr PR [PR ...]]
               [-sla SLA [SLA ...]] [-policy POLICY [POLICY ...]]
               [-physical PHYSICAL [PHYSICAL ...]]
               [-virtual VIRTUAL] [-profile PROFILE] [-output OUTPUT]
                -online -offline

Elastic Energy-Aware Virtual Infrastructure Realocation Algoritm

optional arguments:
  -h, --help            show this help message and exit
  -nit NIT              Number of iterations: N
  -bi BI [BI ...]       BI type: VI|SLA
  -pr PR [PR ...]       % of SLA-restricted machines: 0.2
  -sla SLA [SLA ...]    SLA policy: NORMAL|FREE
  -policy POLICY [POLICY ...]
                        Allocation policy: BF|WF
  -physical PHYSICAL [PHYSICAL ...]
                        Physical infrastructure location
  -virtual VIRTUAL      Virtual infrastructure input folder
  -profile PROFILE      Profile file location (build with GVT)
  -output OUTPUT        Results output directory
  -online ONLINE [ONLINE ...]
                        Allocation on-line algorithm: VITREEM|MBFD
  -offline OFFLINE [OFFLINE ...]
                        Reallocation algorithm (offline): EAVIRA|MM


Exemplo de uso: 100 iterações, IB por SLA, 10% de restrições de VMs
			SLA normal, política de alocação best-fit,
			algoritmos analisados VITreeM, RLC-E (EAVIRA),
			infraestrutura física large.dat,
			infraestrutura virtual fat-tree,
			profile criado com GVT.py ./ft
			output dos resultados
			substituir arquivos de saída caso existentes

python2 main.py -nit 100 -bi SLA -pr 0.1 -sla NORMAL -policy BF -physical ../input/physical/large.dat -virtual ../input/virtual/ft/ -profile ./ft -output ../output/results/ -online VITREEM -offline EAVIRA

MPILLON = Comando p/ execução na g5k:

python2 GVT.py -nit 100 -pr 0.1 -virtual ../input/virtual/vpc-m3/ -output ./

python2 main.py -nit 100 -bi SLA -pr 0.1 -sla NORMAL -policy BF -physical ../input/physical/g5k.dat -virtual ../input/virtual/vpc-m3/ -profile ./vpc-m3 -output ../output/results/ -online VITREEM -offline EAVIRA
