\chapter{Realocação de IVs baseada no Consumo de Energia de MVs}
\label{chap:realocacao}

O elevado crescimento do consumo de energia em \textit{datacenters} vem trazendo preocupações relacionadas ao impacto ambiental resultante.
%
No ano de 2011, o consumo energético somente dos Estados Unidos correspondia a cerca de 3\% de toda a energia produzida~\apud{energy:2007}{duan-2016}.
%
Este consumo é principalmente associado ao suprimento energético e resfriamento dos servidores, que pode chegar a 53\% do seu consumo total~\cite{zhang:2010}.

Atualmente, \textit{datacenters} são responsáveis por emitir mais $CO_2$ do que a Argentina e a Holanda juntas.  
%
De acordo com Kaplan \textit{et al.}, o consumo médio de um \textit{datacenter} em 2008 era de aproximadamente 25 mil domicílios e sua pegada de carbono poderia quadruplicar até 2020~\cite{kaplan2008revolutionizing}.

Neste cenário, estratégias como a virtualização são úteis para reduzir tanto o custo  gerencial do \textit{datacenter} quanto o impacto ambiental causado pelo consumo energético.  
%
Entretanto, para minimizar acentuadamente este impacto, faz-se necessário transferir o foco das atividades gerenciais em \textit{datacenters}, que usualmente visam apenas o desempenho, para uma combinação entre minimização do gasto energético e maximização do desempenho~\cite{buyya2010energy}.

\section{Contexto de Aplicação}
\label{sec:cenario}

O cenário em que o mecanismo proposto é aplicado constitui-se de um \textit{datacenter} contendo $n$ requisições de infraestruturas virtuais já alocadas.  
%
O provedor que hospeda este \textit{datacenter} recebe ocasionalmente requisições por parte de seus usuários, tanto para alocação quanto para elasticidade de recursos.  
%
Para a elasticidade, as requisições podem ser de replicação ou redimensionamento de MVs, ou seja, aumentar ou diminuir a capacidade das IVs.  
%
Embora não seja solicitada por usuários, a migração é uma opção caso o redimensionamento ou replicação não sejam possíveis nos hospedeiros atuais.

Para exemplificar a relação entre o mecanismo proposto e os algoritmos de alocação, a Figura \ref{fig:scenario} representa a execução dos algoritmos em um mesmo \textit{datacenter} (os algoritmos são executados por um \textit{framework} de gerenciamento).  
%
O cenário apresentado recebe $m$ requisições de alocação de IV a cada $1$ intervalo de tempo (caixas vermelhas), de modo que cada requisição deve ser atendida rapidamente (identificado pela legenda $-$). 
%
O algoritmo de realocação (legenda $+$) é executado a cada $t$ intervalos de tempo (caixas azuis). 
%
O valor de $t$ (definido como 5 para compor o exemplo) pode ser fixo ou variável, ou seja, executado sempre que $k$ IVs forem alocadas com sucesso ou sempre que uma requisição por elasticidade for detectada.

\FloatBarrier
\begin{figure}[ht]
  \centering
  \includegraphics[width=.8\textwidth]{Imagens/scenario.eps}
  \caption{Exemplo de execução de algoritmos de alocação e realocação de IVs. Fonte: próprio autor.}
  \label{fig:scenario}
  \end{figure}
\FloatBarrier

Em resumo, após as IVs serem alocadas, garantindo as requisições do usuário, elas podem ser reorganizadas para atender a objetivos definidos para o próprio provedor.

\section{Objetivo do Provedor}

No contexto de provisionamento verde, elasticidade e realocação, o mecanismo proposto busca a realocação elástica das IVs já alocadas para minimizar o custo energético, buscando a consolidação de recursos virtuais e o desligamento de máquinas redundantes.  
%
Além disso, este objetivo necessita ser alcançado respeitando o SLA contratado pelo usuário que, para este contexto, é definido como o tempo (número de intervalos discretos) máximo aceitável para a migração de um recurso.
%
A realocação buscando a consolidação de recursos virtuais é uma boa abordagem para a economia de energia quando são levados em conta as considerações propostas por Hinz \textit{et al.}~\cite{hinz:2016}, discutidas na Seção~\ref{sec:green}. 

Apesar da realocação ser efetuada pelo provedor, os beneficiados neste processo são tanto provedores quanto usuários. 
%
% Em suma, o mecanismo deve (i) definir qual método de elasticidade será utilizado ao se receber uma requisição do usuário; (ii) definir as ordens de aplicação dos métodos; (iii) respeitar as restrições de capacidade dos recursos físicos; (iv) respeitar o acordo de nível de serviço estabelecido e (v) buscar a realocação de recursos visando a minimização do consumo energético.

\section{Formulação do Problema}

A forma mais intuitiva de se representar um \textit{datacenter} é através de um grafo $G(V,E,C)$, com $V$ representando os nós físicos, $E(u,v)$ os enlaces entre os nós $u$ e $v$ e $C(i)$ denota a capacidade disponível em um recurso ou canal de comunicação. Entretanto, dependendo do problema, pode ser favorável a sua representação na forma de árvores, como proposto por Oliveira~\cite{vitreem}, visando a remoção de ciclos no grafo e a redução do espaço de busca de uma solução válida. 

A Tabela~\ref{notacao} apresenta a notação adotada na formulação do mecanismo proposto. O mecanismo de realocação seguiu a estratégia de representação na forma de um grafo não-direcionado descrito acima, sendo a capacidade de cada nó ($C(i)$) composto de CPU, RAM e disco disponíveis. Dada a sua representação, a consolidação de recursos virtuais neste grafo é realizado através de uma busca em largura (\ac{BFS}). O algoritmo BFS é um método de busca usado tanto em grafos direcionados como não-direcionados, em que seus vértices são explorados sistematicamente e uniformemente a partir de um nó central~\cite{cormen2009introduction}. Cada um dos módulos percententes ao mecanismo proposto, tais como realocação e elasticidade, será discutido Capítulo~\ref{chap:eavira}. 

A Tabela \ref{notacao} apresenta a notação de alto nível adotada no decorrer do trabalho para formular o mecanismo de realocação. As expressões $rl$, $rd$ e $rp$ são usadas para descrever as requisições de provisionamento elástico recebidas dos usuários.

\begin{table}[!ht]
	\caption{Notação adotada na formulação do mecanismo de realocação elástica}
	\label{notacao}
	\begin{center}
		\begin{tabular}{|c|c|}
			\hline
			\textbf{Notação} & \textbf{Descrição} \\ \hline
			$rl(i)$ & Requisição de liberação da MV i \\ \hline
			$rd(i, c)$ & \begin{tabular}[c]{@{}c@{}}Requisição de redimensionamento da \\ MV i com nova capacidade c\end{tabular} \\ \hline
			$rp(i)$ & Requisição de replicação da MV i \\ \hline
			$H(i)$ & \begin{tabular}[c]{@{}c@{}}Máquina física que hospeda o \\ recurso ou requisição i\end{tabular} \\ \hline
			$C(i)$ & \begin{tabular}[c]{@{}c@{}}Capacidade disponível para o \\ recurso ou requisição i\end{tabular} \\ \hline
			$adj(i)$ & \begin{tabular}[c]{@{}c@{}}Máquinas físicas ou virtuais adjacentes \\ ao recurso i\end{tabular} \\ \hline
			$migre(i,r)$ & \begin{tabular}[c]{@{}c@{}}Migre MV i no melhor recurso\\ encontrado no conjunto r\end{tabular} \\ \hline
			$replique(i,r)$ & \begin{tabular}[c]{@{}c@{}}Replique a MV i no melhor recurso\\ encontrado no conjunto r\end{tabular} \\ \hline
		\end{tabular}
	\end{center}
\end{table}

\section{Trabalhos Relacionados}

Esta seção divide os trabalhos relacionados em categorias de algoritmos de alocação e realocação de recursos virtuais. 
%
Os algoritmos de alocação de infraestruturas virtuais, também chamados algoritmos \textit{on-line}, são executados sempre que uma nova requisição é recebida. 
%
Assim, tendo como base a quantidade de recursos disponíveis no momento, o algoritmo decidirá se a infraestrutura requisitada pode ou não ser alocada. 
%
É evidente que esta decisão deve ser feita rapidamente e sua resposta retornada ao usuário que a efetuou. 

Oliveira \textit{et al.} propuseram um algoritmo heurístico baseado em árvores que busca a minimização do tempo necessário para alocação~\cite{vitreem}.
%
Esse trabalho seguiu como embasamento teórico para a simplificação de um \textit{datacenter} e suas topologias virtuais como árvores. 
%
Por sua vez, as soluções D-ViNE e R-ViNE~\cite{chowdhury2009virtual} buscaram a alocação com foco na garantia da rota mais curta entre os recursos virtuais. 
%
A continuação deste trabalho, PolyViNE~\cite{chowdhury2010polyvine}, implementou um \textit{framework} de alocação inter-domínio utilizando um protocolo distribuído para coordenar a alocação entre diferentes provedores. 
%
Ainda, algoritmos gulosos são tradicionalmente aplicados para alocação de redes virtuais. Gong \textit{et al.} criaram um algoritmo guiado pela métrica (\textit{Global Resource Capacity (GRC)})~\cite{gong2014toward} , com a finalidade de evitar gargalos na rede gerados pelo uso da estratégia de \textit{Local Resource Capacity - LRC}, definida em trabalhos anteriores e amplamente usada para selecionar os candidatos físicos~\cite{yu2008rethinking}. 
%
A redução da fragmentação do \textit{datacenter} foi estudada por Cavalcanti e seus colegas \cite{Cavalcant-2014}, que quantificaram, entre outros resultados, o impacto negativo de um \textit{datacenter} fragmentado na taxa de aceitação de requisições de IVs.

Já os algoritmos de realocação de infraestruturas virtuais, são executados com um intervalo de tempo maior e levam em consideração todas as IVs já alocadas, buscando sua reorganização de modo a otimizar uma função objetivo.
%
Tendo a principal motivação a diminuição do custo da infraestrutura alocada por parte do usuário, Sharma \textit{et al.} propuseram um método de elasticidade baseado em programação linear. 
%
Sempre que a infraestrutura alocada necessitasse ser escalada, o conjunto de instâncias de MV (oferecidas como escolhas pelo provedor) que apresentasse o menor custo para o usuário seria escolhido, tendo como possibilidades o uso de migrações e replicações~\cite{Sharma-2011}. 
%
Um algoritmo destinado no balanceamento da alocação, dando foco a alocações mais críticas foi investigado em ~\cite{zhu2006algorithms} enquanto~\cite{duan-2016} propôs um algoritmo com mecanismo de predição baseado em fractais e utilizando ACO (\textit{Ant Colony Optimization}) como estratégia de alocação de modo a reduzir o custo energético.
%
Darolt (2016)~\cite{darolt2016explorando} explora a elasticidade dos recursos que compõem uma IV para otimizar o desempenho de aplicações n-camadas através de limiares de QoS estabelecidos pelo próprio usuário. Foi observado a diminuição do tempo médio de processamento e custo de provisionamento para o usuário ao hospedar a aplicação em uma infraestrutura elástica.

O diferencial do algoritmo proposto no presente trabalho é a inserção do atendimento de requisições de elasticidade junto com o procedimento de realocação de IVs com foco na redução do consumo energético, não encontrado em nenhum dos outros trabalhos.

\section{Considerações Parciais}

O capítulo retratou o contexto no qual a realocação de IVs é aplicada. Neste cenário, um \textit{datacenter} é composto por uma infraestrutura física na qual são provisionadas diversas IVs com o passar do tempo. Durante este processo, tanto algoritmos \textit{on-line} quanto \textit{off-line} encontram semelhantes restrições, como saturação dos enlaces, má escolha de hospedeiros para alocações, restrições como SLA, e tantas outras. Para o provedor, as suas funções objetivo podem variar desde o balanceamento de carga, aumento da taxa de aceitação até a minimização do gasto energético no \textit{datacenter}. Para isso, as mais diversas abordagens foram estudadas pela literatura especializada, mas nenhuma delas leva em consideração as requisições elásticas do usuário.

De modo a atingir este objetivo da minimização do consumo energético, o presente trabalho propõe um mecanismo de realocação de IVs que busca a reorganização dos recursos virtuais de modo a consolidar máquinas físicas e, assim, reduzir o desperdício energético do \textit{datacenter} através do desligamento de máquinas redundantes. Este mecanismo deverá levar em conta as restrições de tempo máximo definidas no SLA contratado, bem como atender as requisições de elasticidade. Entretanto, este é um problema NP-Difícil.

Apesar da função objetivo ser definida pelo provedor de serviços, os beneficiários são tanto usuários quanto provedores, visto que o modelo de tarifação utilizado para analisar o consumo energético de MVs propõe a cobrança mais justa para os recursos provisionados, baseado no trabalho de Hinz et al~\cite{hinz:2016}. 