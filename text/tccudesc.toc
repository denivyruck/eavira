\select@language {brazil}
\contentsline {chapter}{Lista de Figuras}{8}{chapter*.6}
\contentsline {chapter}{Lista de Abreviaturas}{10}{chapter*.7}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{11}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivos}{12}{section.1.1}
\contentsline {section}{\numberline {1.2}Contribui\IeC {\c c}\IeC {\~o}es}{13}{section.1.2}
\contentsline {section}{\numberline {1.3}Estrutura do Trabalho}{13}{section.1.3}
\contentsline {chapter}{\numberline {2}Revis\IeC {\~a}o de Literatura}{14}{chapter.2}
\contentsline {section}{\numberline {2.1}Nuvens Computacionais}{14}{section.2.1}
\contentsline {section}{\numberline {2.2}Elasticidade de Recursos Virtuais}{18}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}\textit {Service Level Agreement (SLA)}}{19}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Requisi\IeC {\c c}\IeC {\~a}o de Provisionamento El\IeC {\'a}stico}{20}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Mecanismos para Provisionamento de Elasticidade}{21}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Replica\IeC {\c c}\IeC {\~a}o de M\IeC {\'a}quinas Virtuais}{22}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Migra\IeC {\c c}\IeC {\~a}o de M\IeC {\'a}quinas Virtuais}{22}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Redimensionamento de Recursos Virtuais}{24}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Consolida\IeC {\c c}\IeC {\~a}o}{25}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}Aloca\IeC {\c c}\IeC {\~a}o e Realoca\IeC {\c c}\IeC {\~a}o de Infraestruturas Virtuais}{25}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Objetivos de Provedores e Usu\IeC {\'a}rios}{27}{subsection.2.4.1}
\contentsline {section}{\numberline {2.5}Provisionamento Verde de \ac {IVs}}{29}{section.2.5}
\contentsline {section}{\numberline {2.6}Considera\IeC {\c c}\IeC {\~o}es Parciais}{30}{section.2.6}
\contentsline {chapter}{\numberline {3}Realoca\IeC {\c c}\IeC {\~a}o de IVs baseada no Consumo de Energia de MVs}{32}{chapter.3}
\contentsline {section}{\numberline {3.1}Contexto de Aplica\IeC {\c c}\IeC {\~a}o}{32}{section.3.1}
\contentsline {section}{\numberline {3.2}Objetivo do Provedor}{33}{section.3.2}
\contentsline {section}{\numberline {3.3}Formula\IeC {\c c}\IeC {\~a}o do Problema}{34}{section.3.3}
\contentsline {section}{\numberline {3.4}Trabalhos Relacionados}{34}{section.3.4}
\contentsline {section}{\numberline {3.5}Considera\IeC {\c c}\IeC {\~o}es Parciais}{36}{section.3.5}
\contentsline {chapter}{\numberline {4}\textit {Energy-Aware Virtual Infrastructure Reallocation Algorithm} (EAVIRA)}{38}{chapter.4}
\contentsline {section}{\numberline {4.1}Defini\IeC {\c c}\IeC {\~a}o da Infraestrutura Base}{40}{section.4.1}
\contentsline {section}{\numberline {4.2}Etapa de Libera\IeC {\c c}\IeC {\~a}o de Recursos}{40}{section.4.2}
\contentsline {section}{\numberline {4.3}Etapa de Redimensionamento}{41}{section.4.3}
\contentsline {section}{\numberline {4.4}Etapa de Replica\IeC {\c c}\IeC {\~a}o}{41}{section.4.4}
\contentsline {section}{\numberline {4.5}Etapa de Migra\IeC {\c c}\IeC {\~a}o}{41}{section.4.5}
\contentsline {section}{\numberline {4.6}Considera\IeC {\c c}\IeC {\~o}es Parciais}{45}{section.4.6}
\contentsline {chapter}{\numberline {5}An\IeC {\'a}lise Experimental}{46}{chapter.5}
\contentsline {section}{\numberline {5.1}M\IeC {\'e}tricas}{46}{section.5.1}
\contentsline {section}{\numberline {5.2}Infraestruturas F\IeC {\'\i }sicas}{46}{section.5.2}
\contentsline {section}{\numberline {5.3}Infraestruturas Virtuais}{47}{section.5.3}
\contentsline {section}{\numberline {5.4}Algoritmos para Aloca\IeC {\c c}\IeC {\~a}o de IVs}{48}{section.5.4}
\contentsline {section}{\numberline {5.5}Algoritmos Comparados}{48}{section.5.5}
\contentsline {section}{\numberline {5.6}Cen\IeC {\'a}rio Experimental}{50}{section.5.6}
\contentsline {section}{\numberline {5.7}An\IeC {\'a}lise dos Experimentos}{51}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}N\IeC {\'u}mero de Migra\IeC {\c c}\IeC {\~o}es}{52}{subsection.5.7.1}
\contentsline {subsection}{\numberline {5.7.2}An\IeC {\'a}lise de Execu\IeC {\c c}\IeC {\~a}o com IVs do tipo \textit {Fat-Tree}}{52}{subsection.5.7.2}
\contentsline {subsection}{\numberline {5.7.3}An\IeC {\'a}lise dos Resultados com IVs do tipo \textit {VPC}}{56}{subsection.5.7.3}
\contentsline {subsection}{\numberline {5.7.4}An\IeC {\'a}lise dos Resultados com IVs do tipo \textit {n-layers}}{58}{subsection.5.7.4}
\contentsline {subsection}{\numberline {5.7.5}An\IeC {\'a}lise dos Resultados com IVs do tipo \textit {Mixed}}{61}{subsection.5.7.5}
\contentsline {subsection}{\numberline {5.7.6}Discuss\IeC {\~a}o e Principais Observa\IeC {\c c}\IeC {\~o}es}{63}{subsection.5.7.6}
\contentsline {section}{\numberline {5.8}Considera\IeC {\c c}\IeC {\~o}es Parciais}{65}{section.5.8}
\contentsline {chapter}{\numberline {6}Conclus\IeC {\~a}o}{67}{chapter.6}
\contentsline {chapter}{Refer\IeC {\^e}ncias}{68}{chapter*.40}
